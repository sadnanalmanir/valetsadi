SELECT TABLE0.algAdverseReaction AS X7
FROM Nallergy AS TABLE0
WHERE EXISTS( SELECT *
             FROM Nallergy AS TABLE1,
                  Npatient AS TABLE2
             WHERE TABLE0.algWID = TABLE1.algWID
               AND TABLE1.algPatWID = TABLE2.patWID)