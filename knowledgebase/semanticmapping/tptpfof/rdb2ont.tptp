%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: Npatient
%% TABLE PREDICATE: db_Npatient
%% PRIMARY-KEY-TO-ENTITY FUNCTION: entityForPatient
%% INVERSE FOR PRIMARY-KEY-TO-ENTITY FUNCTION: entityForPatientToPatWID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setting up primary key PatWID
% entityForPatientToPatWID is an inverse function for entityForPatient.

fof(inverse_for_entityForPatient_1,axiom,
   ! [PatWID] : (entityForPatientToPatWID(entityForPatient(PatWID)) = PatWID)
).

fof(inverse_for_entityForPatient_2,axiom,
   ! [P] : (entityForPatient(entityForPatientToPatWID(P)) = P)
).

% Mapping the attribute Npatient.patWID to entities of the concept HAI:Patient.

fof(table_Npatient_represents_instances_of_concept_Person,axiom,
  ! [PatWID, PatFacWID, PatLastName,PatFirstName] :
  (db_Npatient(PatWID, PatFacWID, PatLastName,PatFirstName)
  =>
  p_Patient(entityForPatient(PatWID)))
).

fof(table_Npatient_represents_instances_of_concept_Person,axiom,
  ! [PatWID, PatFacWID, PatLastName,PatFirstName] :
  (db_Npatient(PatWID, PatFacWID, PatLastName,PatFirstName)
  =>
  p_has_last_name(entityForPatient(PatWID), PatLastName))
).

fof(table_Npatient_represents_instances_of_concept_Person,axiom,
  ! [PatWID, PatFacWID, PatLastName,PatFirstName] :
  (db_Npatient(PatWID, PatFacWID, PatLastName,PatFirstName)
  =>
  p_has_first_name(entityForPatient(PatWID), PatFirstName))
).

fof(attribute_PatFacWID_assigned_fk_to_table_Nfacility,axiom,
  ! [PatWID, PatFacWID, PatLastName,PatFirstName] :
  (db_Npatient(PatWID, PatFacWID, PatLastName,PatFirstName)
  =>
  p_has_facility_reference(entityForPatient(PatWID), entityForFacility(PatFacWID)))
).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: Nfacility
%% TABLE PREDICATE: db_Nfacility
%% PRIMARY-KEY-TO-ENTITY FUNCTION: entityForFacility
%% INVERSE FOR PRIMARY-KEY-TO-ENTITY FUNCTION: entityForFacilityToFacWID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setting up primary key FacWID
% entityForFacilityToFacWID is an inverse function for entityForFacility.

fof(inverse_for_entityForFacility_1,axiom,
   ! [FacWID] : (entityForFacilityToFacWID(entityForFacility(FacWID)) = FacWID)
).

fof(inverse_for_entityForFacility_2,axiom,
   ! [P] : (entityForFacility(entityForFacilityToFacWID(P)) = P)
).

% Mapping the attribute Nfacility.FacWID to entities of the concept HAI:Facility.

fof(table_Nfacility_represents_instances_of_concept_Facility,axiom,
  ! [FacWID, FacFacilityDesc] :
  (db_Nfacility(FacWID, FacFacilityDesc)
  =>
  p_Facility(entityForFacility(FacWID)))
).

% Mapping the attribute Nfacility.FacFacilityDesc to the property HAI:has_facility_description.

fof(attribute_PatLastName_assigns_has_last_name,axiom,
  ! [FacWID, FacFacilityDesc] :
  (db_Nfacility(FacWID, FacFacilityDesc)
  =>
  p_has_facility_description(entityForFacility(FacWID),FacFacilityDesc))
).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: NhrAbstract
%% TABLE PREDICATE: db_NhrAbstract
%% PRIMARY-KEY-TO-ENTITY FUNCTION: entityForAbstract
%% INVERSE FOR PRIMARY-KEY-TO-ENTITY FUNCTION: entityForAbstractTohraWID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setting up primary key hraWID
% entityForAbstractTohraWID is an inverse function for entityForAbstract.

fof(inverse_for_entityForAbstract_1,axiom,
   ! [HraWID] : (entityForAbstractTohraWID(entityForAbstract(HraWID)) = HraWID)
).

fof(inverse_for_entityForAbstract_2,axiom,
   ! [P] : (entityForAbstract(entityForAbstractTohraWID(P)) = P)
).

% Mapping the attribute NhrAbstract.hraWID to entities of the concept HAI:Abstract.

fof(table_NhrAbstract_represents_instances_of_concept_Abstract,axiom,
  ! [HraWID, HraEncWID, HraPatWID] :
  (db_NhrAbstract(HraWID, HraEncWID, HraPatWID)
  =>
  p_Abstract(entityForAbstract(HraWID)))
).

% Mapping the attribute NhrAbstract.hraWID to entities of the concept HAI:Abstract.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: NhrDiagnosis
%% TABLE PREDICATE: db_NhrDiagnosis
%% PRIMARY-KEY-TO-ENTITY FUNCTION: entityForDiagnosis
%% INVERSE FOR PRIMARY-KEY-TO-ENTITY FUNCTION: entityForDiagnosisTohdgWID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setting up primary key hdgWID
% entityForDiagnosisTohdgWID is an inverse function for entityForDiagnosis.

fof(inverse_for_entityForDiagnosis_1,axiom,
   ! [HdgWID] : (entityForDiagnosisTohdgWID(diagnosisEntity(HdgWID)) = HdgWID)
).

fof(inverse_for_entityForDiagnosis_2,axiom,
   ! [P] : (entityForDiagnosis(entityForDiagnosisTohdgWID(P)) = P)
).

% Mapping the attribute NhrDiagnosis.hdgWID to entities of the concept HAI:Diagnosis.

fof(table_NhrDiagnosis_represents_instances_of_concept_Diagnosis,axiom,
  ! [HdgWID, HdgHraWID, HdgHraEncWID, HdgCd] :
  (db_NhrDiagnosis(HdgWID, HdgHraWID, HdgHraEncWID, HdgCd)
  =>
  p_Diagnosis(entityForDiagnosis(HdgWID)))
).

% Mapping the attribute NhrDiagnosis.hdgCd to the property HAI:has_diagnosis_code.

fof(table_NhrDiagnosis_represents_has_diagnosis_code,axiom,
  ! [HdgWID, HdgHraWID, HdgHraEncWID, HdgCd] :
  (db_NhrDiagnosis(HdgWID, HdgHraWID, HdgHraEncWID, HdgCd)
  =>
  p_has_diagnosis_code(entityForDiagnosis(HdgWID), HdgCd))
).

% Mapping the attribute NhrDiagnosis.hdgHraEncWID to the property HAI:diagnosisEncountered.

fof(table_NhrDiagnosis_represents_diagnosis_encountered,axiom,
  ! [HdgWID, HdgHraWID, HdgHraEncWID, HdgCd] :
  (db_NhrDiagnosis(HdgWID, HdgHraWID, HdgHraEncWID, HdgCd)
  =>
  p_diagnosis_encountered(entityForDiagnosis(HdgWID), entityForEncounter(HdgHraEncWID)))
).

% Mapping the attribute NhrDiagnosis.HdgHraWID to the property HAI:hasAbstractRecord.

fof(table_NhrAbstract_represents_instances_of_concept_Abstract,axiom,
  ! [HraWID, HraEncWID, HraPatWID] :
  (db_NhrAbstract(HraWID, HraEncWID, HraPatWID)
  =>
  %p_abstractRecordForPatient(entityForAbstract(HraWID), entityForPatient(HraPatWID)))
  p_abstractRecordForPatient(entityForPatient(HraPatWID), entityForAbstract(HraWID)))
).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fof(table_NhrDiagnosis_represents_has_diagnosis_code,axiom,
  ! [HdgWID, HdgHraWID, HdgHraEncWID, HdgCd] :
  (db_NhrDiagnosis(HdgWID, HdgHraWID, HdgHraEncWID, HdgCd)
  =>
  p_diagnosis_encountered(entityForDiagnosis(HdgWID), entityForAbstract(HdgHraWID)))
).

fof(table_NhrAbstract_represents_instances_of_concept_Abstract,axiom,
  ! [HraWID, HraEncWID, HraPatWID] :
  (db_NhrAbstract(HraWID, HraEncWID, HraPatWID)
  =>
  % both directions work
  % p_has_abstract_record(entityForPatient(HraPatWID), entityForAbstract(HraWID)))
  p_has_abstract_record(entityForAbstract(HraWID), entityForPatient(HraPatWID)))
).

fof(arbitrary_rule,axiom,
  ! [PatWID, HraWID, HdgWID] :
  ( 
   (
    p_has_abstract_record(entityForAbstract(HraWID), entityForPatient(PatWID))
    &
    p_diagnosis_encountered(entityForDiagnosis(HdgWID), entityForAbstract(HraWID))
   )
  =>
    p_has_diagnosis(entityForPatient(PatWID), entityForDiagnosis(HdgWID))
)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: Nallergy
%% TABLE PREDICATE: db_Nallergy
%% PRIMARY-KEY-TO-ENTITY FUNCTION: entityForAllergy
%% INVERSE FOR PRIMARY-KEY-TO-ENTITY FUNCTION: entityForAllergyToAlgWID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setting up primary key AlgWID
% entityForAllergyToAlgWID is an inverse function for entityForAllergy.

fof(inverse_for_entityForAllergy_1,axiom,
   ! [AlgWID] : (entityForAllergyToAlgWID(entityForAllergy(AlgWID)) = AlgWID)
).

fof(inverse_for_entityForAllergy_2,axiom,
   ! [P] : (entityForAllergy(entityForAllergyToAlgWID(P)) = P)
).

% Mapping the attribute Nallergy.AlgWID to entities of the concept HAI:Allergy.

fof(table_Nallergy_represents_instances_of_concept_Allergy,axiom,
  ! [AlgWID, AlgPatWID, AlgAdverseReaction] :
  (db_Nallergy(AlgWID, AlgPatWID, AlgAdverseReaction)
  =>
  p_Allergy(entityForAllergy(AlgWID)))
).

% Mapping the attribute Nallergy.AlgAllergyDesc to the property HAI:has_facility_description.

fof(attribute_AlgPatWID_used_as_foreign_key_to_patient,axiom,
  ! [AlgWID, AlgPatWID, AlgAdverseReaction] :
  (db_Nallergy(AlgWID, AlgPatWID, AlgAdverseReaction)
  =>
  p_has_allergy(entityForPatient(AlgPatWID), entityForAllergy(AlgWID)))
).

fof(attribute_AlgPatWID_used_as_foreign_key_to_patient,axiom,
  ! [AlgWID, AlgPatWID, AlgAdverseReaction] :
  (db_Nallergy(AlgWID, AlgPatWID, AlgAdverseReaction)
  =>
  p_has_adverse_reaction_to(entityForAllergy(AlgWID), AlgAdverseReaction))
).





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: NhrProcedure
%% TABLE PREDICATE: db_NhrProcedure
%% PRIMARY-KEY-TO-ENTITY FUNCTION: entityForProcedure
%% INVERSE FOR PRIMARY-KEY-TO-ENTITY FUNCTION: entityForProcedureTohprcWID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setting up primary key hprcWID
% entityForProcedureTohprcWID is an inverse function for entityForProcedure.

fof(inverse_for_entityForProcedure_1,axiom,
   ! [HprcWID] : (entityForProcedureTohprcWID(entityForProcedure(HprcWID)) = HprcWID)
).

fof(inverse_for_entityForProcedure_2,axiom,
   ! [P] : (entityForProcedure(entityForProcedureTohprcWID(P)) = P)
).

% Mapping the attribute NhrProcedure.hprcWID to entities of the concept HAI:Procedure.

fof(table_NhrProcedure_represents_instances_of_concept_Procedure,axiom,
  ! [HprcWID, HprcHraWID, HprcHraEncWID, HprcStartTime] :
  (db_NhrProcedure(HprcWID, HprcHraWID, HprcHraEncWID, HprcStartTime)
  =>
  p_Procedure(entityForProcedure(HprcWID)))
).

% Mapping the attribute NhrProcedure.hprcStartTime to the property HAI:has_start_time.

fof(table_NhrProcedure_represents_has_start_time,axiom,
  ! [HprcWID, HprcHraWID, HprcHraEncWID, HprcStartTime] :
  (db_NhrProcedure(HprcWID, HprcHraWID, HprcHraEncWID, HprcStartTime)
  =>
  p_has_start_time(entityForProcedure(HprcWID), HprcStartTime))
).


fof(table_NhrProcedure_represents_procedure_encounterd,axiom,
  ! [HprcWID, HprcHraWID, HprcHraEncWID, HprcStartTime] :
  (db_NhrProcedure(HprcWID, HprcHraWID, HprcHraEncWID, HprcStartTime)
  =>
  p_procedure_reference(entityForProcedure(HprcWID), entityForAbstract(HprcHraWID)))
).

fof(arbitrary_rule_for_procedure_for_patient,axiom,
  ! [PatWID, HprcWID, HraWID] :
  ( 
   (
    p_has_abstract_record(entityForAbstract(HraWID), entityForPatient(PatWID))
    &
    p_procedure_reference(entityForProcedure(HprcWID), entityForAbstract(HraWID))
   )
  =>
    p_undergo_procedure(entityForPatient(PatWID), entityForProcedure(HprcWID))
)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: Nencounter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fof(inverse_for_entityForEncounter_1,axiom,
   ! [EncWID] : (entityForEncounterToEncWID(entityForEncounter(EncWID)) = EncWID)
).

fof(inverse_for_entityForEncounter_2,axiom,
   ! [P] : (entityForEncounter(entityForEncounterToEncWID(P)) = P)
).

fof(table_Nencounter_represents_instances_of_concept_Encounter,axiom,
  ! [EncWID, EncPatWID] :
  (db_Nencounter(EncWID, EncPatWID)
  =>
  p_Encounter(entityForEncounter(EncWID)))
).

fof(attribute_EncPatWID_assigned_fk_to_table_Npatient,axiom,
  ! [EncWID, EncPatWID] :
  (db_Nencounter(EncWID, EncPatWID)
  =>
  p_encountersPatient(entityForEncounter(EncWID), entityForPatient(EncPatWID)))
).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: Nservice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fof(inverse_for_entityForService_1,axiom,
   ! [SvcWID] : (entityForServiceToSvcWID(entityForService(SvcWID)) = SvcWID)
).

fof(inverse_for_entityForService_2,axiom,
   ! [P] : (entityForService(entityForServiceToSvcWID(P)) = P)
).

fof(table_Nservice_represents_instances_of_concept_Service,axiom,
  ! [SvcWID, SvcEncWID, SvcTableCd, SvcOrderedBy, SvcPerformedBy] :
  (db_Nservice(SvcWID, SvcEncWID, SvcTableCd, SvcOrderedBy, SvcPerformedBy)
  =>
  p_Service(entityForService(SvcWID)))
).

fof(attribute_SvcEncWID_assigned_fk_to_table_Nencounter,axiom,
  ! [SvcWID, SvcEncWID, SvcTableCd, SvcOrderedBy, SvcPerformedBy] :
  (db_Nservice(SvcWID, SvcEncWID, SvcTableCd, SvcOrderedBy, SvcPerformedBy)
  =>
  p_service_for_encounter(entityForService(SvcWID), entityForEncounter(SvcEncWID)))
).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLE: NphmIngredient
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fof(inverse_for_entityForPharmacy_1,axiom,
   ! [PhmWID] : (entityForPharmacyToPhmWID(entityForPharmacy(PhmWID)) = PhmWID)
).

fof(inverse_for_entityForPharmacy_2,axiom,
   ! [P] : (entityForPharmacy(entityForPharmacyToPhmWID(P)) = P)
).

fof(table_NphmIngredient_represents_instances_of_concept_Pharmacy,axiom,
  ! [PhmWID, PhmSvcWID, PhmOrderDesc, PhmStartDtm, PhmEndDtm, PhmStrengthDesc, PhmDosageForm] :
  (db_NphmIngredient(PhmWID, PhmSvcWID, PhmOrderDesc, PhmStartDtm, PhmEndDtm, PhmStrengthDesc, PhmDosageForm)
  =>
  p_Pharmacy(entityForPharmacy(PhmWID)))
).

fof(attribute_NphmIngredient_assigned_fk_to_table_Nservice,axiom,
  ! [PhmWID, PhmSvcWID, PhmOrderDesc, PhmStartDtm, PhmEndDtm, PhmStrengthDesc, PhmDosageForm] :
  (db_NphmIngredient(PhmWID, PhmSvcWID, PhmOrderDesc, PhmStartDtm, PhmEndDtm, PhmStrengthDesc, PhmDosageForm)
  =>
  p_prescription_service(entityForPharmacy(PhmWID), entityForService(PhmSvcWID)))
).

fof(arbitrary_rule_for_pharmacy_for_patient,axiom,
  ! [PatWID, EncWID, SvcWID, PhmWID] :
  ( 
   (
    p_encountersPatient(entityForEncounter(EncWID), entityForPatient(PatWID))
    &
    p_service_for_encounter(entityForService(SvcWID), entityForEncounter(EncWID))
    &
    p_prescription_service(entityForPharmacy(PhmWID), entityForService(SvcWID))
   )
  =>
    p_councel_at(entityForPatient(PatWID), entityForPharmacy(PhmWID))
)).

fof(attribute_PhmStartDtm_assigned_to_has_start_time,axiom,
  ! [PhmWID, PhmSvcWID, PhmOrderDesc, PhmStartDtm, PhmEndDtm, PhmStrengthDesc, PhmDosageForm] :
  (db_NphmIngredient(PhmWID, PhmSvcWID, PhmOrderDesc, PhmStartDtm, PhmEndDtm, PhmStrengthDesc, PhmDosageForm)
  =>
  p_has_start_time(entityForPharmacy(PhmWID), PhmStartDtm))
).

fof(attribute_PhmOrderDesc_assigned_to_description_of_order,axiom,
  ! [PhmWID, PhmSvcWID, PhmOrderDesc, PhmStartDtm, PhmEndDtm, PhmStrengthDesc, PhmDosageForm] :
  (db_NphmIngredient(PhmWID, PhmSvcWID, PhmOrderDesc, PhmStartDtm, PhmEndDtm, PhmStrengthDesc, PhmDosageForm)
  =>
  p_description_of_order(entityForPharmacy(PhmWID), PhmOrderDesc))
).
