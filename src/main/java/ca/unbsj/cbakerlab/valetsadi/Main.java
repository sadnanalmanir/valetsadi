package ca.unbsj.cbakerlab.valetsadi;

import ca.unbsj.cbakerlab.valetsadi.module2.IOGraphGenerator;
import ca.unbsj.cbakerlab.valetsadi.module2.ServiceOntologyException;
import ca.unbsj.cbakerlab.valetsadi.module2.ServiceOntologyParamExtractor;
import ca.unbsj.cbakerlab.valetsadi.module3.Executor;
import ca.unbsj.cbakerlab.valetsadi.module3.SQLGenerationException;
import ca.unbsj.cbakerlab.valetsadi.module4.CodeGenerationException;
import ca.unbsj.cbakerlab.valetsadi.module4.ServiceCodeGenerator;
import com.tinkerpop.blueprints.Graph;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLClassExpression;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);

    // Syntactic I/O Validity checker library for HYDRA
    private static final String HYDRA_COMPATIBILITY_VALIDATOR_FILE_PATH = "iocompatibility/hydravalidator/";
    private static final String HYDRA_COMPATIBILITY_VALIDATOR_FILE_NAME = "SADI_service_description_validator.jar";

    // Path for TPTP-FOF translation of domain ontology in OWL
    private static final String DOMAIN_ONTOLOGY_TO_TPTP_FOF_PATH = "knowledgebase/domain-ontology/tptpfof/";

    // Path for TPTP-FOF translation of PSOA mapping rules
    private static final String SEMANTIC_MAPPING_TO_TPTP_FOF_PATH = "knowledgebase/semanticmapping/tptpfof/";

    // Path for semantic query
    private static final String SEMANTIC_QUERY_PATH = "semanticquery/";
    private static final String TPTP_FILE_EXTENTION = "tptp";

    // Path for schematic answers
    private static final String SCHEMATIC_ANSWERS_PATH = "schematicanswers/";
    private static final String SCHEMATIC_ANSWERS_FILE_NAME = "schematic_answers";
    public static final String XML_FILE_EXTENTION = "xml";

    // Path for predicate views filename
    public static final String PREDICATE_VIEWS_FILE_PATH = "rdb/views/";
    public static final String PREDICATE_VIEWS_FILE_NAME = "predicate_views";

    // Path for schema definition of predicate views
    public static final String PREDICATE_VIEWS_SCHEMA_FILE_PATH = "rdb/views/schema/";
    public static final String PREDICATE_VIEWS_SCHEMA_FILE_NAME = "PredicateViews";
    public static final String XML_SCHEMA_FILE_EXTENTION = "xsd";

    // Path for SQL-template query
    private static final String SQL_TEMPLATE_QUERY_PATH = "sql/template/";
    private static final String SQL_TEMPLATE_QUERY_FILE_NAME = "query";
    private static final String SQL_FILE_EXTENTION = "sql";

    // function names used as a prefix in the mapping rules of class-table equations
    public static final String PRIMARY_KEY_MAPPING_ATOM_PREFIX = "entityFor";

    // Path for the resources directory
    private static String RESOURCES_DIR_PATH = "src/main/resources/";
    // Vampire Prime FOL reasoner
    private static String VAMPIRE_PRIME_FOL_REASONER_PATH = RESOURCES_DIR_PATH + "reasoner/vampireprime/";
    // answer predicate file
    private static String ANSWER_PREDICATES_FILE_PATH = "rdb/abstraction/answer_predicates.xml";
    // extensional predicate file
    private static String EXTENSIONAL_PREDICATES_FILE_PATH = "rdb/abstraction/extensional_predicates.xml";

    // Service code generation specifications
    public static final String SERVICE_CODE_DIR_NAME = "sadi-service-code";
    public static final String SERVICE_PACKAGE_STRUCTURE = "ca.unbsj.cbakerlab.sadi.service";
    public static final String SOURCE_DIRECTORY = "src/main/java";
    public static final String RESOURCES_DIRECTORY = "src/main/resources";
    public static final boolean IS_SYNCHRONOUS_SERVICE = false;

    public static final String DB_CLASS_NAME = "MySqlDatabaseConn";
    public static final String VOCAB_FILE_NAME = "Vocab";
    public static final String README_FILENAME = "README";
    public static final String WEB_XML_PATH = "src/main/webapp/WEB-INF/web.xml";
    public static final String INDEX_JSP_PATH = "src/main/webapp/index.jsp";
    public static final String DB_PROPERTY_FILENAME = "database";

    // Prefix and suffix for the URIs
    public static final String URI_PREFIX = "http://cbakerlab.unbsj.ca:8080/dw/model/function/";
    public static final String URI_SUFFIX = "/val?val";

    private final String ESCAPE_DOUBLE_QUOTE_SYMBOL = "\"";

    private static void run(String[] args) throws ServiceOntologyException, SQLGenerationException, IOException, CodeGenerationException {

        //long startTime = System.currentTimeMillis();

        // Parameters needed to define a SADI service
        String serviceOntologyURI;
        String serviceName;
        String inputClassURI;
        String outputClassURI;
        String email;
        String description;

        //boolean needSeedInputData = false;
        boolean runGenerator = false;

        // Define options.

        // create the command line parser
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        Options options = new Options();

        Option serviceURI = new Option("s", "serviceOntologyURI", true, "URI of Service Ontology");
        serviceURI.setRequired(true);
        options.addOption(serviceURI);

        Option emailAddress = new Option("e", "email", true, "The contact email of the author");
        emailAddress.setRequired(true);
        options.addOption(emailAddress);

        Option desc = new Option("d", "description", true, "The description of the service's functionality");
        desc.setRequired(true);
        options.addOption(desc);

        Option generate = new Option("generate", "Initiate SADI service generation process");
        generate.setRequired(true);
        options.addOption(generate);

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
        }

        serviceOntologyURI = cmd.getOptionValue("serviceOntologyURI");
        if (serviceOntologyURI != null) {
            logger.info("Service Ontology URI: " + serviceOntologyURI);
        }

        email = cmd.getOptionValue("email");
        if (email != null) {
            logger.info("Contact email: " + email);
        }

        description = cmd.getOptionValue("description");
        if (description != null) {
            logger.info("Description: [No white space allowed, use underscore.] " + description);
        }

        if (cmd.hasOption("generate")) {
            runGenerator = true;
            logger.info("runGenerator: " + true);
        }

        if (runGenerator) {

            ServiceOntologyParamExtractor paramExtractor = new ServiceOntologyParamExtractor(serviceOntologyURI);

            // Check the validity of the Service Ontology URI
            if (paramExtractor.isValidURI(serviceOntologyURI)) {
                throw new ServiceOntologyException("The URI of the Service Ontology cannot be accessed.");
            }

            // unique service name
            serviceName = paramExtractor.getServiceNameFromURI();

            inputClassURI = paramExtractor.getInputURIFromServiceOntology();
            if (inputClassURI == null) {
                throw new ServiceOntologyException("No input/Input (concept) class definition found in the service ontology.");
            }
            // Check the validity of the input of Service Ontology URI
            if (paramExtractor.isValidURI(inputClassURI)) {
                throw new ServiceOntologyException("The URI of the input class expression cannot be accessed.");
            }

            outputClassURI = paramExtractor.getOutputURIFromServiceOntology();
            if (outputClassURI == null) {
                throw new ServiceOntologyException("No output/Output (concept) class definition found in the service ontology.");
            }
            // Check the validity of the output of Service Ontology URI
            if (paramExtractor.isValidURI(outputClassURI)) {
                throw new ServiceOntologyException("The URI of the output class expression cannot be accessed.");
            }

            logger.info("==================================");
            logger.info("* Getting Input class expression *");
            logger.info("==================================");
            OWLClassExpression inputClassExpr = paramExtractor.getInputClassExpr(serviceOntologyURI, inputClassURI);
            if (inputClassExpr == null) {
                throw new ServiceOntologyException("No input OWL class expression found for : " + inputClassURI);
            }
            logger.info(inputClassExpr);

            logger.info("===================================");
            logger.info("* Getting Output class expression *");
            logger.info("===================================");
            OWLClassExpression outputClassExpr = paramExtractor.getOutputClassExpr(serviceOntologyURI, outputClassURI);
            if (outputClassExpr == null) {
                throw new ServiceOntologyException("No output OWL class expression found for : " + outputClassURI);
            }
            logger.info(outputClassExpr);

            logger.info("================================================");
            logger.info("* Checking HYDRA compatibility of Input class  *");
            logger.info("================================================");
            if (!paramExtractor.isInputHYDRACompatible(inputClassURI, HYDRA_COMPATIBILITY_VALIDATOR_FILE_PATH + HYDRA_COMPATIBILITY_VALIDATOR_FILE_NAME)) {
                throw new ServiceOntologyException("The input class expression of the service is incompatible with HYDRA for " + inputClassURI);
            }

            logger.info("================================================");
            logger.info("* Checking HYDRA compatibility of Output class *");
            logger.info("================================================");
            if (!paramExtractor.isOutputHYDRACompatible(outputClassURI, HYDRA_COMPATIBILITY_VALIDATOR_FILE_PATH + HYDRA_COMPATIBILITY_VALIDATOR_FILE_NAME)) {
                throw new ServiceOntologyException("The output class expression of the service is incompatible with HYDRA for " + outputClassURI);
            }

            logger.info("=============================================================");
            logger.info("* Generating Input/Output graphs from I/O Class Expressions *");
            logger.info("=============================================================");
            IOGraphGenerator ioGraphGenerator = new IOGraphGenerator();
            ioGraphGenerator.generateIOGraphs(inputClassExpr, outputClassExpr);
            Graph inputGraph = ioGraphGenerator.getInputExprGraph();
            Graph outputGraph = ioGraphGenerator.getOutputExprGraph();

            logger.info("==============================================================");
            logger.info("* Getting domain ontologies imported in the Service ontology *");
            logger.info("==============================================================");

            List<String> domainOntologyURIS = paramExtractor.getImportedOntologies();
            if (domainOntologyURIS.size() == 0) {
                throw new ServiceOntologyException("No domain ontology imported to service ontology. Check the service ontology.");
            } else {
                // Check the validity of the output of Service Ontology URI
                for (String uri : domainOntologyURIS) {
                    if (paramExtractor.isValidURI(uri)) {
                        throw new ServiceOntologyException("Unable to access imported domain ontology at " + uri);
                    }
                }
            }

            logger.info("======================================================================");
            logger.info("* Translate and save imported domain ontologies into TPTP-FOF axioms *");
            logger.info("======================================================================");

            Executor executor = new Executor();
            for (String uri : domainOntologyURIS) {
                String domainOntologyName = paramExtractor.getOntologyNameFromURI(uri);
                logger.info("Translation begins for " + domainOntologyName);
                String ontologyInTptpFof = executor.translateOntologyToTptpFofAxiom(uri);
                if(StringUtils.isBlank(ontologyInTptpFof))
                    throw new SQLGenerationException("Error translating the domain ontology " + domainOntologyName);
                else{
                    logger.info("Translation ends.");
                    String pathSaved = executor.saveTranslatedOntology(DOMAIN_ONTOLOGY_TO_TPTP_FOF_PATH, domainOntologyName, ontologyInTptpFof, TPTP_FILE_EXTENTION);
                    logger.info("Translated Ontology Saved as : " + pathSaved);
                }
            }

            logger.info("=========================================");
            logger.info("* Initiate generation of semantic query *");
            logger.info("=========================================");

            String semanticQuery;
            semanticQuery = executor.createSemanticQueryFromIOGraphs(inputGraph, inputClassExpr, outputGraph, outputClassExpr, DOMAIN_ONTOLOGY_TO_TPTP_FOF_PATH, SEMANTIC_MAPPING_TO_TPTP_FOF_PATH);
            if(StringUtils.isBlank(semanticQuery))
                throw new SQLGenerationException("Semantic query could not be created");
            logger.info("------------------");
            logger.info("* Semantic Query *");
            logger.info("------------------");
            logger.info("\n" + semanticQuery);
            String semanticQueryPath = executor.saveSemanticQuery(SEMANTIC_QUERY_PATH, serviceName, semanticQuery, TPTP_FILE_EXTENTION);
            logger.info("Semantic query saved as : " + semanticQueryPath);

            logger.info("-------------------------------------------------------------------------------");
            logger.info("* Getting input variables (WHERE clause) and output variables (SELECT clause) *");
            logger.info("-------------------------------------------------------------------------------");
            List<String> inputVars = new LinkedList<>(executor.getInputVariables());
            logger.info("Input variables (WHERE clause) : " + inputVars.toString());
            List<String> outputVars = new LinkedList<>(executor.getOutputVariables());
            logger.info("Output variables (SELECT clause) : " + outputVars.toString());

            logger.info("==============================");
            logger.info("* Generate schematic answers *");
            logger.info("==============================");
            String schematicAnswers;
            schematicAnswers = executor.generateSchematicAnswers(VAMPIRE_PRIME_FOL_REASONER_PATH, ANSWER_PREDICATES_FILE_PATH, EXTENSIONAL_PREDICATES_FILE_PATH, semanticQueryPath);
            if(StringUtils.isBlank(schematicAnswers))
                throw new SQLGenerationException("Schematic answers to the semantic query could not be generated");
            logger.info("---------------------");
            logger.info("* Schematic Answers *");
            logger.info("---------------------");
            logger.info("\n" + schematicAnswers);
            String schematicAnswersPath = executor.saveSchematicAnswers(SCHEMATIC_ANSWERS_PATH, SCHEMATIC_ANSWERS_FILE_NAME, schematicAnswers, XML_FILE_EXTENTION);
            logger.info("Schematic answers saved as : " + schematicAnswersPath);

            logger.info("=================================");
            logger.info("* Generating SQL Query Template *");
            logger.info("=================================");
            String sqlTemplateQuery;
            sqlTemplateQuery = executor.generateSQLTemplateQuery(schematicAnswers, PREDICATE_VIEWS_FILE_PATH + PREDICATE_VIEWS_FILE_NAME + "." + XML_FILE_EXTENTION);
            if(StringUtils.isBlank(sqlTemplateQuery))
                throw new SQLGenerationException("SQL-template query could not be generated");
            // trim white spaces from the query
            sqlTemplateQuery = sqlTemplateQuery.trim();
            logger.info("----------------------");
            logger.info("* SQL-template Query *");
            logger.info("----------------------");
            logger.info("\n" + sqlTemplateQuery);
            String sqlTemplateQueryPath = executor.saveSqlTemplateQuery(SQL_TEMPLATE_QUERY_PATH, SQL_TEMPLATE_QUERY_FILE_NAME, sqlTemplateQuery, SQL_FILE_EXTENTION);
            logger.info("SQL-template query saved as : " + sqlTemplateQueryPath);




            logger.info("\n");
            logger.info("=======================================");
            logger.info("* Initiate generation of service code *");
            logger.info("=======================================");
            ServiceCodeGenerator serviceCodeGenerator = new ServiceCodeGenerator();
            serviceCodeGenerator.startInputCodeGenerationInJava(inputClassExpr, inputGraph);
            String generatedInputCode = serviceCodeGenerator.getGeneratedInputCode();
            logger.info(generatedInputCode);

            serviceCodeGenerator.startBuildingAuxiliaryFiles(serviceOntologyURI, serviceName, inputClassURI, inputClassExpr, outputClassURI, outputClassExpr, email, description);

        }

    }


    public static void main(String[] args) throws Exception {

        //args = new String[]{"-s ", "http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/getInsecticideIdByAssayId.owl", " -e ", "sadnanalamnir@gmail.com", " -d ", "TEST_DESCRIPTION", " --seed", " --generate"};
        boolean hasArguments = false;
        if (args.length != 0) {
            hasArguments = args.length != 1 || !args[0].equals("${args}");
        }

        if (hasArguments) {
            //String arguments = Arrays.toString(args).replace(", ", " ");
            //arguments = arguments.substring(1, arguments.length() - 1);
            logger.info("====================");
            logger.info("* Arguments passed *");
            logger.info("====================");
            //logger.info(arguments);
            //run(arguments);
            run(args);
        } else {
            System.out.println("NO ARGUMENTS. CHECKARGUMENTS.");

        }
        System.out.println("\nAll done.");

    }

}