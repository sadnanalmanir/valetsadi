package ca.unbsj.cbakerlab.valetsadi.module3.owltotptp;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLAsymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLAxiomVisitor;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLClassExpressionVisitor;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataComplementOf;
import org.semanticweb.owlapi.model.OWLDataExactCardinality;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataIntersectionOf;
import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
import org.semanticweb.owlapi.model.OWLDataMinCardinality;
import org.semanticweb.owlapi.model.OWLDataOneOf;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLDataUnionOf;
import org.semanticweb.owlapi.model.OWLDataVisitor;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLDatatypeDefinitionAxiom;
import org.semanticweb.owlapi.model.OWLDatatypeRestriction;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLFacetRestriction;
import org.semanticweb.owlapi.model.OWLFunctionalDataPropertyAxiom;
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLHasKeyAxiom;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLIndividualAxiom;
import org.semanticweb.owlapi.model.OWLInverseFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLIrreflexiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLNegativeDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLNegativeObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectInverseOf;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
import org.semanticweb.owlapi.model.OWLObjectOneOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.OWLPropertyAxiom;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import org.semanticweb.owlapi.model.OWLReflexiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLSameIndividualAxiom;
import org.semanticweb.owlapi.model.OWLSubAnnotationPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLTransitiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.SWRLAtom;
import org.semanticweb.owlapi.model.SWRLBuiltInAtom;
import org.semanticweb.owlapi.model.SWRLClassAtom;
import org.semanticweb.owlapi.model.SWRLDArgument;
import org.semanticweb.owlapi.model.SWRLDataPropertyAtom;
import org.semanticweb.owlapi.model.SWRLDataRangeAtom;
import org.semanticweb.owlapi.model.SWRLDifferentIndividualsAtom;
import org.semanticweb.owlapi.model.SWRLIArgument;
import org.semanticweb.owlapi.model.SWRLIndividualArgument;
import org.semanticweb.owlapi.model.SWRLLiteralArgument;
import org.semanticweb.owlapi.model.SWRLObjectPropertyAtom;
import org.semanticweb.owlapi.model.SWRLRule;
import org.semanticweb.owlapi.model.SWRLSameIndividualAtom;
import org.semanticweb.owlapi.model.SWRLVariable;
import org.semanticweb.owlapi.vocab.OWLFacet;

//import tptp_parser.*;
import logic.is.power.tptp_parser.*;
//import tptp_parser.*;
public class Converter {
	
	/**********************************************************************************************************
	 * ********************************************************************************************************
	 * ********************************************************************************************************
	 */
	
	public static class TPTPSymbolDescriptor{
		
		public TPTPSymbolDescriptor(boolean predicate,
			    String name,
			    int arity) {
			_isPredicate = predicate;
			_name = name;
			_arity = arity;
		}
		
		public boolean isPredicate() { return _isPredicate; }
		public String name() { return _name; }
		public int arity() { return _arity; }
		
		public int hashCode() { 
		    return _name.hashCode() + _arity + ((_isPredicate)? 0 : 1);
		}
		
		public boolean equals(Object obj) {
		    return (obj instanceof TPTPSymbolDescriptor) &&
			_isPredicate == ((TPTPSymbolDescriptor)obj)._isPredicate &&
			_name.equals(((TPTPSymbolDescriptor)obj)._name) &&
			_arity == ((TPTPSymbolDescriptor)obj)._arity;
		}
		
		public String toString() {
		    return ((_isPredicate)? "PRED " : "") + _name + ":" + _arity;
		}
			
		private boolean _isPredicate;
		private String _name;
		private int _arity;

	}// class TPTPSymbolDescriptor

	
	/**********************************************************************************************************
	 * ********************************************************************************************************
	 * ********************************************************************************************************
	 */

	
	/** Various parameters that guide the conversion; some of 
     *  the parameters are modified by the conversion within this object
     *  and can be used afterwards in other objects; eg, mappings
     *  of URIs to TPTP identifiers are augmented when new URIs are
     *  encountered.
     */
	
	
	public static class Parameters{
		
		Parameters(){
			 _URIsToTPTPSymbols = 
						new HashMap<String,Set<Converter.TPTPSymbolDescriptor>>();
					     _otherReservedSymbols = 
						 new HashSet<Converter.TPTPSymbolDescriptor>();
					     _dataDomainPredicate = null;
					     _equalityPredicate = null;
					     _intLitFunc = null;
					     _stringLitNoLangFunc = null;
					     _stringLitWithLangFunc = null;
					     _typedLitFunc = null;
		}
		
		/** Mapping that accumulates the correspondence between URIs 
		 *  and TPTP symbols of different categories and arities.
		 *  @return nonnull
		 */
		public
		    Map<String,Set<Converter.TPTPSymbolDescriptor>> 
		    URIsToTPTPSymbols() {
		    return _URIsToTPTPSymbols;
		}
				
		/** Set of reserved TPTP identifiers that the converter
		 *  cannot use to map new URIs.
		 */
		public 
		    Set<Converter.TPTPSymbolDescriptor> otherReservedSymbols() {
		    return _otherReservedSymbols;
		}

		/** Id for the predicate for all the data values; can be null. */
		public String dataDomainPredicate() {
		    return _dataDomainPredicate;
		}
		
		/** Sets the id for the predicate for all the data values. */
		public void setDataDomainPredicate(String pred) {
		    _dataDomainPredicate = pred;
		}
		
		/** Id for the equality predicate. */
		public String equalityPredicate() {
		    return _equalityPredicate;
		}
		
		/** Sets the id for the equality predicate. */
		public void setEqualityPredicate(String pred) {
		    _equalityPredicate = pred;
		}
		
		/** Id for the constructor function for integer literals. */
		public String intLitFunc() {
		    return _intLitFunc;
		}

		/** Sets the id for the constructor function for integer literals. */
		public void setIntLitFunc(String func) {
		    _intLitFunc = func;
		}

		/** Id for the constructor function for string literals
		 *  with no language specified.
		 */
		public String stringLitNoLangFunc() {
		    return _stringLitNoLangFunc;
		}

		/** Sets the id for the constructor function for string literals
		 *  with no language specified.
		 */
		public void setStringLitNoLangFunc(String func) {
		    _stringLitNoLangFunc = func;
		}

		/** Id for the constructor function for string literals
		 *  with some language specified.
		 */
		public String stringLitWithLangFunc() { 
		    return _stringLitWithLangFunc;
		}
		
		/** Sets the id for the constructor function for string literals
		 *  with some language specified.
		 */
		public void setStringLitWithLangFunc(String func) { 
		    _stringLitWithLangFunc = func;
		}
		

		/** Id for the constructor function for typed data literals. */
		public String typedLitFunc() {
		    return _typedLitFunc;
		}

		/** Sets the id for the constructor function for typed data literals.
		 */
		public void setTypedLitFunc(String func) {
		    _typedLitFunc  = func;
		}

		
		//                 Data:

		/** Mapping that accumulates the correspondence between URIs 
		 *  and TPTP symbols of different categories and arities.
		 */
		private Map<String,Set<Converter.TPTPSymbolDescriptor>> _URIsToTPTPSymbols;

		/** Set of reserved TPTP identifiers that the converter
		 *  cannot use to map new URIs.
		 */
		private HashSet<Converter.TPTPSymbolDescriptor> _otherReservedSymbols;
		
		/** Id for the predicate for all the data values. */
		private String _dataDomainPredicate;
		
		/** Id for the equality predicate. */
		private String _equalityPredicate;

		/** Id for the constructor function for integer literals.*/
		private String _intLitFunc;

		/** Id for the constructor function for string literals
		 *  with no language specified.
		 */
		private String _stringLitNoLangFunc;

		/** Id for the constructor function for string literals
		 *  with some language specified.
		 */
		private String _stringLitWithLangFunc;

		/** Id for the constructor function for typed data literals. */
		private String _typedLitFunc;

	} // class Parameters

	public Converter(Parameters param,boolean generateCNF){
		assert param != null;
		_parameters = param;
		_generateCNF = generateCNF;
		_syntaxFactory = new SimpleTptpParserOutput();
		_nextVarIndex = 0;
		_nextSkolemSymIndex = 0;
		_nextAxiomIndex = 0;		
		_reservedSymbols = new HashSet<TPTPSymbolDescriptor>();

		// All symbols to which URIs are mapped, are reserved:
		for (Set<TPTPSymbolDescriptor> symDescSet :
			 _parameters.URIsToTPTPSymbols().values())
		    _reservedSymbols.addAll(symDescSet);
		
		// Explicitly reserved symbols:
		_reservedSymbols.addAll(_parameters.otherReservedSymbols());
		
		if (_parameters.dataDomainPredicate() != null)	    
		    _reservedSymbols.add(new TPTPSymbolDescriptor(true,
								  _parameters.dataDomainPredicate(),
								  1));

		if (_parameters.equalityPredicate() != null)	    
		    _reservedSymbols.add(new TPTPSymbolDescriptor(true,
								  _parameters.equalityPredicate(),
								  2));

		if (_parameters.stringLitNoLangFunc() != null)	    
		    _reservedSymbols.add(new TPTPSymbolDescriptor(false,
								  _parameters.stringLitNoLangFunc(),
								  1));

		if (_parameters.stringLitWithLangFunc() != null)	    
		    _reservedSymbols.add(new TPTPSymbolDescriptor(false,
								  _parameters.stringLitWithLangFunc(),
								  2));

		if (_parameters.typedLitFunc() != null)	    
		    _reservedSymbols.add(new TPTPSymbolDescriptor(false,
								  _parameters.typedLitFunc(),
								  2));
	
	} // Converter(Parameters param, boolean generateCNF)
		
	
	
    /** Converts all entries of the ontology into TPTP annotated formulas;
     *  note that imports are ignored. 
     */
    public 
	Collection<? extends SimpleTptpParserOutput.TopLevelItem> 
	convert(OWLOntology ontology) {
	
	_currentOntology = ontology;

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();


	//for (org.semanticweb.owlapi.model.OWLEntity entity : ontology.getReferencedEntities())
	for (OWLEntity entity : ontology.getSignature())
	    if (entity instanceof OWLClass)
		{
		    result.addAll(convert((OWLClass)entity));
		}
	    else if (entity instanceof OWLDatatype)
		{
		    result.addAll(convert((OWLDatatype)entity));
		}
	    else if (entity instanceof OWLObjectProperty)
		{
		    result.addAll(convert((OWLObjectProperty)entity));
		}
	    else if (entity instanceof OWLDataProperty)
		{
		    result.addAll(convert((OWLDataProperty)entity));
		}
	    else if (entity instanceof OWLIndividual)
		{
		//    result.addAll(convert((OWLIndividual)entity));
		};



	for (OWLLogicalAxiom axiom : ontology.getLogicalAxioms())
	    {
		if (axiom instanceof OWLClassAxiom)
		    {
			result.addAll(convert((OWLClassAxiom)axiom));
		    }
		else if (axiom instanceof OWLPropertyAxiom)
		    {
			result.addAll(convert((OWLPropertyAxiom)axiom));
		    }
		else if (axiom instanceof OWLIndividualAxiom)
		    {
		    //	result.addAll(convert((OWLIndividualAxiom)axiom));
		    }
		else if (axiom instanceof SWRLRule)
		    {
			result.addAll(convert((SWRLRule)axiom));
		    }
		else if (axiom instanceof OWLHasKeyAxiom)
	    {
			result.addAll(convert((OWLHasKeyAxiom)axiom));
	    }
		else 
		    throw new Error("Logical axiom of an unsupported kind : " + 
				    axiom);

	    }; // for (OWLLogicalAxiom axiom : ontology.getLogicalAxioms())

		/* Old implementation. Does not work because 
		   ontology.getObjectPropertyAxioms() does not return 
		   inverse property axioms for some reason. I suspect
		   a bug in OWL API itself.


		for (OWLClassAxiom classAxiomEntry : ontology.getClassAxioms())
		    result.addAll(convert(classAxiomEntry));
	 
		for (OWLPropertyAxiom propAxiomEntry : ontology.getObjectPropertyAxioms())	  
		    result.addAll(convert(propAxiomEntry));

		for (OWLPropertyAxiom propAxiomEntry : ontology.getDataPropertyAxioms())
		    result.addAll(convert(propAxiomEntry));


		for (OWLIndividualAxiom individualAxiomEntry : ontology.getIndividualAxioms())
		    result.addAll(convert(individualAxiomEntry));

		for (SWRLRule ruleEntry : ontology.getRules())
		    result.addAll(convert(ruleEntry));
		*/
		
		if (_generateCNF) return clausify(result);

		return result;

	    } // convert(OWLOntology ontology)

    /** Axioms that implicitly belong to any ontology,
     *  eg that owl:Thing contaings all entities, etc.
     *  The TPTP identifiers are assigned consistently with all other
     *  TPTP formulas produced by this object.
     */
    public 
	Collection<? extends SimpleTptpParserOutput.TopLevelItem>
	commonAxioms() {
	
	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();


	// ![X] : (owlThing(X) | dataDomain(X))
	TptpParserOutput.FofFormula formula = 
	    forAll("X",or(owlThingConstraint("X"),
			  dataDomConstraint("X")));

	SimpleTptpParserOutput.AnnotatedFormula axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'owl:Thing U dataDomain cover all model elements'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);


	// ?[X]: (owlThing(X))
	formula = exist("X",owlThingConstraint("X"));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'There is at least one individual'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);

	
	// ?[X]: (dataDomain(X))
	formula = exist("X",dataDomConstraint("X"));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'There is at least one data value'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);
	

	// ![X]: (~(owlThing(X) & dataDomain(X)))
	formula = forAll("X",not(and(owlThingConstraint("X"),
				     dataDomConstraint("X"))));

	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'owl:Thing and dataDomain are disjoint'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);

	
	
	// ![X]: (~owlNothing(X))
	formula = forAll("X",not(owlNothingConstraint("X")));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'owl:Nothing is empty'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);
	
	

	// ! [X,Y]: intLit(X) !=  stringLitNoLang(Y)

	TptpParserOutput.Term varX =
	    _syntaxFactory.createVariableTerm("X");

	TptpParserOutput.Term varY =
	    _syntaxFactory.createVariableTerm("Y");

	LinkedList<TptpParserOutput.Term> args =
	    new LinkedList<TptpParserOutput.Term>();
	args.add(varX);

	TptpParserOutput.Term intLitX = 
	    _syntaxFactory.createPlainTerm(intLitFunc(),args);

	args = new LinkedList<TptpParserOutput.Term>();
	args.add(varY);
	TptpParserOutput.Term stringLitNoLangY = 
	    _syntaxFactory.createPlainTerm(stringLitNoLangFunc(),args);
	

	formula = 
	    forAll("X","Y",not(equalityApplication(intLitX,stringLitNoLangY)));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'integer literals are distinct from string-without-language literals'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);
	

	
	// ! [X,Y]: intLit(X) !=  stringLitWithLang(Y)

	args = new LinkedList<TptpParserOutput.Term>();
	args.add(varY);
	TptpParserOutput.Term stringLitWithLangY = 
	    _syntaxFactory.createPlainTerm(stringLitWithLangFunc(),args);

	formula = 
	    forAll("X","Y",not(equalityApplication(intLitX,stringLitWithLangY)));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'integer literals are distinct from string-with-language literals'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);
	


	// ! [X,Y]: intLit(X) !=  typedLit(Y)

	args = new LinkedList<TptpParserOutput.Term>();
	args.add(varY);
	TptpParserOutput.Term typedLitY = 
	    _syntaxFactory.createPlainTerm(typedLitFunc(),args);

	formula = 
	    forAll("X","Y",not(equalityApplication(intLitX,typedLitY)));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'integer literals are distinct from general typed literals'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);
	



	// ! [X,Y]: stringLitNoLang(X) !=  stringLitWithLang(Y)

	args = new LinkedList<TptpParserOutput.Term>();
	args.add(varX);
	TptpParserOutput.Term stringLitNoLangX = 
	    _syntaxFactory.createPlainTerm(stringLitNoLangFunc(),args);
	
	formula = 
	    forAll("X","Y",not(equalityApplication(stringLitNoLangX,stringLitWithLangY)));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'string-without-language literals are distinct from string-with-language literals'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);



	// ! [X,Y]: stringLitNoLang(X) !=  typedLit(Y)

	formula = 
	    forAll("X","Y",not(equalityApplication(stringLitNoLangX,typedLitY)));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'string-without-language literals are distinct from general typed literals'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);



	// ! [X,Y]: stringLitWithLang(X) !=  typedLit(Y)
	args = new LinkedList<TptpParserOutput.Term>();
	args.add(varX);
	TptpParserOutput.Term stringLitWithLangX = 
	    _syntaxFactory.createPlainTerm(stringLitWithLangFunc(),args);
	
	formula = forAll("X",not(equalityApplication(stringLitWithLangX,typedLitY)));
	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'string-with-language literals are distinct from general typed literals'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);




	// ! [X,Y]: intLit(X) = intLit(Y) => X = Y
	

	args = new LinkedList<TptpParserOutput.Term>();
	args.add(varY);

	TptpParserOutput.Term intLitY = 
	    _syntaxFactory.createPlainTerm(intLitFunc(),args);

	formula = 
	    forAll("X",
		   "Y",
		   implies(equalityApplication(intLitX,intLitY),
			   equalityApplication(varX,varY)));

	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'intLit is a constructor'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);
	

	
	// ! [X,Y]: stringLitNoLang(X) = stringLitNoLang(Y) => X = Y

	formula = 
	    forAll("X",
		   "Y",
		   implies(equalityApplication(stringLitNoLangX,stringLitNoLangY),
			   equalityApplication(varX,varY)));

	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'stringLitNoLang is a constructor'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);
	

	


	// ! [X,Y]: stringLitWithLang(X) = stringLitWithLang(Y) => X = Y


	formula = 
	    forAll("X",
		   "Y",
		   implies(equalityApplication(stringLitWithLangX,stringLitWithLangY),
			   equalityApplication(varX,varY)));

	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'stringLitWithLang is a constructor'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);



	// ! [X,Y]: typedLit(X) = typedLit(Y) => X = Y

	args = new LinkedList<TptpParserOutput.Term>();
	args.add(varX);
	TptpParserOutput.Term typedLitX = 
	    _syntaxFactory.createPlainTerm(typedLitFunc(),args);

	formula = 
	    forAll("X",
		   "Y",
		   implies(equalityApplication(typedLitX,typedLitY),
			   equalityApplication(varX,varY)));

	axiom = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'typedLit is a constructor'",
			       TptpParserOutput.FormulaRole.Axiom,
			       formula,
			       null,
			       0);
	result.add(axiom);




	
	if (_generateCNF) return clausify(result);

	return result;

    } // commonAxioms()

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**********************************************************************************************************
	 * ********************************************************************************************************
	 * ********************************************************************************************************
	 */
	
	
	  //           Private classes:


    /** Converts an OWL description by visiting all its 
     *  possible concrete forms.
     */

	private class DescriptionVisitor implements OWLClassExpressionVisitor{
		
		/** @param argument term to be used as the argument of predicates,
		 *  corresponding to the implicit argument of concepts and 
		 *  properties.
		 */
		public DescriptionVisitor(TptpParserOutput.Term argument) {
		    _result = null;
		    _argument = argument;
		}

		/** @param variable will be converted to the term to be used as the argument 
		 *  of predicates, corresponding to the implicit argument of concepts and 
		 *  properties.
		 */
		public DescriptionVisitor(String variable) {
		    _result = null;
		    _argument =
			_syntaxFactory.createVariableTerm(variable);
		}

		/** The result of the last visit. */
		public SimpleTptpParserOutput.Formula result() {
		    return _result;
		}

		
		
		public void visit(OWLClass node) {
			_result = classPredicateApplication(node,_argument);

		} // visit(OWLClass node)


		public void visit(OWLObjectIntersectionOf node) {
			
			 if (node.getOperands().size() == 0)
				{
				    _result = builtInTrue();
				    return;
				};
			    
			    Iterator iter = node.getOperands().iterator();
			    OWLClassExpression subdesc = (OWLClassExpression)iter.next();
			    subdesc.accept(this);
			    SimpleTptpParserOutput.Formula accumulatedResult = _result;
			    while (iter.hasNext())
				{
				    subdesc = (OWLClassExpression)iter.next();
				    subdesc.accept(this);
				    accumulatedResult = and(accumulatedResult,_result);
				};
			    
			    _result = accumulatedResult;

			} // visit(OWLObjectIntersectionOf node)


		public void visit(OWLObjectUnionOf node) {
			if (node.getOperands().size() == 0)
			{
			    _result = builtInFalse();
			    return;
			};
		    
		    Iterator iter = node.getOperands().iterator();
		    OWLClassExpression subdesc = (OWLClassExpression)iter.next();
		    subdesc.accept(this);
		    SimpleTptpParserOutput.Formula accumulatedResult = _result;
		    while (iter.hasNext())
			{
			    subdesc = (OWLClassExpression)iter.next();
			    subdesc.accept(this);
			    accumulatedResult = or(accumulatedResult,_result);
			};
		    
		    _result = accumulatedResult;

		} // visit(OWLObjectUnionOf node) 


		public void visit(OWLObjectComplementOf node) {
			// IMPORTANT: complemention only within the abstract domain!
		    
		    // ~(C(X)) & owlThing(X)

		    node.getOperand().accept(this);
		    // Here _result contains C(X).

		    _result = and(not(_result),owlThingConstraint(_argument));

		} // visit(OWLObjectComplementOf node)


		public void visit(OWLObjectSomeValuesFrom node) {
			// ?[Y] : prop(X,Y) & C(Y)
		    
		    String newVar = generateNewVariable("Y");

		    TptpParserOutput.FofFormula propApplication = 
			propertyPredicateApplication(node.getProperty(),
						     _argument,
						     newVar);

		    DescriptionVisitor conceptVis = new DescriptionVisitor(newVar);
		    node.getFiller().accept(conceptVis);
		    

		    TptpParserOutput.FofFormula matrix = 
			and(propApplication,conceptVis.result());

		    _result = exist(newVar,matrix);

		} // visit(OWLObjectSomeValuesFrom node) 


		public void visit(OWLObjectAllValuesFrom node) {
			// node represents "all X such that prop(X,Y) implies C(Y),
		    // where C is a concept
		    
		    String newVar = generateNewVariable("Y");

		    // prop(X,Y)
		    TptpParserOutput.FofFormula condition =
			propertyPredicateApplication(node.getProperty(),
						     _argument,
						     newVar);
		    

		    DescriptionVisitor vis = new DescriptionVisitor(newVar);	    
		    node.getFiller().accept(vis);
		    // vis.result() contains C(Y) where C is the range of the restriction 

		    TptpParserOutput.FofFormula dataAllRestrictionFormula = 
			implies(condition,vis.result());
		    
		    // Universally quantify over newVar:
		    dataAllRestrictionFormula = forAll(newVar,dataAllRestrictionFormula);
			
		    
		    // owlThing(X) is necessary to ensure that the concept
		    // does not apply to data values.
		    _result = and(owlThingConstraint(_argument),
				  dataAllRestrictionFormula);
		    
		} // visit(OWLObjectAllRestriction node)
		

		public void visit(OWLObjectHasValue node) {
			 // prop(X,c) 

		    TptpParserOutput.Term individualTerm = convertIndividual(node.getValue());
   
		    _result = propertyPredicateApplication(node.getProperty(),
						     _argument,
						     individualTerm);


		} // visit(OWLObjectHasValue node) 


		public void visit(OWLObjectMinCardinality node) {
			 _result = atLeastRestriction(node.getProperty(),node.getCardinality());

		} // visit(OWLObjectMinCardinality node)


		public void visit(OWLObjectExactCardinality node) {
			 _result = and(atLeastRestriction(node.getProperty(),node.getCardinality()),atMostRestriction(node.getProperty(),node.getCardinality()));
					    
		} // visit(OWLObjectExactCardinality node)


		public void visit(OWLObjectMaxCardinality node) {
			_result = atMostRestriction(node.getProperty(),node.getCardinality());
				    
		} // visit(OWLObjectMaxCardinality node)


		public void visit(OWLObjectHasSelf node) {
			// prop(X,X)

		     _result = 
			propertyPredicateApplication(node.getProperty(),
						     _argument,
						     _argument);

		} // visit(OWLObjectHasSelf node)


		public void visit(OWLObjectOneOf node) {
		   
			if (node.getIndividuals().isEmpty())
			{
			    _result = builtInFalse();
			    return;
			};
		    
		    Iterator<OWLIndividual> indIter = node.getIndividuals().iterator();
		    
		    TptpParserOutput.Term individualTerm = convertIndividual(indIter.next());

		    _result = equalityApplication(_argument,individualTerm);

		    while (indIter.hasNext())
			{
			    individualTerm = convertIndividual(indIter.next());
			    
			    _result = or(_result,equalityApplication(_argument,individualTerm));
			};		    

		} // visit(OWLObjectOneOf node)

		public void visit(OWLDataSomeValuesFrom node) {
		    // ?[Y] : prop(X,Y) & D(Y)
		    
		    String newVar = generateNewVariable("Y");

		    TptpParserOutput.FofFormula propApplication = 
			propertyPredicateApplication(node.getProperty(),
						     _argument,
						     newVar);

		    DataVisitor datatypeVis = new DataVisitor(newVar);
		    node.getFiller().accept(datatypeVis);
		    

		    TptpParserOutput.FofFormula matrix = 
			and(propApplication,datatypeVis.result());

		    _result = exist(newVar,matrix);

		} // visit(OWLDataSomeValuesFrom node) 


		public void visit(OWLDataAllValuesFrom node) {
			// node represents "all X such that prop(X,Y) implies D(Y),
		    // where D is a datatype

			
		    String newVar = generateNewVariable("Y");

		    // prop(X,Y)
		    TptpParserOutput.FofFormula condition =
			propertyPredicateApplication(node.getProperty(),
						     _argument,
						     newVar);


		    DataVisitor vis = new DataVisitor(newVar);	    
		    node.getFiller().accept(vis);
		    // vis.result() contains D(Y) where D is the range of the restriction 

		    TptpParserOutput.FofFormula dataAllRestrictionFormula = 
			implies(condition,vis.result());
		    
		    // Universally quantify over newVar:
		    dataAllRestrictionFormula = forAll(newVar,dataAllRestrictionFormula);
			
		    
		    // owlThing(X) is necessary to ensure that the concept
		    // does not apply to data values.
		    _result = and(owlThingConstraint(_argument),
				  dataAllRestrictionFormula);

		} // visit(OWLDataAllValuesFrom node)

		public void visit(OWLDataHasValue node) {
		    // prop(X,c) 

		    TptpParserOutput.Term valueTerm = convertDataValue(node.getValue());
	    
		    _result = propertyPredicateApplication(node.getProperty(),
						     _argument,
						     valueTerm);

		} // visit(OWLDataHasValue node) 


		public void visit(OWLDataMinCardinality node) {
			  _result = atLeastRestriction(node.getProperty(),
								   node.getCardinality());
					    
					} // visit(OWLDataMinCardinality node)


		public void visit(OWLDataExactCardinality node) {
			 _result = and(atLeastRestriction(node.getProperty(),
								       node.getCardinality()),
						    atMostRestriction(node.getProperty(),
								      node.getCardinality()));
					    
					} // visit(OWLDataExactCardinality node)


		public void visit(OWLDataMaxCardinality node) {
			_result = atMostRestriction(node.getProperty(),
							  node.getCardinality());

				} // visit(OWLDataMaxCardinality node)


		
		//                 Private methods:
		
		
		
		/** ?[Y_1 .. Y_n] : prop(X,Y_1) & .. & prop(X,Y_n) */
		private 
		    SimpleTptpParserOutput.Formula 
		    atLeastRestriction(OWLPropertyExpression property,int n) {
			//System.out.println(property.toString() + " -- " + n);
		    if (n == 0) 
			return builtInTrue();

		    LinkedList<String> newVars = new LinkedList<String>();
		    String newVar = generateNewVariable("Y");
		    newVars.addLast(newVar);

		    TptpParserOutput.FofFormula result = 
			propertyPredicateApplication(property,_argument,newVar);
		    //System.out.println(_argument.toString() + " -- " + newVar);
		    for (int i = 1; i < n; ++i)
			{
			    newVar = generateNewVariable("Y");
			    //System.out.println(_argument.toString() + " -- " + newVar);
			    newVars.addLast(newVar);
			    result = and(result,
					 propertyPredicateApplication(property,
								      _argument,
								      newVar));
			}; // for (int i = 0; i < n; ++i)
		
		    

		    // Existentially quantify over newVar:
		    result = exist(newVars,result);

		    return (SimpleTptpParserOutput.Formula)result;
		    
		} // atLeastRestriction(OWLPropertyExpression property,int n)
	
		/**  ![Y_1 .. Y_n,Z] : 
		 *     (prop(X,Z) & prop(X,Y_1) & .. & prop(X,Y_n)  
		 *       => 
		 *      Z == Y_1 | .. | Z == Y_n) 
	         *     & 
	         *   owlThing() 
	         */
		private 
		    SimpleTptpParserOutput.Formula 
		    atMostRestriction(OWLPropertyExpression property,int n) {
		
		    if (n == 0) 
			return builtInFalse();

		    LinkedList<String> newVars = new LinkedList<String>();
		    
		    String zVar = generateNewVariable("Z");
		    newVars.addLast(zVar);
		    
		    // prop(X,Z)
		    TptpParserOutput.FofFormula condition =
			propertyPredicateApplication(property,_argument,zVar);

		    String yVar = generateNewVariable("Y");
		    newVars.addLast(yVar);

		    // prop(X,Z) & prop(X,Y_1)
		    condition =
			and(condition,
			    propertyPredicateApplication(property,
							 _argument,
							 yVar));
			    
	          
		    // Z == Y_1
		    TptpParserOutput.FofFormula conclusion = 
			equalityApplication(zVar,yVar);

		    for (int i = 1; i < n; ++i)
			{
			    yVar = generateNewVariable("Y");
			    newVars.addLast(yVar);

			    // .. & prop(X,Y_i)
			    condition = 
				and(condition,
				    propertyPredicateApplication(property,
								 _argument,
								 yVar));
			    // .. | Z == Y_i
			    conclusion = 
				and(conclusion,equalityApplication(zVar,yVar));
			}; // for (int i = 0; i < n; ++i)

		    
		    // condition => conclusion
		    TptpParserOutput.FofFormula result = implies(condition,conclusion);

		    // Universally quantify over newVars:
		    result = forAll(newVars,result);

		    return (SimpleTptpParserOutput.Formula)result;

		} // atMostRestriction(OWLPropertyExpression property,int n)

		
		
		
		
		
		
		
		
		
		
		
		//                     Data: 


		/** Keeps the result of every visit until the next visit. */
		private SimpleTptpParserOutput.Formula _result;

		/** The term to be used as the argument of predicates,
		 *  corresponding to the implicit argument of concepts and 
		 *  properties.
		 */
		private final TptpParserOutput.Term _argument;

		
	} //class DescriptionVisitor

	
	
	/**********************************************************************************************************
	 * ********************************************************************************************************
	 * ********************************************************************************************************
	 */

	
	
	
	/** Converts an OWL datatype or range by visiting all its 
     *  possible concrete forms.
     */
    private class DataVisitor implements OWLDataVisitor{
    	
    	/** @param argument term to be used as the argument of predicates,
    	 *  corresponding to the implicit argument of concepts and 
    	 *  properties.
    	 */
    	public DataVisitor(TptpParserOutput.Term argument) {
    	    _result = null;
    	    _argument = argument;
    	}

    	/** @param variable will be converted to the term to be used as the argument 
    	 *  of predicates, corresponding to the implicit argument of concepts and 
    	 *  properties.
    	 */
    	public DataVisitor(String variable) {
    	    _result = null;
    	    _argument =
    		_syntaxFactory.createVariableTerm(variable);
    	}

    	/** The result of the last visit. */
    	public SimpleTptpParserOutput.Formula result() {
    	    return _result;
    	}

		public void visit(OWLDatatype node) {
			// type(X)

		    _result = atom(predicateForDatatype(node),_argument);

		} // visit(OWLDataType node)


		private String predicateForDatatype(OWLDatatype t) 
	    {
			return predicateForURI(t.getIRI(),1);
		}

		/** Maps the URI as a predicate of the specified arity into a TPTP identifier;
	     *  the mapping is consistent with the previous identifier assignments.
	     */
		private String predicateForURI(IRI uri, int arity) {
			Set<TPTPSymbolDescriptor> symDescSet = 
				    _parameters.URIsToTPTPSymbols().get(uri.toString());

				if (symDescSet == null)
				    {
					symDescSet = new HashSet<TPTPSymbolDescriptor>();
					_parameters.URIsToTPTPSymbols().put(uri.toString(),symDescSet);
				    }
				else
				    {
					// Check if the predicate has already been mapped:
					for (TPTPSymbolDescriptor desc : symDescSet)
					    if (desc.isPredicate() && desc.arity() == arity)
						return desc.name();
				    };

				String result = tptpfyURI(uri,"p_");
				
				if (symbolIsReserved(true,result,arity))
				    {
					int i;
					for (i = 0;
					     symbolIsReserved(true,result + i,arity);
					     ++i);
					result = result + i;
				    };

				TPTPSymbolDescriptor desc = 
				    new TPTPSymbolDescriptor(true,result,arity);

				symDescSet.add(desc);

				_reservedSymbols.add(desc);
			     

				return result;

			    } // predicateForURI(URI uri,int arity)


		/** Checks if a symbol with the specified parameters 
	     *  has already been reserved in some way, so that a new
	     *  symbol with these parameters cannot be introduced.
	     */
	    private boolean symbolIsReserved(boolean predicate,
					     String name,
					     int arity) {

		TPTPSymbolDescriptor desc = 
		    new TPTPSymbolDescriptor(predicate,name,arity);

		return _reservedSymbols.contains(desc);

	    } // symbolIsReserved(boolean predicate,..)


		private String tptpfyURI(IRI uri,String prefix) {
			if (uri.getFragment() == null)
		    {
			String pathSuffix = pathSuffix(uri.toURI().getPath());
			if (!pathSuffix.equals(""))
			    return prefix + pathSuffix;		    
			
			return "'" + uri.toString() + "'";
		    };
		
		return tptpfyId(prefix + uri.getFragment()); 

	    } // tptpfyURI(URI uri,String prefix)
	    

		private String tptpfyId(String id) {
			String res = id;

			res = res.replace('-','_');
			res = res.replace('%','_');
			res = res.replace('.','_');
			
			return res;

		    } // tptpfyId(String id)
		    

		/** Extracts the last segment of the URI path, ie, the substring
	     *  (possibly empty) following the last '/' in the path.
	     *  @param path can be null, in which case the result is ""
	     */
		private String pathSuffix(String path) {
			if (path == null) return "";
			
			for (int i = path.length() - 1; i >= 0; --i)
			    if (path.charAt(i) == '/')
				return path.substring(i + 1);
			
			// No '/'
			
			return path;

		    } // pathSuffix(String path)


		public void visit(OWLDataOneOf node) {
		    // X == c1 | .. | X == cn

		    if (node.getValues().isEmpty())
			{
			    _result = builtInFalse();
			    return;
			};

		    //Iterator<OWLConstant> valIter = node.getValues().iterator();
			//FIXED
		    Iterator<OWLLiteral> valIter = node.getValues().iterator();
		    
		    TptpParserOutput.Term valueTerm = convertDataValue(valIter.next());

		    _result = equalityApplication(_argument,valueTerm);

		    while (valIter.hasNext())
			{
			    valueTerm = convertDataValue(valIter.next());
			    
			    _result = or(_result,equalityApplication(_argument,valueTerm));
			};

		} // visit(OWLDataOneOf node)

		public void visit(OWLDataComplementOf node) {
			// ~range 
			
			node.getDataRange().accept(this);
			_result = not(_result);
			
		} // visit(OWLDataComplementOf node)



		public void visit(OWLDataIntersectionOf node) {			
			throw new Error("visit(OWLLiteral node) should not be called.");
		}

		public void visit(OWLDataUnionOf node) {			
			throw new Error("visit(OWLLiteral node) should not be called.");
		}

		public void visit(OWLDatatypeRestriction node) {
		    // range(X) & facet1(X,c1) & .. & facetn(X,cn)
		    
		    //node.getDataRange().accept(this); // range(X)
			//fixed
			node.getDatatype().accept(this); // range(X)
			
		    SimpleTptpParserOutput.Formula result = _result;
		    

		    //for (OWLDataRangeFacetRestriction facetRestr :node.getFacetRestrictions())
		    //fixed
		    for (OWLFacetRestriction facetRestr :node.getFacetRestrictions())
		    	
			{
			    visit(facetRestr);
			    result = and(result,_result);
			};
			
		    _result = result;

		} // visit(OWLDatatypeRestriction node)
		
		//formerly OWLTypedConstant and OWLUntypedConstant
		public void visit(OWLLiteral node) {
			// TODO Auto-generated method stub
			throw new Error("visit(OWLLiteral node) should not be called.");
			
		} // visit(OWLLiteral node)

		public void visit(OWLFacetRestriction node) {
			// facet(X,c)
		    
		    //OWLRestrictedDataRangeFacetVocabulary facet = node.getFacet();
			OWLFacet facet = node.getFacet();
		    
		    //_result = atom(predicateForURI(facet.getURI(),2),_argument,convertDataValue(node.getFacetValue()));
			//fixed
		    _result = atom(predicateForURI(facet.getIRI(),2),_argument,convertDataValue(node.getFacetValue()));

		} // visit(OWLFacetRestriction node)
    	

		
		private SimpleTptpParserOutput.Formula not(TptpParserOutput.FofFormula form) {
			
			return (SimpleTptpParserOutput.Formula)_syntaxFactory.createNegationOf(form);

	    } // not(TptpParserOutput.FofFormula form)

		
    	//                     Data: 

    	/** Keeps the result of every visit until the next visit. */
    	private SimpleTptpParserOutput.Formula _result;


    	/** The term to be used as the argument of predicates,
    	 *  corresponding to the implicit argument of concepts and 
    	 *  properties.
    	 */
    	private final TptpParserOutput.Term _argument;

    } //class DataVisitor
	
	
	
	
	
    /**********************************************************************************************************
	 * ********************************************************************************************************
	 * ********************************************************************************************************
	 */

    
    
    
    
    
    /** Converts an OWL axiom by visiting all its 
     *  possible concrete forms.
     */
    private class AxiomVisitor implements OWLAxiomVisitor{
    	
    	public AxiomVisitor() {}

    	/** The result of the last visit. */
    	public SimpleTptpParserOutput.Formula result() {
    	    return _result;
    	}

 
		public void visit(OWLAnnotationAssertionAxiom axiom) {
			throw 
			new Error("visit(OWLEntityAnnotationAxiom axiom) should not be called.");

		} // visit(OWLEntityAnnotationAxiom axiom)

		public void visit(OWLSubAnnotationPropertyOfAxiom axiom) {
			throw 
			new Error("visit(OWLSubAnnotationPropertyOfAxiom axiom) should not be called.");

		} // visit(OWLSubAnnotationPropertyOfAxiom axiom)


		public void visit(OWLAnnotationPropertyDomainAxiom axiom) {
			throw 
			new Error("visit(OWLAnnotationPropertyDomainAxiom axiom) should not be called.");

		} // visit(OWLAnnotationPropertyDomainAxiom axiom)


		public void visit(OWLAnnotationPropertyRangeAxiom axiom) {
			throw 
			new Error("visit(OWLAnnotationPropertyRangeAxiom axiom) should not be called.");

		} // visit(OWLAnnotationPropertyRangeAxiom axiom)


		public void visit(OWLDeclarationAxiom axiom) {
			throw 
			new Error("visit(OWLDeclarationAxiom axiom) should not be called.");

		} // visit(OWLDeclarationAxiom axiom) 


		public void visit(OWLSubClassOfAxiom axiom) {
			// ![X] : C1(X) => C2(X)

		    
		    DescriptionVisitor descVis1 = new DescriptionVisitor("X");
		    DescriptionVisitor descVis2 = new DescriptionVisitor("X");
		    
		    axiom.getSubClass().accept(descVis1);
		    axiom.getSuperClass().accept(descVis2);
		    
		    _result = forAll("X",implies(descVis1.result(),descVis2.result()));
		    
		} // visit(OWLSubClassAxiom axiom)


		public void visit(OWLNegativeObjectPropertyAssertionAxiom axiom) {
			// ~prop(subj,obj)

		    _result =
			not(propertyPredicateApplication(axiom.getProperty(),
							 convertIndividual(axiom.getSubject()),
							 convertIndividual(axiom.getObject())));
						     
			
		} // visit(OWLNegativeObjectPropertyAssertionAxiom axiom) 


		public void visit(OWLAsymmetricObjectPropertyAxiom axiom) {
			// ! [X,Y,Z] : prop(X,Y) & prop(Y,X) => X = Y
		    
		    TptpParserOutput.FofFormula matrix = 
			implies(and(propertyPredicateApplication(axiom.getProperty(),
								 "X",
								 "Y"),
				    propertyPredicateApplication(axiom.getProperty(),
								 "Y",
								 "X")),
				equalityApplication("X","Y"));
		    _result = forAll("X","Y",matrix);
		    
		} // visit(OWLAsymmetricObjectPropertyAxiom axiom) 


		public void visit(OWLReflexiveObjectPropertyAxiom axiom) {
			// ! [X] : prop(X,X)

		    _result = 
			forAll("X",
			       propertyPredicateApplication(axiom.getProperty(),
							    "X",
							    "X"));

		} // visit(OWLReflexiveObjectPropertyAxiom axiom) 


		public void visit(OWLDisjointClassesAxiom axiom) {
			if (axiom.getClassExpressions().size() <= 1)
			{
			    _result = builtInTrue();
			    return;
			};

		    // ![X] : ~(C1(X) & C2(X)) & ~(C2(X) & C3(X)) & .. & ~(Cn-1(X) & Cn(X))
		    
		    
		    Object[] descriptions = axiom.getClassExpressions().toArray();

		    DescriptionVisitor descVis1 = new DescriptionVisitor("X");
		    DescriptionVisitor descVis2 = new DescriptionVisitor("X");
		    
		    ((OWLClassExpression)descriptions[0]).accept(descVis1);
		    ((OWLClassExpression)descriptions[1]).accept(descVis2);

		    // ~(C1(X) & C2(X))
		    _result = not(and(descVis1.result(),descVis2.result()));

		    for (int i = 1; i + 1 < descriptions.length; ++i)
			{
			    ((OWLClassExpression)descriptions[i]).accept(descVis1);
			    ((OWLClassExpression)descriptions[i+1]).accept(descVis2);
			    _result = and(_result,
					  equivalent(descVis1.result(),descVis2.result()));
			};
		    
		    _result = forAll("X",_result);


		} // visit(OWLDisjointClassesAxiom axiom)


		public void visit(OWLDataPropertyDomainAxiom axiom) {
			// ! [X,Y] : prop(X,Y) => C(X)
		    
		    DescriptionVisitor domVisitor = new DescriptionVisitor("X");
		    axiom.getDomain().accept(domVisitor);

		    TptpParserOutput.FofFormula matrix = 
			implies(propertyPredicateApplication(axiom.getProperty(),
							     "X",
							     "Y"),
				domVisitor.result());
		    
		    
		    _result = forAll("X","Y",matrix);

		} // visit(OWLDataPropertyDomainAxiom axiom)


		public void visit(OWLObjectPropertyDomainAxiom axiom) {
			// ! [X,Y] : prop(X,Y) => C(X)
		    
		    DescriptionVisitor domVisitor = new DescriptionVisitor("X");
		    axiom.getDomain().accept(domVisitor);

		    TptpParserOutput.FofFormula matrix = 
			implies(propertyPredicateApplication(axiom.getProperty(),
							     "X",
							     "Y"),
				domVisitor.result());
		    
		    
		    _result = forAll("X","Y",matrix);

		} // visit(OWLObjectPropertyDomainAxiom axiom)


		public void visit(OWLEquivalentObjectPropertiesAxiom axiom) {
			if (axiom.getProperties().size() <= 1)
			{
			    _result = builtInTrue();
			    return;
			};

		    // ![X,Y] : (prop1(X,Y) <=> prop2(X,Y)) & 
		    //          (prop2(X,Y) <=> prop3(X,Y)) & .. & 
		    //          (propn-1(X,Y) <=> propn(X,Y))
		    
		    
		    Object[] prop = axiom.getProperties().toArray();

		    // prop1(X,Y) <=> prop2(X,Y)
		    
		    _result = 
			equivalent(propertyPredicateApplication((OWLProperty)prop[0],
								"X",
								"Y"),
				   
				   propertyPredicateApplication((OWLProperty)prop[1],
								"X",
								"Y"));

		    for (int i = 1; i + 1 < prop.length; ++i)
			{
			    _result = and(_result,
					  equivalent(propertyPredicateApplication((OWLProperty)prop[i],
										  "X",
										  "Y"),
						     
						     propertyPredicateApplication((OWLProperty)prop[i+1],
										  "X",
										  "Y")));
			};
		    

		    _result = forAll("X","Y",_result);

		} // visit(OWLEquivalentObjectPropertiesAxiom axiom)


		public void visit(OWLNegativeDataPropertyAssertionAxiom axiom) {
			// ~prop(subj,obj)

		    _result =
			not(propertyPredicateApplication(axiom.getProperty(),
							 convertIndividual(axiom.getSubject()),
							 convertDataValue(axiom.getObject())));
						     
			
		} // visit(OWLNegativeDataPropertyAssertionAxiom axiom) 


		public void visit(OWLDifferentIndividualsAxiom axiom) {
			if (axiom.getIndividuals().size() <= 1)
			{
			    _result = builtInTrue();
			    return;
			};

		    // ind1 != ind2 & ind1 != ind3 & .. & indn-1 != indn
		    
		    Object[] individuals = axiom.getIndividuals().toArray();
		    _result = null;

		    for (int i = 0; i + 1 < individuals.length; ++i)    
			for (int j = i + 1; j < individuals.length; ++j)
			    if (_result == null)
				{
				    // First disequality:
				    _result =
					not(equalityApplication(convertIndividual((OWLIndividual)
										  individuals[i]),
								convertIndividual((OWLIndividual)
										  individuals[j])));
				}
			    else
				_result =
				    and(_result,
					not(equalityApplication(convertIndividual((OWLIndividual)
										  individuals[i]),
								convertIndividual((OWLIndividual)
										  individuals[j]))));


		} // visit(OWLDifferentIndividualsAxiom axiom)


		public void visit(OWLDisjointDataPropertiesAxiom axiom) {
			if (axiom.getProperties().size() <= 1)
			{
			    _result = builtInTrue();
			    return;
			};


		    // ![X,Y] : ~(prop1(X,Y) & prop2(X,Y)) & 
		    //          ~(prop1(X,Y) & prop3(X,Y)) & 
		    //               .. &
	            //          ~(propn-1(X,Y) & propn(X,Y))
		    

		    Object[] prop = axiom.getProperties().toArray();

		    _result = null;

		    for (int i = 0; i + 1 < prop.length; ++i)
			for (int j = i + 1; j < prop.length; ++j)
			    if (_result == null)
				{
				    _result = 
					not(and(propertyPredicateApplication((OWLDataPropertyExpression)prop[i],
									     "X",
									     "Y"),
						propertyPredicateApplication((OWLDataPropertyExpression)prop[j],
									     "X",
									     "Y")));
				}
			    else
				_result = 
				    and(_result,
					not(and(propertyPredicateApplication((OWLDataPropertyExpression)prop[i],
									     "X",
									     "Y"),
						propertyPredicateApplication((OWLDataPropertyExpression)prop[j],
									     "X",
									     "Y"))));
			    
		    _result = forAll("X","Y",_result);

		} // visit(OWLDisjointDataPropertiesAxiom axiom) 


		public void visit(OWLDisjointObjectPropertiesAxiom axiom) {
			if (axiom.getProperties().size() <= 1)
			{
			    _result = builtInTrue();
			    return;
			};


		    // ![X,Y] : ~(prop1(X,Y) & prop2(X,Y)) & 
		    //          ~(prop1(X,Y) & prop3(X,Y)) & 
		    //               .. &
	            //          ~(propn-1(X,Y) & propn(X,Y))
		    

		    Object[] prop = axiom.getProperties().toArray();

		    _result = null;

		    for (int i = 0; i + 1 < prop.length; ++i)
			for (int j = i + 1; j < prop.length; ++j)
			    if (_result == null)
				{
				    _result = 
					not(and(propertyPredicateApplication((OWLPropertyExpression)prop[i],
									     "X",
									     "Y"),
						propertyPredicateApplication((OWLPropertyExpression)prop[j],
									     "X",
									     "Y")));
				}
			    else
				_result = 
				    and(_result,
					not(and(propertyPredicateApplication((OWLPropertyExpression)prop[i],
									     "X",
									     "Y"),
						propertyPredicateApplication((OWLPropertyExpression)prop[j],
									     "X",
									     "Y"))));
			    
		    _result = forAll("X","Y",_result);

		} // visit(OWLDisjointObjectPropertiesAxiom axiom) 


		public void visit(OWLObjectPropertyRangeAxiom axiom) {
			 // ! [X,Y] : prop(X,Y) => C(Y)
		    
		    DescriptionVisitor rangeVisitor = new DescriptionVisitor("Y");
		    axiom.getRange().accept(rangeVisitor);

		    TptpParserOutput.FofFormula matrix = 
			implies(propertyPredicateApplication(axiom.getProperty(),
							     "X",
							     "Y"),
				rangeVisitor.result());
		    
		    
		    _result = forAll("X","Y",matrix);

		} // visit(OWLObjectPropertyRangeAxiom axiom) 


		public void visit(OWLObjectPropertyAssertionAxiom axiom) {
			// prop(subj,obj)

		    _result =
			propertyPredicateApplication(axiom.getProperty(),
						     convertIndividual(axiom.getSubject()),
						     convertIndividual(axiom.getObject()));
						     

		} // visit(OWLObjectPropertyAssertionAxiom axiom)


		public void visit(OWLFunctionalObjectPropertyAxiom axiom) {
			// ! [X,Y,Z] : prop(X,Y) & prop(X,Z) => Y = Z

		    TptpParserOutput.FofFormula condition = 
			and(propertyPredicateApplication(axiom.getProperty(),
							 "X",
							 "Y"),
			    propertyPredicateApplication(axiom.getProperty(),
							 "X",
							 "Z"));

		    TptpParserOutput.FofFormula matrix = 
			implies(condition,
				equalityApplication("Y","Z"));
		   	    
		    _result = forAll("X","Y","Z",matrix);
		    
		} // visit(OWLFunctionalObjectPropertyAxiom axiom) 


		public void visit(OWLSubObjectPropertyOfAxiom axiom) {
			// ![X,Y] : prop1(X,Y) => prop2(X,Y)
		    
		    OWLObjectPropertyExpression prop1 = axiom.getSubProperty();
		    OWLObjectPropertyExpression prop2 = axiom.getSuperProperty();

		    _result = implies(propertyPredicateApplication(prop1,
								   "X",
								   "Y"),
				      propertyPredicateApplication(prop2,
								   "X",
								   "Y"));

		    _result = forAll("X","Y",_result);

		} // visit(OWLObjectSubPropertyAxiom axiom)


		public void visit(OWLDisjointUnionAxiom axiom) {

		    // (![X] : C(X) <=> C0(X) | .. | Cn(X))
		    //            &
		    // (![X] : ~(C0(X) & C1(X)) &
		    //         ~(C0(X) & C2(X)) & 
		    //                 .. &
		    //         ~(Cn-1(X) & Cn(X))

		    

		    Object[] descriptions = axiom.getClassExpressions().toArray();

		    DescriptionVisitor classVis = new DescriptionVisitor("X");
		    axiom.getOWLClass().accept(classVis);

		    
		    SimpleTptpParserOutput.Formula disjunction = null;

		    
		    for (int i = 0; i < descriptions.length; ++i)
			{
				DescriptionVisitor descVis = new DescriptionVisitor("X");
				((OWLClassExpression)descriptions[i]).accept(descVis);

				if (disjunction == null)
				    {
					disjunction = descVis.result();
				    }
				else
				    disjunction = or(disjunction,descVis.result());
						   
			}; // for (int i = 0; i + 1 < descriptions.length)

		    
		    if (disjunction == null)
			disjunction = builtInFalse();

		    SimpleTptpParserOutput.Formula equivalence = 
			equivalent(classVis.result(),disjunction);
		    

		    SimpleTptpParserOutput.Formula disjointness = null;

		    for (int i = 0; i + 1 < descriptions.length; ++i)
			{
				DescriptionVisitor descVis1 = new DescriptionVisitor("X");
				((OWLClassExpression)descriptions[i]).accept(descVis1);

				for (int j = i + 1; j < descriptions.length; ++j)
				    {
					DescriptionVisitor descVis2 = new DescriptionVisitor("X");
					((OWLClassExpression)descriptions[j]).accept(descVis2);
					
					if (disjointness == null)
					    {
						disjointness = 
						    not(and(descVis1.result(),
							    descVis2.result()));
					    }
					else
					    disjointness = 
						and(disjointness,
						    not(and(descVis1.result(),
							    descVis2.result())));
				    }; // for (int j = i + 1; j < descriptions.length; ++j)
			}; // for (int i = 0; i + 1 < descriptions.length; ++i)

		    
		    if (disjointness == null)
			disjointness = builtInTrue();

		    _result = and(forAll("X",equivalence),
				  forAll("X",disjointness));

		} // visit(OWLDisjointUnionAxiom axiom) 

		public void visit(OWLSymmetricObjectPropertyAxiom axiom) {
			// ! [X,Y,Z] : prop(X,Y) => prop(Y,X)
		    
		    TptpParserOutput.FofFormula matrix = 
			implies(propertyPredicateApplication(axiom.getProperty(),
							     "X",
							     "Y"),
				propertyPredicateApplication(axiom.getProperty(),
							     "Y",
							     "X"));
		    _result = forAll("X","Y",matrix);
		    
		} // visit(OWLSymmetricObjectPropertyAxiom axiom) 


		public void visit(OWLDataPropertyRangeAxiom axiom) {
			// ! [X,Y] : prop(X,Y) => type(Y)

		    DataVisitor datatypeVisitor = new DataVisitor("Y");
		    axiom.getRange().accept(datatypeVisitor);
		    
		    TptpParserOutput.FofFormula matrix = 
			implies(propertyPredicateApplication(axiom.getProperty(),
							     "X",
							     "Y"),
				datatypeVisitor.result());
		    
		    
		    _result = forAll("X","Y",matrix);

		} // visit(OWLDataPropertyRangeAxiom axiom)

		public void visit(OWLFunctionalDataPropertyAxiom axiom) {
			// ! [X,Y,Z] : prop(X,Y) & prop(X,Z) => Y = Z

		    TptpParserOutput.FofFormula condition = 
			and(propertyPredicateApplication(axiom.getProperty(),
							 "X",
							 "Y"),
			    propertyPredicateApplication(axiom.getProperty(),
							 "X",
							 "Z"));

		    TptpParserOutput.FofFormula matrix = 
			implies(condition,
				equalityApplication("Y","Z"));
		   	    
		    _result = forAll("X","Y","Z",matrix);
		    
		} // visit(OWLFunctionalDataPropertyAxiom axiom) 
		

		public void visit(OWLEquivalentDataPropertiesAxiom axiom) {
			if (axiom.getProperties().size() <= 1)
			{
			    _result = builtInTrue();
			    return;
			};

		    // ![X,Y] : (prop1(X,Y) <=> prop2(X,Y)) & 
		    //          (prop2(X,Y) <=> prop3(X,Y)) & .. & 
		    //          (propn-1(X,Y) <=> propn(X,Y))
		    
		    
		    Object[] prop = axiom.getProperties().toArray();

		    // prop1(X,Y) <=> prop2(X,Y)
		    
		    _result = 
			equivalent(propertyPredicateApplication((OWLProperty)prop[0],
								"X",
								"Y"),
				   
				   propertyPredicateApplication((OWLProperty)prop[1],
								"X",
								"Y"));

		    for (int i = 1; i + 1 < prop.length; ++i)
			{
			    _result = and(_result,
					  equivalent(propertyPredicateApplication((OWLProperty)prop[i],
										  "X",
										  "Y"),
						     
						     propertyPredicateApplication((OWLProperty)prop[i+1],
										  "X",
										  "Y")));
			};
		    

		    _result = forAll("X","Y",_result);

		} // visit(OWLEquivalentDataPropertiesAxiom axiom)


		public void visit(OWLClassAssertionAxiom axiom) {
			// C(ind)
		    
		    DescriptionVisitor visitor = 
			new DescriptionVisitor(convertIndividual(axiom.getIndividual()));
		    axiom.getClassExpression().accept(visitor);

		    _result = visitor.result();
		    
		} // visit(OWLClassAssertionAxiom axiom)

		public void visit(OWLEquivalentClassesAxiom axiom) {
			if (axiom.getClassExpressions().size() <= 1)
			{
			    _result = builtInTrue();
			    return;
			};

		    // ![X] : (C1(X) <=> C2(X)) & (C2(X) <=> C3(X)) & .. & (Cn-1(X) <=> Cn(X))
		    
		    Object[] descriptions = axiom.getClassExpressions().toArray();

		    DescriptionVisitor descVis1 = new DescriptionVisitor("X");
		    DescriptionVisitor descVis2 = new DescriptionVisitor("X");
		    
		    ((OWLClassExpression)descriptions[0]).accept(descVis1);
		    ((OWLClassExpression)descriptions[1]).accept(descVis2);

		    // C1(X) <=> C2(X)
		    _result = equivalent(descVis1.result(),descVis2.result());

		    for (int i = 1; i + 1 < descriptions.length; ++i)
			{
			    ((OWLClassExpression)descriptions[i]).accept(descVis1);
			    ((OWLClassExpression)descriptions[i+1]).accept(descVis2);
			    _result = and(_result,
					  equivalent(descVis1.result(),descVis2.result()));
			};
		    
		    _result = forAll("X",_result);
		    
		} // visit(OWLEquivalentClassesAxiom axiom) 

		public void visit(OWLDataPropertyAssertionAxiom axiom) {
			// prop(subj,obj)

		    _result =
			propertyPredicateApplication(axiom.getProperty(),
						     convertIndividual(axiom.getSubject()),
						     convertDataValue(axiom.getObject()));
						     

		} // visit(OWLDataPropertyAssertionAxiom axiom)

		public void visit(OWLTransitiveObjectPropertyAxiom axiom) {
			// ! [X,Y,Z] : prop(X,Y) & prop(Y,Z) => prop(X,Z)

		    TptpParserOutput.FofFormula condition = 
			and(propertyPredicateApplication(axiom.getProperty(),
							 "X",
							 "Y"),
			    propertyPredicateApplication(axiom.getProperty(),
							 "Y",
							 "Z"));

		    TptpParserOutput.FofFormula matrix = 
			implies(condition,
				propertyPredicateApplication(axiom.getProperty(),
							     "X",
							     "Z"));
		   	    
		    _result = forAll("X","Y","Z",matrix);

		} // visit(OWLTransitiveObjectPropertyAxiom axiom) 


		public void visit(OWLIrreflexiveObjectPropertyAxiom axiom) {
			 // ! [X] : ~prop(X,X)

		    _result = 
			forAll("X",
			       not(propertyPredicateApplication(axiom.getProperty(),
								"X",
								"X")));

		} // visit(OWLIrreflexiveObjectPropertyAxiom axiom) 


		public void visit(OWLSubDataPropertyOfAxiom axiom) {
		    // ![X,Y] : prop1(X,Y) => prop2(X,Y)
		    
		    OWLDataPropertyExpression prop1 = axiom.getSubProperty();
		    OWLDataPropertyExpression prop2 = axiom.getSuperProperty();

		    _result = implies(propertyPredicateApplication(prop1,
								   "X",
								   "Y"),
				      propertyPredicateApplication(prop2,
								   "X",
								   "Y"));

		    _result = forAll("X","Y",_result);

		} // visit(OWLSubDataPropertyOfAxiom axiom)


		public void visit(OWLInverseFunctionalObjectPropertyAxiom axiom) {
			// ! [X,Y,Z] : prop(Y,X) & prop(Z,X) => Y = Z

		    TptpParserOutput.FofFormula condition = 
			and(propertyPredicateApplication(axiom.getProperty(),
							 "Y",
							 "X"),
			    propertyPredicateApplication(axiom.getProperty(),
							 "Z",
							 "X"));

		    TptpParserOutput.FofFormula matrix = 
			implies(condition,
				equalityApplication("Y","Z"));
		   	    
		    _result = forAll("X","Y","Z",matrix);

		} // visit(OWLInverseFunctionalObjectPropertyAxiom axiom)


		public void visit(OWLSameIndividualAxiom axiom) {
			if (axiom.getIndividuals().size() <= 1)
			{
			    _result = builtInTrue();
			    return;
			};

		    // ind1 = ind2 & ind2 = ind3 & .. & indn-1 = indn
		    
		    Object[] individuals = axiom.getIndividuals().toArray();
		    _result = 
			equalityApplication(convertIndividual((OWLIndividual)
							      individuals[0]),
					    convertIndividual((OWLIndividual)
							      individuals[1]));

		    for (int i = 1; i + 1 < individuals.length; ++i)    
			_result =
			    and(_result,
				equalityApplication(convertIndividual((OWLIndividual)
								      individuals[i]),
						    convertIndividual((OWLIndividual)
								      individuals[i+1])));


		} // visit(OWLSameIndividualsAxiom axiom)


		public void visit(OWLSubPropertyChainOfAxiom axiom) {
			if (axiom.getPropertyChain().size() == 0)
			{
			    // I am not sure this is the right treatment. AR
			    _result = builtInTrue();
			    return;
			};
		    

		    // ! [X1,..,Xn+1] : 
		    //     prop1(X1,X2) & .. & propn(Xn,Xn+1) => prop(X1,Xn+1)

		    Object[] chain = axiom.getPropertyChain().toArray();
		    
		    
		    TptpParserOutput.FofFormula condition =
			propertyPredicateApplication((OWLPropertyExpression)chain[0],
						     "X0",
						     "X1");
		    
		    LinkedList<String> vars = new LinkedList<String>();
		    vars.addLast("X0");
		    vars.addLast("X1");

		    for (int i = 1; i < chain.length; ++i)
			{
			    condition = and(condition,
					    propertyPredicateApplication((OWLPropertyExpression)chain[i],
									 "X" + i,
									 "X" + (i + 1)));
			    vars.addLast("X" + (i + 1));
			};

		    _result = 
			implies(condition,
				propertyPredicateApplication(axiom.getSuperProperty(),
							     "X" + 0,
							     "X" + chain.length));

		    _result = forAll(vars,_result);
		    
		} //  visit(OWLSubPropertyChainOfAxiom axiom) 

		public void visit(OWLInverseObjectPropertiesAxiom axiom) {
			// ! [X,Y] : prop(X,Y) <=> invProp(Y,X)

		    TptpParserOutput.FofFormula matrix = 
			equivalent(propertyPredicateApplication(axiom.getFirstProperty(),
								"X",
								"Y"),
				   propertyPredicateApplication(axiom.getSecondProperty(),
								"Y",
								"X"));
		    _result = forAll("X","Y",matrix);
		    

		} // visit(OWLInverseObjectPropertiesAxiom axiom)


		public void visit(OWLHasKeyAxiom axiom) {
			//throw 
			//new Error("visit(OWLHasKeyAxiom axiom) should not be called.");
			
			// Two ways to express HasKey axiom: HasKey( CLE ( OPE1 ... OPEn ) ( DPE1 ... DPEn ))
			
			// 1. HasKey( owl:Thing ( OPE ) () ) is similar to the axiom InverseFunctionalObjectProperty( OPE )")
			
			// 2. for class C and a set of n properties {prop1, prop2, ..., propn}
			//    ! [X, Y, Z1,..,Zn] :
			//      C(X) & C(Y) & prop1(X,Z1) & prop1(Y,Z1) & ... & propn(X,Zn) & propn(Y,Zn) => X = Y

		    OWLClassExpression description = axiom.getClassExpression();

		    Object[] props = axiom.getPropertyExpressions().toArray();
		    // In each such axiom in an OWL ontology, number of properties MUST be larger than zero
		    if(props.length < 1)
		    	throw new Error(axiom + " MUST have at least one property");
		    
		    // 1. implementation   
		    if(description.isOWLThing() && axiom.getPropertyExpressions().size() == 1){
		    	if((OWLPropertyExpression)props[0] instanceof OWLObjectProperty){
		    		// similar to the axiom InverseFunctionalObjectProperty( OPE ));
		    		// which is ! [X,Y,Z] : prop(Y,X) & prop(Z,X) => Y = Z

				    TptpParserOutput.FofFormula condition = 
					and(propertyPredicateApplication((OWLPropertyExpression)axiom.getPropertyExpressions().toArray()[0],
									 "Y",
									 "X"),
					    propertyPredicateApplication((OWLPropertyExpression)axiom.getPropertyExpressions().toArray()[0],
									 "Z",
									 "X"));

				    TptpParserOutput.FofFormula matrix = 
					implies(condition,
						equalityApplication("Y","Z"));
				   	    
				    _result = forAll("X","Y","Z",matrix);
		    	}
		    }
		    else{
		    DescriptionVisitor descVis1 = new DescriptionVisitor("X");
		    DescriptionVisitor descVis2 = new DescriptionVisitor("Y");
		    
		    ((OWLClassExpression)description).accept(descVis1);
		    ((OWLClassExpression)description).accept(descVis2);

		    // C(X) & C(Y)
		    _result = and(descVis1.result(),descVis2.result());

			
			
			
		    		    
		    
		    
		    
		    // C(X) & C(Y) & prop1(X,Z1) & prop1(Y,Z1)
		    _result = and(_result,  
		    			and(
		    					propertyPredicateApplication((OWLPropertyExpression)props[0],"X", "Z1"),
		    					propertyPredicateApplication((OWLPropertyExpression)props[0],"Y","Z1")
		    			));
		    
		    TptpParserOutput.FofFormula condition = _result; 
	
		    LinkedList<String> vars = new LinkedList<String>();
		    vars.addLast("X");
		    vars.addLast("Y");
		    vars.addLast("Z1");

		    for (int i = 1; i < props.length; ++i)
			{
			    condition = and(condition,
					    and(propertyPredicateApplication((OWLPropertyExpression)props[i],
									 "X",
									 "Z" + (i+1)),
							propertyPredicateApplication((OWLPropertyExpression)props[i],
									 "Y",
									 "Z" + (i+1))
			    			));
			    
			    vars.addLast("Z" + (i + 1));
			};

			TptpParserOutput.FofFormula matrix = 
					implies(condition,
						equalityApplication("X","Y"));
				   	    
				    _result = forAll(vars,matrix);
				    
		    }
		} // visit(OWLHasKeyAxiom axiom) 


		public void visit(OWLDatatypeDefinitionAxiom axiom) {
			throw 
			new Error("visit(OWLDatatypeDefinitionAxiom axiom) should not be called.");

		} // visit(OWLDatatypeDefinitionAxiom axiom) 


		public void visit(SWRLRule rule) {
			// ![X1,..,Xn] : body => head where body can be $true
			// and head can be $false


			// Maps SWRL variable URIs to TPTP variable names:
			TreeMap<URI,String> variableMap = 
			    new TreeMap<URI,String>();


			TptpParserOutput.FofFormula condition;

			Iterator<SWRLAtom> atomIter = rule.getBody().iterator();
			
			if (atomIter.hasNext())
			    {
				condition = convert(atomIter.next(),variableMap);
				while (atomIter.hasNext())
				    condition = and(condition,
						    convert(atomIter.next(),
							    variableMap));
			    }
			else 
			    condition = builtInTrue();


			TptpParserOutput.FofFormula conclusion;
			
			atomIter = rule.getHead().iterator();

			
			if (atomIter.hasNext())
			    {
				conclusion = convert(atomIter.next(),variableMap);
				while (atomIter.hasNext())
				    condition = or(condition,
						    convert(atomIter.next(),
							    variableMap));
			    }
			else 
			    conclusion = builtInFalse();

			

		    _result = implies(condition,conclusion);
		    
		    for (Map.Entry<URI,String> var : variableMap.entrySet()) 
			_result = forAll(var.getValue(),_result);
			
		} // visit(SWRLRule rule)


		
		
	   	/** Keeps the result of every visit until the next visit. */
    	private SimpleTptpParserOutput.Formula _result;

    } // class AxiomVisitor

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

  //               Private methods:


    
    private Collection<SimpleTptpParserOutput.AnnotatedFormula>  convert(OWLClass klass) {

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	

	// ![X] : C(X) => owlThing(X)

	_nextVarIndex = 0;

	DescriptionVisitor visitor = new DescriptionVisitor("X");
	klass.accept(visitor);

	TptpParserOutput.FofFormula formula = 
	    forAll("X",implies(visitor.result(),owlThingConstraint("X")));
	
	result.add((SimpleTptpParserOutput.AnnotatedFormula)
		   _syntaxFactory.
		   createFofAnnotated("'" + klass.getIRI().toURI() + " is a subclass of owl:Thing'",
				      TptpParserOutput.FormulaRole.Axiom,
				      formula,
				      null,
				      0));

	return result;

    } // convert(OWLClass klass) 


    

    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(OWLObjectProperty prop) {

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	
	// Domain and range must be owlThing:

	_nextVarIndex = 0;	
	result.add(domainIsOwlThingAxiom(prop));

	_nextVarIndex = 0;
	result.add(rangeIsOwlThingAxiom(prop));
	        	

	return result;

    } // convert(OWLObjectProperty prop) 



    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(OWLDataProperty prop) {

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	
	// Domain must be owlThing:
	
	_nextVarIndex = 0;
	result.add(domainIsOwlThingAxiom(prop));

	_nextVarIndex = 0;
	result.add(rangeIsDataDomAxiom(prop));
	

	return result;

    } // convert(OWLDataProperty prop) 





    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(OWLIndividual ind) {
		
	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	
	if (ind.getTypes(_currentOntology).isEmpty() && 
	    ind.getObjectPropertyValues(_currentOntology).isEmpty() &&
	    ind.getDataPropertyValues(_currentOntology).isEmpty()) 
	    {
		// We only know about this individual that it is not a data value.
	
		// owlThing(ind)
		_nextVarIndex = 0;
		TptpParserOutput.FofFormula form =
		    owlThingConstraint(convertIndividual(ind));
		
	
		// we treat it as a named individual because anonumous individual does not give URI, only String
		OWLNamedIndividual nid = ind.asOWLNamedIndividual();
		
		SimpleTptpParserOutput.AnnotatedFormula annotated = 
		    (SimpleTptpParserOutput.AnnotatedFormula)
		    _syntaxFactory.
		    createFofAnnotated("'" + nid.getIRI().toURI() + " is an individual'",
				       TptpParserOutput.FormulaRole.Axiom,
				       form,
				       null,
				       0);
		result.add(annotated);
	    }; // if (ind.getTypes(_currentOntology).isEmpty() && 

	return result;

    } // convert(OWLIndividual ind) 




    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(OWLDatatype type) {

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	
	
	DataVisitor typeVisitor = new DataVisitor("X");
	type.accept(typeVisitor);

	TptpParserOutput.FofFormula matrix = 
	    implies(typeVisitor.result(),dataDomConstraint("X"));
	    

	// ![X] : type(X) => dataDomain(X)

	_nextVarIndex = 0;

	SimpleTptpParserOutput.AnnotatedFormula annotated = 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'" + type.getIRI().toURI() + " is a datatype'",
			       TptpParserOutput.FormulaRole.Axiom,
			       forAll("X",matrix),
			       null,
			       0);
	result.add(annotated);

	return result;

    } // convert(OWLDataType type)





    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(OWLClassAxiom classAxiom) {

	_nextVarIndex = 0;

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	
	AxiomVisitor visitor = new AxiomVisitor();
	classAxiom.accept(visitor);

	result.add((SimpleTptpParserOutput.AnnotatedFormula)
		   _syntaxFactory.
		   createFofAnnotated("'Class axiom #" + _nextAxiomIndex++ + "'",
				      TptpParserOutput.FormulaRole.Axiom,
				      visitor.result(),
				      null,
				      0));

	return result;

    } // convert(OWLClassAxiom classAxiom)



    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(OWLPropertyAxiom propAxiom) {

	_nextVarIndex = 0;

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	
	AxiomVisitor visitor = new AxiomVisitor();
	propAxiom.accept(visitor);

	result.add((SimpleTptpParserOutput.AnnotatedFormula)
		   _syntaxFactory.
		   createFofAnnotated("'Property axiom #" + _nextAxiomIndex++ + "'",
				      TptpParserOutput.FormulaRole.Axiom,
				      visitor.result(),
				      null,
				      0));

	return result;

    } // convert(OWLPropertyAxiom propAxiom)

    

    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(OWLIndividualAxiom indAxiom) {

	_nextVarIndex = 0;

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	
	AxiomVisitor visitor = new AxiomVisitor();
	indAxiom.accept(visitor);

	result.add((SimpleTptpParserOutput.AnnotatedFormula)
		   _syntaxFactory.
		   createFofAnnotated("'Individual axiom #" + _nextAxiomIndex++ + "'",
				      TptpParserOutput.FormulaRole.Axiom,
				      visitor.result(),
				      null,
				      0));

	return result;

    } // convert(OWLIndividualAxiom indAxiom)

    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(OWLHasKeyAxiom keyAxiom) {

	_nextVarIndex = 0;

	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();
	
	AxiomVisitor visitor = new AxiomVisitor();
	keyAxiom.accept(visitor);

	result.add((SimpleTptpParserOutput.AnnotatedFormula)
		   _syntaxFactory.
		   createFofAnnotated("'HasKey axiom #" + _nextAxiomIndex++ + "'",
				      TptpParserOutput.FormulaRole.Axiom,
				      visitor.result(),
				      null,
				      0));

	return result;

    } // convert(OWLHasKeyAxiom keyAxiom)



    private
	Collection<SimpleTptpParserOutput.AnnotatedFormula> 
	convert(SWRLRule rule) {

	_nextVarIndex = 0;
	
	LinkedList<SimpleTptpParserOutput.AnnotatedFormula> result =
	    new LinkedList<SimpleTptpParserOutput.AnnotatedFormula>();

	
	AxiomVisitor visitor = new AxiomVisitor();
	rule.accept(visitor);

	result.add((SimpleTptpParserOutput.AnnotatedFormula)
		   _syntaxFactory.
		   createFofAnnotated("'SWRL rule #" + _nextAxiomIndex++ + "'",
				      TptpParserOutput.FormulaRole.Axiom,
				      visitor.result(),
				      null,
				      0));

	return result;

    } // convert(SWRLRule rule)

    

    private 
	TptpParserOutput.FofFormula 
	convert(SWRLAtom atom,Map<URI,String> variableMap) {

	if (atom instanceof SWRLClassAtom)
	    {
		DescriptionVisitor visitor = 
		    new DescriptionVisitor(convert(((SWRLClassAtom)atom).
						   getArgument(),
						   variableMap));
		((SWRLClassAtom)atom).getPredicate().accept(visitor);

		return visitor.result();
	    }
	else if (atom instanceof SWRLObjectPropertyAtom) 
	    {
		return
		    propertyPredicateApplication(((SWRLObjectPropertyAtom)atom).
						 getPredicate(),
						 convert(((SWRLObjectPropertyAtom)atom).
							 getFirstArgument(),
							 variableMap),
						 convert(((SWRLObjectPropertyAtom)atom).
							 getSecondArgument(),
							 variableMap));
						 
	    }
	else if (atom instanceof SWRLDataPropertyAtom) 
	    {

		return
		    propertyPredicateApplication(((SWRLDataPropertyAtom)atom).
						 getPredicate(),
						 convert(((SWRLDataPropertyAtom)atom).
							 getFirstArgument(),
							 variableMap),
						 convert(((SWRLDataPropertyAtom)atom).
							 getSecondArgument(),
							 variableMap));
						 

	    }
	else if (atom instanceof SWRLSameIndividualAtom)
	    {
		return 
		    equalityApplication(convert(((SWRLSameIndividualAtom)atom).
						getFirstArgument(),
						variableMap),
					convert(((SWRLSameIndividualAtom)atom).
						getSecondArgument(),
						variableMap));
	    }
	else if (atom instanceof SWRLDifferentIndividualsAtom)
	    {
		return 
		    not(equalityApplication(convert(((SWRLDifferentIndividualsAtom)atom).
						    getFirstArgument(),
						    variableMap),
					    convert(((SWRLDifferentIndividualsAtom)atom).
						    getSecondArgument(),
						    variableMap)));
	    }
	else if (atom instanceof SWRLBuiltInAtom)
	    {
		int arity = ((SWRLBuiltInAtom)atom).getArguments().size();
		
		//String pred = predicateForURI(((SWRLBuiltInAtom)atom).getPredicate().getIRI().toURI(),arity);
		String pred = predicateForURI(((SWRLBuiltInAtom)atom).getPredicate().toURI(),arity);
					      
		LinkedList<TptpParserOutput.Term> args = 
		    new LinkedList<TptpParserOutput.Term>();
		
		for (SWRLDArgument arg : ((SWRLBuiltInAtom)atom).getArguments())
		    args.addLast(convert(arg,variableMap));

		return atom(pred,args);
	    }
	else if (atom instanceof SWRLDataRangeAtom)
	    {
		DataVisitor visitor = new DataVisitor(convert(((SWRLDataRangeAtom)atom).
					    getArgument(),
					    variableMap));
		((SWRLDataRangeAtom)atom).getPredicate().accept(visitor);

		return visitor.result();
	    };
	

	throw new Error("Bad kind of SWRLAtom.");
	
    } // convert(SWRLAtom atom,Map<URI,String> variableMap)
    
    /****
     *  
     * SWRLArgument : Represents an object in an atom (either a data object or and individual object)
	 * SWRLDArgument : Represents arguments in SWRLAtoms that are either OWLLiterals or variables for individuals SWRLLiteralArgument
	 * SWRLIArgument : Represents arguments in SWRLAtoms that are either OWLIndividuals or variables for individuals SWRLIndividualArgument
     */

    // deal SWRLIArgument
    private TptpParserOutput.Term convert(SWRLIArgument term,Map<URI,String> variableMap) {
	
	if (term instanceof SWRLIndividualArgument)
	    {
		return convertIndividual(((SWRLIndividualArgument)term).getIndividual());
	    }
	else if (term instanceof SWRLVariable) 
	    {
		String var = variableMap.get(((SWRLVariable)term).getIRI().toURI());
		if (var == null)
		    {
			// New variable:
			var = generateNewVariable("X");
			variableMap.put(((SWRLVariable)term).getIRI().toURI(),var);
		    };
		return _syntaxFactory.createVariableTerm(var);
	    }
	else
	    throw new Error("Bad kind of SWRLAtomIObject.");

    } // convert(SWRLAtomIObject term,Map<URI,String> variableMap) 
   
    
    // deal SWRLDArgument
    private 
	TptpParserOutput.Term 
	convert(SWRLDArgument term,Map<URI,String> variableMap) {
	
	if (term instanceof SWRLLiteralArgument)
	    {
		//return convertDataValue(((SWRLLiteralArgument)term).getConstant());
		return convertDataValue(((SWRLLiteralArgument)term).getLiteral());
	    }
	else if (term instanceof SWRLVariable) 
	    {
		String var = 
		    variableMap.get(((SWRLVariable)term).getIRI().toURI());
		if (var == null)
		    {
			// New variable:
			var = generateNewVariable("X");
			variableMap.put(((SWRLVariable)term).getIRI().toURI(),
					var);
		    };
		return _syntaxFactory.createVariableTerm(var);
	    }
	else// change the instance kind in every Error message
	    throw new Error("Bad kind of SWRLAtomDObject.");

    } // convert(SWRLAtomDObject term,Map<URI,String> variableMap) 
    

    
    private
	LinkedList<SimpleTptpParserOutput.AnnotatedClause> 
	clausify(LinkedList<SimpleTptpParserOutput.AnnotatedFormula> axioms) {

	LinkedList<SimpleTptpParserOutput.AnnotatedClause> result = 
	    new LinkedList<SimpleTptpParserOutput.AnnotatedClause>();

	for (SimpleTptpParserOutput.AnnotatedFormula ax : axioms)
	    result.addAll(clausify(ax));
	
	return result;

    } // clausify(LinkedList<SimpleTptpParserOutput.AnnotatedFormula> axioms)


    private 
	LinkedList<SimpleTptpParserOutput.AnnotatedClause> 
	clausify(SimpleTptpParserOutput.AnnotatedFormula axiom) {
	
	LinkedList<SimpleTptpParserOutput.AnnotatedClause> result = 
	    new LinkedList<SimpleTptpParserOutput.AnnotatedClause>();

	assert axiom.getRole() == TptpParserOutput.FormulaRole.Axiom;

	LinkedList<LinkedList<TptpParserOutput.Literal>> clauses = 
	    clausify(axiom.getFormula(),true,new LinkedList<String>());
	
	int n = 0;

	for (LinkedList<TptpParserOutput.Literal> clauseLiterals : clauses)
	    {
		
		TptpParserOutput.CnfFormula clause = 
		    _syntaxFactory.createClause(clauseLiterals);

		String newName = axiom.getName();
		
		if (clauses.size() > 1)
		    newName = 
			newName.substring(0,newName.length() - 1) +
			" CL " + ++n + "'";

		SimpleTptpParserOutput.AnnotatedClause newAxiom = 
		    (SimpleTptpParserOutput.AnnotatedClause)
		    _syntaxFactory.
		    createCnfAnnotated(newName,
				       TptpParserOutput.FormulaRole.Axiom,
				       clause,
				       null,
				       0);
		
		result.add(newAxiom);
 
	    }; // for (SimpleTptpParserOutput.Clause cl : clauses)


	return result;

    } // clausify(SimpleTptpParserOutput.AnnotatedFormula axiom)


    private 
	LinkedList<LinkedList<TptpParserOutput.Literal>>
	clausify(SimpleTptpParserOutput.Formula formula,
		 boolean positive,
		 LinkedList<String> universalVariables) {

       
	switch (formula.getKind())
	    {
	    case Atomic:
		{
		    TptpParserOutput.Literal literal =
			_syntaxFactory.
			createLiteral(positive,
				      (SimpleTptpParserOutput.Formula.Atomic)formula);

		    LinkedList<LinkedList<TptpParserOutput.Literal>> 
			result =
			new LinkedList<LinkedList<TptpParserOutput.Literal>>();
		    
		    result.add(new LinkedList<TptpParserOutput.Literal>());
		    result.getFirst().add(literal);
		    
		    return result;

		} // case Atomic:
	

	    case Binary:
		return 
		    clausifyBinary((SimpleTptpParserOutput.Formula.Binary)formula,
				   positive,
				   universalVariables);
				   

	    case Negation:
		return 
		    clausify(((SimpleTptpParserOutput.Formula.Negation)formula).
			     getArgument(),
			     !positive,
			     universalVariables);
		
	    case Quantified:
		return 
		    clausifyQuantified((SimpleTptpParserOutput.Formula.Quantified)formula,
				       positive,
				       universalVariables);
		


	    } // switch (formula.getKind())
	
	assert false;
	
	return null;

    } // clausify(SimpleTptpParserOutput.Formula formula)


    
    private 
	LinkedList<LinkedList<TptpParserOutput.Literal>>
	clausifyBinary(SimpleTptpParserOutput.Formula.Binary formula,
		       boolean positive,
		       LinkedList<String> universalVariables) {

	switch (formula.getConnective())
	    {
	    case And:
		{
		    LinkedList<LinkedList<TptpParserOutput.Literal>> clausifiedLHS =
			clausify(formula.getLhs(),positive,universalVariables);
		    LinkedList<LinkedList<TptpParserOutput.Literal>> clausifiedRHS =
			clausify(formula.getRhs(),positive,universalVariables);
		    
		    if (positive)
			{
			    clausifiedLHS.addAll(clausifiedRHS);
			    return clausifiedLHS;
			}
		    else
			{
			    // "Multiply" clausifiedLHS and clausifiedRHS:

			    LinkedList<LinkedList<TptpParserOutput.Literal>> result =
				new LinkedList<LinkedList<TptpParserOutput.Literal>>();

			    for (LinkedList<TptpParserOutput.Literal> cl1 : 
				     clausifiedLHS)
				for (LinkedList<TptpParserOutput.Literal> cl2 : 
					 clausifiedRHS)
				    {
					LinkedList<TptpParserOutput.Literal> cl = 
					    new LinkedList<TptpParserOutput.Literal>();
					cl.addAll(cl1);
					cl.addAll(cl2);
					result.add(cl);
				    };
			    
			    return result;

			} // if (positive)

		} // case And:

	    case Disequivalence:
		{
		    // Treat it as a negation of equivalence:

		    SimpleTptpParserOutput.Formula equivalence =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createBinaryFormula(formula.getLhs(),
					    TptpParserOutput.BinaryConnective.Equivalence,
					    formula.getRhs());
		    
		    return clausify(equivalence,!positive,universalVariables);

		} // case Disequivalence:


	    case Equivalence:
		{
		    // Treat it as a conjunction of implications:
		    
		    SimpleTptpParserOutput.Formula implication =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createBinaryFormula(formula.getLhs(),
					    TptpParserOutput.BinaryConnective.Implication,
					    formula.getRhs());
		    
		    SimpleTptpParserOutput.Formula reverseImplication =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createBinaryFormula(formula.getRhs(),
					    TptpParserOutput.BinaryConnective.Implication,
					    formula.getLhs());
		    

		    SimpleTptpParserOutput.Formula conjunction =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createBinaryFormula(implication,
					    TptpParserOutput.BinaryConnective.And,
					    reverseImplication);

		    return clausify(conjunction,positive,universalVariables);

		} // case Equivalence:


	    case Implication:
		{
		    // Treat F => G as ~F \/ G:

		    SimpleTptpParserOutput.Formula negationOfLHS =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.createNegationOf(formula.getLhs());

		    
		    SimpleTptpParserOutput.Formula disjunction =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createBinaryFormula(negationOfLHS,
					    TptpParserOutput.BinaryConnective.Or,
					    formula.getRhs());
		    

		    return clausify(disjunction,positive,universalVariables);

		} // case Implication:


	    case NotAnd:
		{
		    // Treat it as a negation of conjunction:
		    

		    SimpleTptpParserOutput.Formula conjunction =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createBinaryFormula(formula.getLhs(),
					    TptpParserOutput.BinaryConnective.And,
					    formula.getRhs());

		    return clausify(conjunction,!positive,universalVariables);

		} // case NotAnd:
		

	    case NotOr:
		{
		    // Treat it as a negation of disjunction:
		    

		    SimpleTptpParserOutput.Formula disjunction =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createBinaryFormula(formula.getLhs(),
					    TptpParserOutput.BinaryConnective.Or,
					    formula.getRhs());

		    return clausify(disjunction,!positive,universalVariables);

		} // case NotOr:
		


	    case Or:
		{
		    LinkedList<LinkedList<TptpParserOutput.Literal>> clausifiedLHS =
			clausify(formula.getLhs(),positive,universalVariables);
		    LinkedList<LinkedList<TptpParserOutput.Literal>> clausifiedRHS =
			clausify(formula.getRhs(),positive,universalVariables);
		    
		    if (!positive)
			{
			    // Can be treated as positive conjunction:
			    clausifiedLHS.addAll(clausifiedRHS);
			    return clausifiedLHS;
			}
		    else
			{
			    // "Multiply" clausifiedLHS and clausifiedRHS:

			    LinkedList<LinkedList<TptpParserOutput.Literal>> result =
				new LinkedList<LinkedList<TptpParserOutput.Literal>>();

			    for (LinkedList<TptpParserOutput.Literal> cl1 : 
				     clausifiedLHS)
				for (LinkedList<TptpParserOutput.Literal> cl2 : 
					 clausifiedRHS)
				    {
					LinkedList<TptpParserOutput.Literal> cl = 
					    new LinkedList<TptpParserOutput.Literal>();
					cl.addAll(cl1);
					cl.addAll(cl2);
					result.add(cl);
				    };
			    
			    return result;

			} // if (positive)

		} // case Or:


	    case ReverseImplication:
		{
		    // Treat F <= G as  F \/ ~G:

		    SimpleTptpParserOutput.Formula negationOfRHS =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.createNegationOf(formula.getRhs());

		    
		    SimpleTptpParserOutput.Formula disjunction =
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createBinaryFormula(formula.getLhs(),
					    TptpParserOutput.BinaryConnective.Or,
					    negationOfRHS);
		    

		    return clausify(disjunction,positive,universalVariables);
		    
		} // case ReverseImplication:
		
	    } // switch (formula.getConnective())

	assert false;
	
	return null;

    } // clausifyBinary(SimpleTptpParserOutput.Formula.Binary formula,





    private 
	LinkedList<LinkedList<TptpParserOutput.Literal>>
	clausifyQuantified(SimpleTptpParserOutput.Formula.Quantified formula,
			   boolean positive,
			   LinkedList<String> universalVariables) {

	if ((positive && 
	     formula.getQuantifier() == TptpParserOutput.Quantifier.Exists) ||
	    (!positive && 
	     formula.getQuantifier() == TptpParserOutput.Quantifier.ForAll))
	    {
		// Skolemise: 
		SimpleTptpParserOutput.Term skolemTerm = 
		    skolemTerm(universalVariables);
		
		SimpleTptpParserOutput.Formula newMatrix = 
		    replace((SimpleTptpParserOutput.Formula)formula.getMatrix(),
			    formula.getVariable(),
			    skolemTerm);
		return clausify(newMatrix,positive,universalVariables);
	    };
	
	// Essentially universal quantifier. Just drop it.
	
	assert !universalVariables.contains(formula.getVariable());
	universalVariables.addLast(formula.getVariable());

	LinkedList<LinkedList<TptpParserOutput.Literal>> result =
	    clausify(formula.getMatrix(),positive,universalVariables);

	universalVariables.removeLast();

	return result;

    } // clausifyQuantified(SimpleTptpParserOutput.Formula.Quantified formula,






    private 
	SimpleTptpParserOutput.Formula 
	atom(String pred,LinkedList<TptpParserOutput.Term> args) {
	
	return 
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    atomAsFormula(_syntaxFactory.
			  createPlainAtom(pred,
					  args));
    } 




    private 
	SimpleTptpParserOutput.Formula 
	atom(String pred,TptpParserOutput.Term arg) {

	LinkedList<TptpParserOutput.Term> arguments = 
	    new LinkedList<TptpParserOutput.Term>();
	arguments.addLast(arg);

	return atom(pred,arguments);
    } 


    private 
	SimpleTptpParserOutput.Formula 
	atom(String pred,
	     TptpParserOutput.Term arg1,
	     TptpParserOutput.Term arg2) {

	LinkedList<TptpParserOutput.Term> arguments = 
	    new LinkedList<TptpParserOutput.Term>();
	arguments.addLast(arg1);
	arguments.addLast(arg2);

	return atom(pred,arguments);
    } 


    
    private SimpleTptpParserOutput.Formula builtInFalse() {
	return 
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    atomAsFormula(_syntaxFactory.builtInFalse());
    }
    
    private SimpleTptpParserOutput.Formula builtInTrue() {
	return 
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    atomAsFormula(_syntaxFactory.builtInTrue());
    }

    private 
	SimpleTptpParserOutput.Formula 
	forAll(LinkedList<String> vars,
	       TptpParserOutput.FofFormula matrix) {

	return 
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    createQuantifiedFormula(TptpParserOutput.Quantifier.ForAll,
				    vars,
				    matrix);
    }


    private 
	SimpleTptpParserOutput.Formula 
	forAll(String var,
	       TptpParserOutput.FofFormula matrix) {
	
	LinkedList<String> vars = new LinkedList<String>();
	vars.addLast(var);
	return forAll(vars,matrix);
    }

    private 
	SimpleTptpParserOutput.Formula 
	forAll(String var1,
	       String var2,
	       TptpParserOutput.FofFormula matrix) {
	
	LinkedList<String> vars = new LinkedList<String>();
	vars.addLast(var2);
	vars.addLast(var1);
	return forAll(vars,matrix);
    }

    private 
	SimpleTptpParserOutput.Formula 
	forAll(String var1,
	       String var2,
	       String var3,
	       TptpParserOutput.FofFormula matrix) {
	
	LinkedList<String> vars = new LinkedList<String>();
	vars.addLast(var3);
	vars.addLast(var2);
	vars.addLast(var1);
	return forAll(vars,matrix);
    }



    private 
	SimpleTptpParserOutput.Formula 
	exist(LinkedList<String> vars,
	      TptpParserOutput.FofFormula matrix) {
	
	return 
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    createQuantifiedFormula(TptpParserOutput.Quantifier.Exists,
				    vars,
				    matrix);
    }

    private 
	SimpleTptpParserOutput.Formula 
	exist(String var,
	      TptpParserOutput.FofFormula matrix) {
	
	LinkedList<String> vars = new LinkedList<String>();
	vars.addLast(var);
	return exist(vars,matrix);
    }

    private 
	SimpleTptpParserOutput.Formula 
	not(TptpParserOutput.FofFormula form) {
	
	
	return
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    createNegationOf(form);

    } // not(TptpParserOutput.FofFormula form)


    private 
	SimpleTptpParserOutput.Formula 
	and(TptpParserOutput.FofFormula form1,
	    TptpParserOutput.FofFormula form2) {
	
	return
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    createBinaryFormula(form1,
				TptpParserOutput.BinaryConnective.And,
				form2);

    } // and(TptpParserOutput.FofFormula form1,..)


    private 
	SimpleTptpParserOutput.Formula 
	or(TptpParserOutput.FofFormula form1,
	   TptpParserOutput.FofFormula form2) {
	
	return
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    createBinaryFormula(form1,
				TptpParserOutput.BinaryConnective.Or,
				form2);

    } // or(TptpParserOutput.FofFormula form1,..)


    private 
	SimpleTptpParserOutput.Formula 
	implies(TptpParserOutput.FofFormula form1,
		TptpParserOutput.FofFormula form2) {
	
	return
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    createBinaryFormula(form1,
				TptpParserOutput.BinaryConnective.Implication,
				form2);

    } // implies(TptpParserOutput.FofFormula form1,..)



    private 
	SimpleTptpParserOutput.Formula 
	equivalent(TptpParserOutput.FofFormula form1,
		   TptpParserOutput.FofFormula form2) {
	
	return
	    (SimpleTptpParserOutput.Formula)
	    _syntaxFactory.
	    createBinaryFormula(form1,
				TptpParserOutput.BinaryConnective.Equivalence,
				form2);

    } // equivalent(TptpParserOutput.FofFormula form1,..)



    
    /** owlThing(var) */
    private TptpParserOutput.FofFormula owlThingConstraint(String var) 
    {
	return owlThingConstraint(_syntaxFactory.createVariableTerm(var));
    } 


    /** owlThing(term) */
    private TptpParserOutput.FofFormula 
	owlThingConstraint(TptpParserOutput.Term term) {

	return atom(predicateForOwlThing(),term);

    } // owlThingConstraint(TptpParserOutput.Term term)

    /** owlNothing(var) */
    private TptpParserOutput.FofFormula owlNothingConstraint(String var) 
    {
	return  atom(predicateForOwlNothing(),_syntaxFactory.createVariableTerm(var));
    } 


    /** dataDomain(var) */
    private TptpParserOutput.FofFormula dataDomConstraint(String var) {

	return atom(predicateForDataDomain(),
		    _syntaxFactory.createVariableTerm(var));

    } // dataDomConstraint(String var)


    /** klass(term) */
    private 
	SimpleTptpParserOutput.Formula 
	classPredicateApplication(OWLClass klass,
				  TptpParserOutput.Term term) {
	
	return atom(predicateForURI(klass.getIRI().toURI(),1),term);

    } // classPredicateApplication(OWLClass klass,


    /** property(term1,term2) */
    private 
	SimpleTptpParserOutput.Formula 
	propertyPredicateApplication(OWLPropertyExpression property,
				     TptpParserOutput.Term term1,
				     TptpParserOutput.Term term2) {
	if (property instanceof OWLDataPropertyExpression)
	    {
		// Every OWLDataPropertyExpression is an OWLDataProperty.
		
		return 
		    atom(predicateForURI(((OWLDataProperty)property).getIRI().toURI(),
					 2),
			 term1,
			 term2);
				    
	    }
	else
	    {
		assert property instanceof OWLObjectPropertyExpression;
		
		if (property instanceof OWLObjectInverseOf)
		    {
			
			return 
			    propertyPredicateApplication(((OWLObjectInverseOf)property).
							 getInverse(),
							 term2,
							 term1);
		    };

		assert property instanceof OWLObjectProperty;
		
		return 
		    atom(predicateForURI(((OWLObjectProperty)property).getIRI().toURI(),
					 2),
			 term1,
			 term2);
	    }

    } // propertyPredicateApplication(OWLPropertyExpression property,..)



    /** property(term,var) */
    private 
	TptpParserOutput.FofFormula 
	propertyPredicateApplication(OWLPropertyExpression property,
				     TptpParserOutput.Term term,
				     String var)  
    {

	return 
	    propertyPredicateApplication(property,
					 term,
					 _syntaxFactory.createVariableTerm(var));

    } // propertyPredicateApplication(OWLPropertyExpression property,..)


    /** property(var1,var2) */
    private 
	TptpParserOutput.FofFormula 
	propertyPredicateApplication(OWLPropertyExpression property,
				     String var1,
				     String var2)  
    {
	return 
	    propertyPredicateApplication(property,
					 _syntaxFactory.createVariableTerm(var1),
					 _syntaxFactory.createVariableTerm(var2));


    } // propertyPredicateApplication(OWLPropertyExpression property,..)


    /** For visualisation purposes only; not to be used as a TPTP id.
     *  @return URI of the property or inverse(<URI>)
     */
    private String toString(OWLPropertyExpression property) {
	
	
	if (property instanceof OWLDataPropertyExpression)
	    {
		// Every OWLDataPropertyExpression is an OWLDataProperty.
		
		return ((OWLDataProperty)property).getIRI().toURI().toString();
	    }
	else
	    {
		assert property instanceof OWLObjectPropertyExpression;
		
		if (property instanceof OWLObjectInverseOf)
		    {
			
			return "inverse(" + 
			    toString(((OWLObjectInverseOf)property).getInverse()) +
			    ")";
		    };

		assert property instanceof OWLObjectProperty;
		
		return ((OWLObjectProperty)property).getIRI().toURI().toString();
	    }


    } // toString(OWLPropertyExpression property)

    
    /** Creater the formula (var1 == var2) */
    private
	SimpleTptpParserOutput.Formula 
	equalityApplication(String var1,String var2) {
	return 
	    equalityApplication(_syntaxFactory.createVariableTerm(var1),
				_syntaxFactory.createVariableTerm(var2));

    } // equalityApplication(String var1,String var2)
	
    
    /** Creater the formula (term1 == term2) */
    private
	SimpleTptpParserOutput.Formula 
	equalityApplication(TptpParserOutput.Term term1,
			    TptpParserOutput.Term term2) {

	return atom(equalityPredicate(),term1,term2);

    } // equalityApplication(TptpParserOutput.Term term1,..)
	


    /** ![X,Y] : (prop(X,Y) => owlThing(X)) */
    private
	SimpleTptpParserOutput.AnnotatedFormula
	domainIsOwlThingAxiom(OWLPropertyExpression prop) {
	
	
	TptpParserOutput.FofFormula matrix = 
	    _syntaxFactory.
	    createBinaryFormula(propertyPredicateApplication(prop,"X","Y"),
				TptpParserOutput.BinaryConnective.Implication,
				owlThingConstraint("X"));
	    

	return 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'Subject of " + toString(prop) + " must be an individual'",
			       TptpParserOutput.FormulaRole.Axiom,
			       forAll("X","Y",matrix),
			       null,
			       0);
    } // domainIsOwlThingAxiom(OWLPropertyExpression prop)



    /** ![X,Y] : (prop(X,Y) => owlThing(Y)) */
    private
	SimpleTptpParserOutput.AnnotatedFormula
	rangeIsOwlThingAxiom(OWLObjectPropertyExpression prop) {
	
	
	TptpParserOutput.FofFormula matrix = 
	    _syntaxFactory.
	    createBinaryFormula(propertyPredicateApplication(prop,"X","Y"),
				TptpParserOutput.BinaryConnective.Implication,
				owlThingConstraint("Y"));
	    
	return 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'Object of " + toString(prop) + " must be an individual'",
			       TptpParserOutput.FormulaRole.Axiom,
			       forAll("X","Y",matrix),
			       null,
			       0);

    } // rangeIsOwlThingAxiom(OWLObjectPropertyExpression prop)


    


    /** ![X,Y] : (prop(X,Y) => dataDomain(Y)) */
    private
	SimpleTptpParserOutput.AnnotatedFormula
	rangeIsDataDomAxiom(OWLDataPropertyExpression prop) {
	
	
	TptpParserOutput.FofFormula matrix = 
	    _syntaxFactory.
	    createBinaryFormula(propertyPredicateApplication(prop,"X","Y"),
				TptpParserOutput.BinaryConnective.Implication,
				dataDomConstraint("Y"));
	    

	return 
	    (SimpleTptpParserOutput.AnnotatedFormula)
	    _syntaxFactory.
	    createFofAnnotated("'Object of " + toString(prop) + " must be a data value'",
			       TptpParserOutput.FormulaRole.Axiom,
			       forAll("X","Y",matrix),
			       null,
			       0);

    } // rangeIsDataDomAxiom(OWLDataPropertyExpression prop)


    private 
	TptpParserOutput.Term convertDataValue(OWLLiteral val) {
	
	TptpParserOutput.Term arg1 =
	    _syntaxFactory.createPlainTerm("\"" + 
					   val.getLiteral() +
					   "\"" ,
					   null);
	
	LinkedList<TptpParserOutput.Term> args =
	    new LinkedList<TptpParserOutput.Term>();
	
	args.addLast(arg1);

	//if (val.isTyped())
	//check if the literal is a typed literal e.g., string, int, float, boolean, double etc.
	if (!val.isRDFPlainLiteral())		
	    { 
		//OWLDatatype type = val.asOWLTypedConstant().getDataType();
		OWLDatatype type = val.getDatatype();
		
		URI typeURI = type.getIRI().toURI();
		
		if (typeURI.toString().
		    equals("http://www.w3.org/2001/XMLSchema#string"))
		    return
			_syntaxFactory.createPlainTerm(stringLitNoLangFunc(),
						       args);
		
		    
		if (typeURI.toString().
		    equals("http://www.w3.org/2001/XMLSchema#integer"))
		    {
			args = new LinkedList<TptpParserOutput.Term>();
			arg1 = _syntaxFactory.createPlainTerm(val.getLiteral(),null);
			args.addLast(arg1);
			return 
			    (SimpleTptpParserOutput.Term)
			    _syntaxFactory.createPlainTerm(intLitFunc(),
							   args);
		    };

		// General case of a typed literal:

		TptpParserOutput.Term arg2 =
		    _syntaxFactory.createPlainTerm("\"" + typeURI + "\"",
						   null);

		args.addLast(arg2);
		    
		return 
		    (SimpleTptpParserOutput.Term)
		    _syntaxFactory.createPlainTerm(typedLitFunc(),
						   args);
		
	    }
	else // !val.isTyped()
	    {
		
		//if (val.asOWLUntypedConstant().hasLang())
		if (val.hasLang())
		    {
			//TptpParserOutput.Term arg2 = _syntaxFactory.createPlainTerm("\"" +val.asOWLUntypedConstant().getLang()+"\"" ,null);
			TptpParserOutput.Term arg2 = _syntaxFactory.createPlainTerm("\"" +val.getLang()+"\"" ,null);
			args.addLast(arg2);

			return 
			    (SimpleTptpParserOutput.Term)
			    _syntaxFactory.createPlainTerm(stringLitWithLangFunc(),
							   args);
		    }
		else
		    {
			return 
			    (SimpleTptpParserOutput.Term)
			    _syntaxFactory.createPlainTerm(stringLitNoLangFunc(),
							   args);
		    }
	    } // if (val.isTyped())
	
    } // convertDataValue(OWLConstant val) 



    private 
	TptpParserOutput.Term convertIndividual(OWLIndividual ind) 
	{

	String id;

	if (ind.isAnonymous())
	    {
		id = freshSkolemIndividualConstant();
	    }
	else{
	    //id = individualConstantForURI(ind.getIRI().toURI());
		OWLNamedIndividual nid = ind.asOWLNamedIndividual();
		id = individualConstantForURI(nid.getIRI().toURI());
		}	
	return _syntaxFactory.createPlainTerm(id,null);

    } // convertIndividual(OWLIndividual ind)



    
    private 
	SimpleTptpParserOutput.Term 
	skolemTerm(LinkedList<String> universalVariables) {

	if (universalVariables.isEmpty())
	    return 
		(SimpleTptpParserOutput.Term)
		_syntaxFactory.createPlainTerm(freshSkolemIndividualConstant(),
					       null);
	String func = freshSkolemFunction(universalVariables.size());
	
	LinkedList<TptpParserOutput.Term> args = 
	    new LinkedList<TptpParserOutput.Term>();

	for (String var : universalVariables)
	    args.addLast(_syntaxFactory.createVariableTerm(var));

	return 
	    (SimpleTptpParserOutput.Term)
	    _syntaxFactory.createPlainTerm(func,args);

    } // skolemTerm(LinkedList<String> universalVariables)



    /** Replaces all the occurences of the variable <code>var</code>
     *  in <code>form</code> by the term <code>replacement</code>.
     */
    private 
	SimpleTptpParserOutput.Formula 
	replace(SimpleTptpParserOutput.Formula form,
		String var,
		SimpleTptpParserOutput.Term replacement) {

       
	switch (form.getKind())
	    {
	    case Atomic:
		{
		    String pred = 
			((SimpleTptpParserOutput.Formula.Atomic)form).
			getPredicate();
		    
		    LinkedList<TptpParserOutput.Term> newArgs = 
			new LinkedList<TptpParserOutput.Term>();

		    if (((SimpleTptpParserOutput.Formula.Atomic)form).
			getArguments() == null ||
			!((SimpleTptpParserOutput.Formula.Atomic)form).
			getArguments().iterator().hasNext())
			{
			    newArgs = null;
			}
		    else
			for (TptpParserOutput.Term arg : 
				 ((SimpleTptpParserOutput.Formula.Atomic)form).
				 getArguments())
			    newArgs.addLast(replace((SimpleTptpParserOutput.Term)arg,
						    var,
						    replacement));
		    return 
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.createPlainAtom(pred,newArgs);
		}

	    case Binary:
		{ 
		    TptpParserOutput.BinaryConnective connective =
			((SimpleTptpParserOutput.Formula.Binary)form).
			getConnective();
		    

		    SimpleTptpParserOutput.Formula newLHS = 
			replace(((SimpleTptpParserOutput.Formula.Binary)form).
				getLhs(),
				var,
				replacement);
		    SimpleTptpParserOutput.Formula newRHS = 
			replace(((SimpleTptpParserOutput.Formula.Binary)form).
				getRhs(),
				var,
				replacement);

			return 
			    (SimpleTptpParserOutput.Formula)
			    _syntaxFactory.createBinaryFormula(newLHS,
							       connective,
							       newRHS);
		}

	    case Negation:
		return
		    (SimpleTptpParserOutput.Formula)
		    _syntaxFactory.
		    createNegationOf(replace(((SimpleTptpParserOutput.Formula.Negation)form).
					     getArgument(),
					     var,
					     replacement));
		
	    case Quantified:
		{
		    LinkedList<String> quantifiedVarAsList = new LinkedList<String>();
		    
		    quantifiedVarAsList.add(((SimpleTptpParserOutput.Formula.Quantified)form).
					    getVariable());
		    
		    return
			(SimpleTptpParserOutput.Formula)
			_syntaxFactory.
			createQuantifiedFormula(((SimpleTptpParserOutput.Formula.Quantified)form).
						getQuantifier(),
						quantifiedVarAsList,
						replace(((SimpleTptpParserOutput.Formula.Quantified)form).
							getMatrix(),
							var,
							replacement));
		}
					    
	    } // switch (formula.getKind())
	
	assert false;
	
	return null;

    } // replace(SimpleTptpParserOutput.Formula form,
	       

    /** Replaces all the occurences of the variable <code>var</code>
     *  in <code>term/code> by the term <code>replacement</code>.
     */
    private 
	SimpleTptpParserOutput.Term 
	replace(SimpleTptpParserOutput.Term term,
		String var,
		SimpleTptpParserOutput.Term replacement) {

	if (term.getTopSymbol().isVariable())
	    if (term.getTopSymbol().getText().equals(var))
		{
		    return replacement;
		}
	    else 
		return term;
	    
	if (term.getNumberOfArguments() == 0) 
	    return term;


	LinkedList<TptpParserOutput.Term> newArgs = 
	    new LinkedList<TptpParserOutput.Term>();
	
	for (TptpParserOutput.Term arg : term.getArguments())
	    newArgs.addLast(replace((SimpleTptpParserOutput.Term)arg,var,replacement));

	
	return 
	    (SimpleTptpParserOutput.Term)
	    _syntaxFactory.
	    createPlainTerm(term.getTopSymbol().getText(),
			    newArgs);

    } // replace(SimpleTptpParserOutput.Term term,
	       










    private String tptpfyURI(URI uri,String prefix) {
	if (uri.getFragment() == null)
	    {
		String pathSuffix = pathSuffix(uri.getPath());
		if (!pathSuffix.equals(""))
		    return prefix + pathSuffix;		    
		
		return "'" + uri.toString() + "'";
	    };
	
	return tptpfyId(prefix + uri.getFragment()); 

    } // tptpfyURI(URI uri,String prefix)
    

    
    private String tptpfyId(String id) {

	String res = id;

	res = res.replace('-','_');
	res = res.replace('%','_');
	res = res.replace('.','_');
	
	return res;

    } // tptpfyId(String id)
    


    /** Maps the URI as a predicate of the specified arity into a TPTP identifier;
     *  the mapping is consistent with the previous identifier assignments.
     */
    private String predicateForURI(URI uri,int arity)  {
		
	Set<TPTPSymbolDescriptor> symDescSet = 
	    _parameters.URIsToTPTPSymbols().get(uri.toString());

	if (symDescSet == null)
	    {
		symDescSet = new HashSet<TPTPSymbolDescriptor>();
		_parameters.URIsToTPTPSymbols().put(uri.toString(),symDescSet);
	    }
	else
	    {
		// Check if the predicate has already been mapped:
		for (TPTPSymbolDescriptor desc : symDescSet)
		    if (desc.isPredicate() && desc.arity() == arity)
			return desc.name();
	    };

	String result = tptpfyURI(uri,"p_");
	
	if (symbolIsReserved(true,result,arity))
	    {
		int i;
		for (i = 0;
		     symbolIsReserved(true,result + i,arity);
		     ++i);
		result = result + i;
	    };

	TPTPSymbolDescriptor desc = 
	    new TPTPSymbolDescriptor(true,result,arity);

	symDescSet.add(desc);

	_reservedSymbols.add(desc);
     

	return result;

    } // predicateForURI(URI uri,int arity)



    /** Maps the URI as an individual constant;
     *  the mapping is consistent with the previous identifier assignments.
     */
    private String individualConstantForURI(URI uri) {

	Set<TPTPSymbolDescriptor> symDescSet = 
	    _parameters.URIsToTPTPSymbols().get(uri.toString());

	if (symDescSet == null)
	    {
		symDescSet = new HashSet<TPTPSymbolDescriptor>();
		_parameters.URIsToTPTPSymbols().put(uri.toString(),symDescSet);
	    }
	else
	    {
		// Check if the constant has already been mapped:
		for (TPTPSymbolDescriptor desc : symDescSet)
		    if (!desc.isPredicate() && desc.arity() == 0)
			return desc.name();
	    };

	String result = tptpfyURI(uri,"c_");
	
	if (symbolIsReserved(false,result,0))
	    {
		int i;
		for (i = 0;
		     symbolIsReserved(false,result + i,0);
		     ++i);
		result = result + i;
	    };

	TPTPSymbolDescriptor desc = 
	    new TPTPSymbolDescriptor(false,result,0);

	symDescSet.add(desc);

	_reservedSymbols.add(desc);
     
	return result;

    } // individualConstantForURI(URI uri)

    
    private String freshSkolemIndividualConstant() {

	while (symbolIsReserved(false,"skc" + _nextSkolemSymIndex,0))
	    ++_nextSkolemSymIndex;

	String result = "skc" + _nextSkolemSymIndex++;

	TPTPSymbolDescriptor desc = 
	    new TPTPSymbolDescriptor(false,result,0);

	_reservedSymbols.add(desc);
	_parameters.otherReservedSymbols().add(desc);

	return result;

    } // freshSkolemIndividualConstant()
    

    /** Works for constants too. */
    private String freshSkolemFunction(int arity) {
	
    if (arity == 0) return freshSkolemIndividualConstant();
	
	while (symbolIsReserved(false,"skf" + _nextSkolemSymIndex,arity))
	    ++_nextSkolemSymIndex;

	String result = "skf" + _nextSkolemSymIndex++;

	TPTPSymbolDescriptor desc = 
	    new TPTPSymbolDescriptor(false,result,arity);

	_reservedSymbols.add(desc);
	_parameters.otherReservedSymbols().add(desc);

	return result;
	
    } // freshSkolemFunction(int arity)


    private String predicateForDatatype(OWLDatatype t) 
    {
	return predicateForURI(t.getIRI().toURI(),1);
    }

    

    private String predicateForDataDomain() {
	String result = _parameters.dataDomainPredicate();
	if (result != null) return result;
	
	result = "dataDomain";

	if (symbolIsReserved(true,result,1))
	    {
		int i;
		for (i = 0; symbolIsReserved(true,result + i,1); ++i);
		result = result + i;
	    };
	
	_parameters.setDataDomainPredicate(result);
	
	_reservedSymbols.add(new TPTPSymbolDescriptor(true,result,1));

	
	return result;

    } // predicateForDataDomain()



    private String predicateForOwlThing() {    
	try
	    {
		return 
		    predicateForURI(new URI("http://www.w3.org/2002/07/owl#Thing"),
				    1);
	    }
	catch (URISyntaxException ex) 
	    {
		throw 
		    new Error("Cannot process standard URI http://www.w3.org/2002/07/owl#Thing : " + ex);
	    }
    } // predicateForOwlThing()


    /** Unary TPTP predicate for http://www.w3.org/2002/07/owl#Nothing. */
    private String predicateForOwlNothing() {   
	try
	    { 
		return 
		    predicateForURI(new URI("http://www.w3.org/2002/07/owl#Nothing"),
				    1);
	    }
	catch (URISyntaxException ex) 
	    {
		throw 
		    new Error("Cannot process standard URI http://www.w3.org/2002/07/owl#Nothing : " + ex);
	    }
    } // predicateForOwlNothing()

    

    private String equalityPredicate() {

	String result = _parameters.equalityPredicate();
	if (result != null) return result;
	
	result = "=";

	assert !symbolIsReserved(true,result,2);

	_parameters.setEqualityPredicate(result);

	return result;


    } // equalityPredicate() 
    
    
    
    /** Id for the constructor function for integer literals. */
    private String intLitFunc() {
	String result = _parameters.intLitFunc();
	if (result != null) return result;
	
	result = "intLit";

	if (symbolIsReserved(false,result,1))
	    {
		int i;
		for (i = 0; symbolIsReserved(false,result + i,1); ++i);
		result = result + i;
	    };

	_parameters.setIntLitFunc(result);

	return result;
    }

    /** Id for the constructor function for string literals
     *  with no language specified.
     */
    private String stringLitNoLangFunc() {
	String result = _parameters.stringLitNoLangFunc();
	if (result != null) return result;
	
	result = "stringLitNoLang";

	if (symbolIsReserved(false,result,1))
	    {
		int i;
		for (i = 0; symbolIsReserved(false,result + i,1); ++i);
		result = result + i;
	    };


	_parameters.setStringLitNoLangFunc(result);

	return result;
    }

    /** Id for the constructor function for string literals
     *  with some language specified.
     */
    private String stringLitWithLangFunc() { 

	String result = _parameters.stringLitWithLangFunc();
	if (result != null) return result;
	
	result = "stringLitWithLang";

	if (symbolIsReserved(false,result,2))
	    {
		int i;
		for (i = 0; symbolIsReserved(false,result + i,1); ++i);
		result = result + i;
	    };


	_parameters.setStringLitWithLangFunc(result);

	return result;
    }
 


    private String typedLitFunc() {

	String result = _parameters.typedLitFunc();
	if (result != null) return result;
	
	result = "typedLit";

	if (symbolIsReserved(false,result,2))
	    {
		int i;
		for (i = 0; symbolIsReserved(false,result + i,1); ++i);
		result = result + i;
	    };

	_parameters.setTypedLitFunc(result);

	return result;
    }
    



    private String generateNewVariable(String prefix) {
	return prefix + _nextVarIndex++;
    }
    
    
    /** Extracts the last segment of the URI path, ie, the substring
     *  (possibly empty) following the last '/' in the path.
     *  @param path can be null, in which case the result is ""
     */
    private String pathSuffix(String path) {
	
	if (path == null) return "";
	
	for (int i = path.length() - 1; i >= 0; --i)
	    if (path.charAt(i) == '/')
		return path.substring(i + 1);
	
	// No '/'
	
	return path;

    } // pathSuffix(String path)


    /** Checks if a symbol with the specified parameters 
     *  has already been reserved in some way, so that a new
     *  symbol with these parameters cannot be introduced.
     */
    private boolean symbolIsReserved(boolean predicate,
				     String name,
				     int arity) {

	TPTPSymbolDescriptor desc = 
	    new TPTPSymbolDescriptor(predicate,name,arity);

	return _reservedSymbols.contains(desc);

    } // symbolIsReserved(boolean predicate,..)








    //            Data:

    /** Various parameters that guide the conversion; some of 
     *  the parameters are modified by the conversion within this object
     *  and can be used afterwards in other objects; eg, mappings
     *  of URIs to TPTP identifiers are augmented when new URIs are
     *  encountered.
     */
    private Parameters _parameters;

    private boolean _generateCNF;

    /** Current ontology being converted. */
    private OWLOntology _currentOntology;	

    /** Factory for TPTP abstract syntax datastructures. */
    private SimpleTptpParserOutput _syntaxFactory;

    // The value is reset to 0 every time a new axiom is converted.
    private int _nextVarIndex;

    private int _nextSkolemSymIndex;

    private int _nextAxiomIndex;

    /** Fast access to all reserved symbols. */
    private HashSet<TPTPSymbolDescriptor> _reservedSymbols;
    
} // class Converter