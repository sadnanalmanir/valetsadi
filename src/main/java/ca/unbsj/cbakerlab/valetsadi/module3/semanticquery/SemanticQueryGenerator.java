package ca.unbsj.cbakerlab.valetsadi.module3.semanticquery;


import ca.unbsj.cbakerlab.valetsadi.Main;
import ca.unbsj.cbakerlab.valetsadi.module3.SQLGenerationException;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxOWLObjectRendererImpl;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class SemanticQueryGenerator {

    private static final Logger logger = Logger.getLogger(SemanticQueryGenerator.class);

    ManchesterOWLSyntaxOWLObjectRendererImpl r = new ManchesterOWLSyntaxOWLObjectRendererImpl();
    private Graph inputGraph;// = new TinkerGraph();
    private Graph outputGraph;// = new TinkerGraph();



    private String inputVarSymbol = "X";
    private String outputVarSymbol = "Y";
    private String ESCAPE_DOUBLE_QUOTE_SYMBOL = "\"";
    private final String OPEN_PAR = "(";
    private final String CLOSED_PAR = ")";
    private final String COMMA = ",";
    private final String SPACE = " ";
    private final String PREFIX_ATOM_PRED = "--p_";
    private final String PREFIX_ANSWER_PRED = "++answer";

    private String TPTP_SPECIALIZED_QUERY = "";
    private List<String> inAtomsPredicates = new LinkedList<String>();
    private List<String> outAtomsPredicates = new LinkedList<String>();
    //answer predicates are of the form ++answer(Y1, Y2, ..., YN)
    private List<String> answerVariables = new LinkedList<String>();
    private List<String> varsForSQLWhereClause = new LinkedList<String>();

    public String generateSemanticQuery(Graph inputGraph, OWLClassExpression inputClassExpr, Graph outputGraph, OWLClassExpression outputClassExpr) {
        this.inputGraph = inputGraph;
        this.outputGraph = outputGraph;

        createInputAtomsPredicates();
        createOutputAtomsPredicates();
        assignSameRootNodeForOutputGraph();
        createAnswerAtomsPredicatesFromOutput(outputClassExpr);
        assignSameRootNodeForAnswerVars();
        String tptpQuery = creteTPTP_SPECIALIZED_QUERY();
        createVarsForSQLWhereClause(inputClassExpr);
        Map<String, String> nodeIdtoEntityMap = new HashMap<String, String>(getNodeIdtoEntityMap());
        tptpQuery = replaceWithNodeIdtoEntityMap(tptpQuery, nodeIdtoEntityMap);
        //Map<String, String> nodeIdtoIndNameForInput = new HashMap<String, String>(getNodeIdtoIndNameForInput());
        //tptpQuery = replaceInputIndNodesWithEntityPrefixMapping(tptpQuery, nodeIdtoIndNameForInput);
        //Map<String, String> nodeIdtoIndNameForOutput = new HashMap<String, String>(getNodeIdtoIndNameForOutput());
        //tptpQuery = replaceOutputIndNodesWithEntityPrefixMapping(tptpQuery, nodeIdtoIndNameForOutput);
        tptpQuery = assignDoubleQuotesForInputVars(tptpQuery);

        return tptpQuery;
    }

    private void createInputAtomsPredicates() {
        for (Edge edge : inputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                //String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                //inAtomsPredicates.add(edge.getVertex(Direction.IN).getProperty("name").toString()
                //        + OPEN_PAR + indNodeName + OPEN_PAR + (inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + CLOSED_PAR + CLOSED_PAR);
                inAtomsPredicates.add(edge.getVertex(Direction.IN).getProperty("name").toString()
                        + OPEN_PAR + (inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + CLOSED_PAR);
            } else {
                inAtomsPredicates.add(edge.getLabel()
                        + OPEN_PAR + inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString() + COMMA + SPACE + inputVarSymbol + edge.getVertex(Direction.IN).getId().toString() + CLOSED_PAR);
            }
        }
    }

    private void createOutputAtomsPredicates() {
        for (Edge edge : outputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                //String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                //outAtomsPredicates.add(edge.getVertex(Direction.IN).getProperty("name").toString()
                //+ OPEN_PAR + indNodeName + OPEN_PAR + (outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + CLOSED_PAR + CLOSED_PAR);
                outAtomsPredicates.add(edge.getVertex(Direction.IN).getProperty("name").toString()
                        + OPEN_PAR + (outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + CLOSED_PAR);
            } else {
                outAtomsPredicates.add(edge.getLabel()
                        + OPEN_PAR + outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString() + COMMA + SPACE + outputVarSymbol + edge.getVertex(Direction.IN).getId().toString() + CLOSED_PAR);
            }
        }
    }

    private void assignSameRootNodeForOutputGraph() {
        //log.info("--------------------------------------------");
        logger.info("Setting same root in output atoms/predciates");
        //log.info("--------------------------------------------");
        for (int i = 0; i < outAtomsPredicates.size(); i++) {
            String temp = outAtomsPredicates.get(i);
            if (temp.contains("Y0")) {
                outAtomsPredicates.set(i, temp.replace("Y0", "X0"));
            }
        }
    }

    private void createAnswerAtomsPredicatesFromOutput(OWLClassExpression classExpr) {

        //When there are datatypes , use them as answer variables
        for (Edge edge : outputGraph.getEdges()) {
            if (isDataproperty(edge.getLabel(), classExpr)) {
                answerVariables.add((outputVarSymbol + edge.getVertex(Direction.IN).getId().toString()));
            }
        }
        // if there are no answer variables acuired, there is no datatypes mentioned in the output expresion
        //  Consider individuals as answer variables e.g. type value Person
        if (answerVariables.isEmpty()) {
            for (Edge edge : outputGraph.getEdges()) {
                if (edge.getLabel().equals("type")) {
                    //String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                    //answerVariables.add(indNodeName + OPEN_PAR + (outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + CLOSED_PAR);
                    answerVariables.add((outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()));
                }
            }
        }

    }

    private void assignSameRootNodeForAnswerVars() {
        //log.info("-----------------------------------------------------");
        logger.info("Setting same root for ++answer(X1, ... ,XN) Variables");
        //log.info("-----------------------------------------------------");
        for (int i = 0; i < answerVariables.size(); i++) {
            String temp = answerVariables.get(i);
            if (temp.contains("Y0")) {
                answerVariables.set(i, temp.replace("Y0", "X0"));
            }
        }
    }

    private String creteTPTP_SPECIALIZED_QUERY() {
        for (int i = 0; i < inAtomsPredicates.size(); i++) {
            TPTP_SPECIALIZED_QUERY += PREFIX_ATOM_PRED + inAtomsPredicates.get(i);
            TPTP_SPECIALIZED_QUERY += COMMA + SPACE;
        }
        for (int i = 0; i < outAtomsPredicates.size(); i++) {
            TPTP_SPECIALIZED_QUERY += PREFIX_ATOM_PRED + outAtomsPredicates.get(i);
            TPTP_SPECIALIZED_QUERY += COMMA + SPACE;
        }
        TPTP_SPECIALIZED_QUERY += PREFIX_ANSWER_PRED + OPEN_PAR;
        for (int i = 0; i < answerVariables.size(); i++) {
            TPTP_SPECIALIZED_QUERY += answerVariables.get(i);

            if (i < (answerVariables.size() - 1)) {
                TPTP_SPECIALIZED_QUERY += COMMA + SPACE;
            }
        }

        TPTP_SPECIALIZED_QUERY += CLOSED_PAR;

        return TPTP_SPECIALIZED_QUERY;
    }

    private boolean isDataproperty(String property, OWLClassExpression classExpr) {
        /*Set<OWLEntity> entities = classExpr.getSignature();
         for (OWLEntity entity : entities) {
         if (entity.isOWLDataProperty()) {
         if (property.equals(entity.asOWLDataProperty().getIRI().getFragment())) {
         return true;
         }
         }
         }
         return false;
         */
        for (OWLDataProperty dataProp : classExpr.getDataPropertiesInSignature()) {
            if (property.equals(dataProp.getIRI().getFragment())) {
                return true;
            }
        }
        return false;
    }

    private void createVarsForSQLWhereClause(OWLClassExpression classExpr) {

        // Find datatypes mentioned in the input class expression
        for (Edge edge : inputGraph.getEdges()) {
            if (isDataproperty(edge.getLabel(), classExpr)) {
                varsForSQLWhereClause.add((inputVarSymbol + edge.getVertex(Direction.IN).getId().toString()));
            }
        }
        // If there is no datatype property, check for instance of (OWLObjectHasValue) i.e. (type value Person)
        if (varsForSQLWhereClause.isEmpty()) {
            for (Edge edge : inputGraph.getEdges()) {
                if (edge.getLabel().equals("type")) {
                    // This resource must be connected to ROOT node referring to "Resource input" form the processInput method
                    // if (edge.getLabel().equals("type") && edge.getVertex(Direction.OUT).getId().toString().equals("0")) {
                    //String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                    //varsForSQLWhereClause.add(edge.getVertex(Direction.IN).getProperty("name").toString()
                    //    + OPEN_PAR + indNodeName + OPEN_PAR + (inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + CLOSED_PAR + CLOSED_PAR);
                    varsForSQLWhereClause.add(edge.getVertex(Direction.IN).getProperty("name").toString()
                            + (inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()));
                }
            }
        }
        if (varsForSQLWhereClause.isEmpty()) {
            logger.info("No value to instantiate semantic query. WHERE clause will have no input value to instantiate.");
        }
    }

    private Map<String, String> getNodeIdtoEntityMap() {
        Map<String, String> result = new HashMap<String, String>();
        for (Edge edge : inputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                result.put((inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()), indNodeName + OPEN_PAR + (inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + CLOSED_PAR);
            }
        }
        for (Edge edge : outputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                if(edge.getVertex(Direction.OUT).getId().toString().equals("0")) {
                    String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                    result.put((inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()), indNodeName + "(" + (outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + ")");
                } else {
                    String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                    result.put((outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()), indNodeName + "(" + (outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + ")");
                }
            }
        }
        return result;
    }


    private String replaceWithNodeIdtoEntityMap(String tptpQuery, Map<String, String> nodeIdtoIndName) {
        for (Map.Entry<String, String> entry : nodeIdtoIndName.entrySet()) {
            //System.out.println(entry.getKey() + "/" + entry.getValue());
            if (tptpQuery.contains(entry.getKey())) {
                tptpQuery = tptpQuery.replaceAll(entry.getKey(), entry.getValue());
            }
        }
        return tptpQuery;
    }



    /*
    private Map<String, String> getNodeIdtoIndNameForInput() {
        Map<String, String> result = new HashMap<String, String>();
        for (Edge edge : inputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                result.put((inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()), indNodeName + "(" + (inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + ")");
            }
        }
        return result;
    }



    private String replaceInputIndNodesWithEntityPrefixMapping(String tptpQuery, Map<String, String> nodeIdtoIndName) {
        for (Map.Entry<String, String> entry : nodeIdtoIndName.entrySet()) {
            //System.out.println(entry.getKey() + "/" + entry.getValue());
            if (tptpQuery.contains(entry.getKey())) {
                tptpQuery = tptpQuery.replaceAll(entry.getKey(), entry.getValue());
            }
        }
        return tptpQuery;
    }
    private Map<String, String> getNodeIdtoIndNameForOutput() {
        Map<String, String> result = new HashMap<String, String>();
        for (Edge edge : outputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                result.put((outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()), indNodeName + "(" + (outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + ")");
            }
        }
        return result;
    }

    private String replaceOutputIndNodesWithEntityPrefixMapping(String tptpQuery, Map<String, String> nodeIdtoIndName) {
        for (Map.Entry<String, String> entry : nodeIdtoIndName.entrySet()) {
            //System.out.println(entry.getKey() + "/" + entry.getValue());
            if (tptpQuery.contains(entry.getKey())) {
                tptpQuery = tptpQuery.replaceAll(entry.getKey(), entry.getValue());
            }
        }
        return tptpQuery;
    }
    */

    private String assignDoubleQuotesForInputVars(String tptpQuery) {
        for (String var : varsForSQLWhereClause) {
            if (tptpQuery.contains(var)) {
                tptpQuery = tptpQuery.replaceAll(var, ESCAPE_DOUBLE_QUOTE_SYMBOL + var + ESCAPE_DOUBLE_QUOTE_SYMBOL);
            }
        }
        return tptpQuery;
    }



    public List<String> getVarsForSQLWhereClause() {
        return varsForSQLWhereClause;
    }

    public List<String> getAnswerVariables() {
        return answerVariables;
    }

    public String constructCompleteSemanticQuery(String queryPredicates, String DOMAIN_ONTOLOGY_TO_TPTP_FOF_PATH, String SEMANTIC_MAPPING_TO_TPTP_FOF_PATH) throws SQLGenerationException, IOException {
        String queryString = "% Semantic Query\n";

        // include domain ontology file as part Knowledge base
        List<File> domainOntfiles = getFileList(DOMAIN_ONTOLOGY_TO_TPTP_FOF_PATH);
        if(domainOntfiles.size() == 0)
            throw new SQLGenerationException("Error locating domain ontology in TPTP-FOF");
        for (File file : domainOntfiles) {
            queryString += "include('" + DOMAIN_ONTOLOGY_TO_TPTP_FOF_PATH + FilenameUtils.getName(file.getName()) +"').\n";
            //queryString += "include('" + file.getCanonicalPath() +"').\n";
        }

        // include semantic mapping rules file as part Knowledge base
        List<File> mappingfiles = getFileList(SEMANTIC_MAPPING_TO_TPTP_FOF_PATH);
        if(mappingfiles.size() == 0)
            throw new SQLGenerationException("Error locating semantic mapping rules in TPTP-FOF");
        for (File file : mappingfiles) {
            queryString += "include('" + SEMANTIC_MAPPING_TO_TPTP_FOF_PATH + FilenameUtils.getName(file.getName()) +"').\n";
            //queryString += "include('" + file.getCanonicalPath() +"').\n";
        }

        queryString += "\n"
                + "input_clause(semanticquery,conjecture,\n"
                + "[\n";
        queryString += queryPredicates;

        queryString += "\n" + "]).";

        return queryString;
    }

    private List<File> getFileList(String dirPath) {
        File dir;
        dir = new File(dirPath);
        String[] extensions = new String[] { "tptp"};
        System.out.println("Getting all *.tptp files in " + dir.getPath());
        return (List<File>) FileUtils.listFiles(dir, extensions, true);
    }


}
