package ca.unbsj.cbakerlab.valetsadi.module3;



import ca.unbsj.cbakerlab.valetsadi.module3.owltotptp.OWL2TPTP;
import ca.unbsj.cbakerlab.valetsadi.module3.schematicanswers.SchematicAnswersGenerator;
import ca.unbsj.cbakerlab.valetsadi.module3.semanticquery.SemanticQueryGenerator;
import ca.unbsj.cbakerlab.valetsadi.module3.sqltemplatequery.querymanager.SAILQueryManager;
import com.tinkerpop.blueprints.Graph;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLClassExpression;



import java.io.IOException;
import java.util.Collection;

public class Executor {

    private static final Logger logger = Logger.getLogger(Executor.class);

    private SQLGenerationUtility sqlGenerationUtility = new SQLGenerationUtility();
    private SemanticQueryGenerator semanticQueryGenerator = new SemanticQueryGenerator();
    private SchematicAnswersGenerator schematicAnswersGenerator = new SchematicAnswersGenerator();

    public String translateOntologyToTptpFofAxiom(String targetOntologyURI) {
        return OWL2TPTP.translateOWLToTPTP(targetOntologyURI);
    }


    public String saveTranslatedOntology(String domainOntologyToTptpFofPath, String domainOntologyName, String ontologyInTptpFof, String fileType) {
        return sqlGenerationUtility.saveFileAs(domainOntologyToTptpFofPath, domainOntologyName, ontologyInTptpFof, fileType);
    }

    public String createSemanticQueryFromIOGraphs(Graph inputGraph, OWLClassExpression inputClassExpr, Graph outputGraph, OWLClassExpression outputClassExpr, String DOMAIN_ONTOLOGY_TO_TPTP_FOF_PATH, String SEMANTIC_MAPPING_TO_TPTP_FOF_PATH) throws SQLGenerationException, IOException {


        logger.info("--------------------------------------------------------");
        logger.info("* Checking if a semantic query is possible to generate *");
        logger.info("--------------------------------------------------------");
        if (outputClassExpr.getDataPropertiesInSignature().isEmpty() && outputClassExpr.getObjectPropertiesInSignature().isEmpty()) {
            throw new SQLGenerationException("Cannot create semantic query. No individuals and data types found in output class expression.");
        } else
            logger.info("* Creating the query ...");

        logger.info("--------------------------------------------------------");
        logger.info("* Generating semantic query from I/O Class Expressions *");
        logger.info("--------------------------------------------------------");

        String queryPredicates = semanticQueryGenerator.generateSemanticQuery(inputGraph,inputClassExpr,outputGraph,outputClassExpr);

        logger.info("-----------------------------------------------");
        logger.info("* Printing  predicates for the semantic query *");
        logger.info("-----------------------------------------------");
        logger.info(queryPredicates);

        logger.info("------------------------------------------");
        logger.info("* Constructing a complete semantic query *");
        logger.info("------------------------------------------");
        String completeSemQuery;
        completeSemQuery = semanticQueryGenerator.constructCompleteSemanticQuery(queryPredicates, DOMAIN_ONTOLOGY_TO_TPTP_FOF_PATH, SEMANTIC_MAPPING_TO_TPTP_FOF_PATH);

        return completeSemQuery;
    }

    public String saveSemanticQuery(String semanticQueryPath, String serviceName, String queryContent, String fileType) {
        return sqlGenerationUtility.saveFileAs(semanticQueryPath, serviceName, queryContent, fileType);
    }


    public String generateSchematicAnswers(String VAMPIRE_PRIME_FOL_REASONER_PATH, String ANSWER_PREDICATES_FILE_PATH, String EXTENSIONAL_PREDICATES_FILE_PATH, String semanticQueryPath) throws IOException {

        return schematicAnswersGenerator.generateSchematicAnswersFromSemanticQuery(VAMPIRE_PRIME_FOL_REASONER_PATH, ANSWER_PREDICATES_FILE_PATH, EXTENSIONAL_PREDICATES_FILE_PATH, semanticQueryPath);

    }


    public String saveSchematicAnswers(String schematicAnswersPath, String schematicAnswersFileName, String schematicAnswers, String schematicAnswersFileExtention) {
        return sqlGenerationUtility.saveFileAs(schematicAnswersPath, schematicAnswersFileName, schematicAnswers, schematicAnswersFileExtention);
    }

    public String generateSQLTemplateQuery(String schematicAnswers, String PREDICATE_VIEWS_FILE) {
        String sqlTemplateQuery = null;
        SAILQueryManager sailQueryManager = new SAILQueryManager();
        String[] args = new String[]{"--output-xml", "off", "--ecnomical-joins", "off", "--table-query-patterns", PREDICATE_VIEWS_FILE};
        try {
            // the args are empty here, the callee method has the assigned arguments
            sqlTemplateQuery = sailQueryManager.generateSQLTemplate(args, schematicAnswers);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlTemplateQuery;
    }

    public String saveSqlTemplateQuery(String sqlTemplateQueryPath, String sqlTemplateQueryFileName, String sqlTemplateQuery, String sqlFileExtention) {
        return sqlGenerationUtility.saveFileAs(sqlTemplateQueryPath, sqlTemplateQueryFileName, sqlTemplateQuery, sqlFileExtention);
    }

    public Collection<? extends String> getInputVariables() {
        return semanticQueryGenerator.getVarsForSQLWhereClause();
    }


    public Collection<? extends String> getOutputVariables() {
        return semanticQueryGenerator.getAnswerVariables();
    }
}
