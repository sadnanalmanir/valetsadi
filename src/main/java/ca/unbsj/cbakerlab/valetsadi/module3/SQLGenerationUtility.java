package ca.unbsj.cbakerlab.valetsadi.module3;

import java.io.File;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;

class SQLGenerationUtility {

    String saveFileAs(String path, String fileName, String fileContent, String extention) {

        String pathToSave;
        pathToSave = "";

        try {

            File dir = new File(path);
            File file = new File(dir, fileName + "." + extention);
            pathToSave = path + fileName + "." + extention;
            FileUtils.writeStringToFile(file,fileContent, Charset.forName("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pathToSave;
    }


}
