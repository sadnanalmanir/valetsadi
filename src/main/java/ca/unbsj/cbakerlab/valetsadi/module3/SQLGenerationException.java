package ca.unbsj.cbakerlab.valetsadi.module3;

public class SQLGenerationException extends Exception {
    public SQLGenerationException() {
        super();
    }

    public SQLGenerationException(String message) {
        super(message);
    }

    public SQLGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}