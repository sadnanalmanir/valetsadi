package ca.unbsj.cbakerlab.valetsadi.module3.schematicanswers;


import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class SchematicAnswersGenerator {

    private static final Logger logger = Logger.getLogger(SchematicAnswersGenerator.class);


    public String generateSchematicAnswersFromSemanticQuery(String VAMPIRE_PRIME_FOL_REASONER_PATH, String ANSWER_PREDICATES_FILE_PATH, String EXTENSIONAL_PREDICATES_FILE_PATH, String semanticQueryPath) throws IOException {

        final HashMap<String, String> substitutionMap = new HashMap<String, String>();
        substitutionMap.put("-I", VAMPIRE_PRIME_FOL_REASONER_PATH);  // option  = -I, value = directory
        substitutionMap.put("-t", "10");  // option  = -t, value = 10
        substitutionMap.put("-m", "300000");  // option  = -m, value = 300000
        substitutionMap.put("--elim_def", "0");  // option  = -elim_def, value = 0
        substitutionMap.put("--selection", "3");  // option  = --selection, value = 3
        substitutionMap.put("--configExtensionalPreds", EXTENSIONAL_PREDICATES_FILE_PATH);  // option  = --config, value = extensional predicates
        substitutionMap.put("--configAnswerPreds", ANSWER_PREDICATES_FILE_PATH);  // option  = --config, value = answer predicates
        substitutionMap.put("--max_number_of_answers", "1000");  // option  = --max_number_of_answers, value = 1000
        substitutionMap.put("--silent", "on");  // option  = --selection, value = off/on
        substitutionMap.put("--show_answers_as_xml", "on");  // option  = --selection, value = on/off

        final CommandLine commandLine;
        commandLine = new CommandLine(VAMPIRE_PRIME_FOL_REASONER_PATH + "vkernel");
        //commandLine.addArgument("-I");
        //commandLine.addArgument("${-I}");
        commandLine.addArgument("-t");
        commandLine.addArgument("${-t}");
        commandLine.addArgument("-m");
        commandLine.addArgument("${-m}");
        commandLine.addArgument("--elim_def");
        commandLine.addArgument("${--elim_def}");
        commandLine.addArgument("--selection");
        commandLine.addArgument("${--selection}");
        commandLine.addArgument("--config");
        commandLine.addArgument("${--configExtensionalPreds}");
        commandLine.addArgument("--config");
        commandLine.addArgument("${--configAnswerPreds}");
        commandLine.addArgument("--max_number_of_answers");
        commandLine.addArgument("${--max_number_of_answers}");
        commandLine.addArgument("--silent");
        commandLine.addArgument("${--silent}");
        commandLine.addArgument("--show_answers_as_xml");
        commandLine.addArgument("${--show_answers_as_xml}");
        commandLine.addArgument(semanticQueryPath);
        commandLine.setSubstitutionMap(substitutionMap);

        logger.info("----------------------------------------------");
        logger.info("* Command executing on VampirePrime reasoner *");
        logger.info("----------------------------------------------");
        logger.info(commandLine.toString());

        final DefaultExecutor executor = new DefaultExecutor();
        final ExecuteWatchdog watchdog = new ExecuteWatchdog(2 * 1000); // allow process no more than 2 secs
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final PumpStreamHandler pumpStreamHandler = new PumpStreamHandler(byteArrayOutputStream, System.err);
        executor.setWatchdog(watchdog);
        executor.setStreamHandler(pumpStreamHandler);
        final long startTime = System.currentTimeMillis();
        executor.execute(commandLine);
        final long duration = System.currentTimeMillis() - startTime;
        logger.info("Process completed in " + duration + " millis");
        if (watchdog.killedProcess()) {
            logger.info("Process timed out and was killed by watchdog.");
        }

        return byteArrayOutputStream.toString();

    }
}
