package ca.unbsj.cbakerlab.valetsadi.module2;

public class ServiceOntologyException extends Exception {
    public ServiceOntologyException(){
        super();
    }
    public ServiceOntologyException(String message){
        super(message);
    }
    public ServiceOntologyException(String message, Throwable cause){
        super(message, cause);
    }
    public ServiceOntologyException(Throwable cause){
        super(cause);
    }



}
