package ca.unbsj.cbakerlab.valetsadi.module2;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

class HYDRACompatibilityChecker {

    private static final Logger logger = Logger.getLogger(HYDRACompatibilityChecker.class);

    boolean isInputHYDRACompatible(String inputClassURI, String fileForHydraCompatibility) {

        String sadiDescriptionValidatorPath = "";

        try {
            sadiDescriptionValidatorPath = new File(logger.getClass().getResource("/" + fileForHydraCompatibility).toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String command = "java -cp " + sadiDescriptionValidatorPath + " com.ipsnp.hydra.sadi.ServiceDescriptionCheckShell --single-service-ontology-model true --input " + inputClassURI;
        logger.info("-- Command begins --");
        logger.info(command);
        logger.info("-- Command ends   --");
        String inputValidationResult = executeCommand(command);
        logger.info("-- Validation Result begins --");
        logger.info(inputValidationResult);
        logger.info("-- Validation Result ends --");

        return !(StringUtils.contains(inputValidationResult, "Undefined nonterminal in input class")
                || StringUtils.contains(inputValidationResult, "Cannot retrieve the definition for input class"));

    }


    boolean isOutputHYDRACompatible(String outputClassURI, String fileForHydraCompatibility) {

        String sadiDescriptionValidatorPath = "";

        try {
            sadiDescriptionValidatorPath = new File(logger.getClass().getResource("/" + fileForHydraCompatibility).toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String command = "java -cp " + sadiDescriptionValidatorPath + " com.ipsnp.hydra.sadi.ServiceDescriptionCheckShell --single-service-ontology-model true" + " --output " + outputClassURI;
        logger.info("-- Command begins --");
        logger.info(command);
        logger.info("-- Command ends   --");
        String outputValidationResult = executeCommand(command);
        logger.info("-- Validation Result begins --");
        logger.info(outputValidationResult);
        logger.info("-- Validation Result ends --");

        return !(StringUtils.contains(outputValidationResult, "Undefined nonterminal in output class")
                || StringUtils.contains(outputValidationResult, "Cannot retrieve the definition for output class"));

    }

    private String executeCommand(String command) {
        StringBuilder output = new StringBuilder();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();
    }


}
