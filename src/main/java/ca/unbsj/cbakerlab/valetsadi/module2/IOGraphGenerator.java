package ca.unbsj.cbakerlab.valetsadi.module2;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.*;
import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxOWLObjectRendererImpl;

import java.util.Stack;

public class IOGraphGenerator {

    private static final Logger logger = Logger.getLogger(IOGraphGenerator.class);

    private ManchesterOWLSyntaxOWLObjectRendererImpl r = new ManchesterOWLSyntaxOWLObjectRendererImpl();
    private Graph inputGraph = new TinkerGraph();
    private Graph outputGraph = new TinkerGraph();

    private Stack<Vertex> parentVerticesForInput = new Stack<Vertex>();
    private static String propFromParentNodeForInput = "";

    private Stack<Vertex> parentVerticesForOutput = new Stack<Vertex>();
    private static String propFromParentNodeForOutput = "";

    String inputVarSymbol = "X";
    String outputVarSymbol = "Y";

    public void generateIOGraphs(OWLClassExpression inputClassExpr, OWLClassExpression outputClassExpr) throws ServiceOntologyException {
        generateTreeFromInputExpr(inputClassExpr);
        generateTreeFromOutputExpr(outputClassExpr);
        displayInputGraphContent();
        displayOutputGraphContent();
    }

    public Graph getInputExprGraph() {
        return inputGraph;
    }

    public Graph getOutputExprGraph() {
        return outputGraph;
    }

    private void displayInputGraphContent() {
        logger.info("----------------------------");
        logger.info("Printing input graph content");
        logger.info("----------------------------");
        logger.info("[id#] subject -- property --> object");
        for (Edge edge : inputGraph.getEdges()) {
            System.out.println(edge.getVertex(Direction.OUT).getId() + " # " + edge.getVertex(Direction.OUT).getProperty("name") + " --  " + edge.getLabel() + "  --> " + edge.getVertex(Direction.IN).getId() + " # " + edge.getVertex(Direction.IN).getProperty("name"));

        }
        logger.info("--------Vertices--------");
        for (Vertex v : inputGraph.getVertices()) {
            System.out.println("Vertex:" + v.getId() + " -> " + v.toString() + " " + v.getProperty("name"));
        }
    }

    private void displayOutputGraphContent() {
        logger.info("-----------------------------");
        logger.info("Printing output graph content");
        logger.info("-----------------------------");
        logger.info("[id#] subject -- property --> object");
        for (Edge edge : outputGraph.getEdges()) {
            System.out.println(edge.getVertex(Direction.OUT).getId() + " # " + edge.getVertex(Direction.OUT).getProperty("name") + " --  " + edge.getLabel() + "  --> " + edge.getVertex(Direction.IN).getId() + " # " + edge.getVertex(Direction.IN).getProperty("name"));
        }
        System.out.println("--------Vertices--------");
        for (Vertex v : outputGraph.getVertices()) {
            System.out.println("Vertex:" + v.getId() + " -> " + v.toString() + " " + v.getProperty("name"));
        }
    }

    private void generateTreeFromInputExpr(OWLClassExpression owlClassExpr) throws ServiceOntologyException {

        if (owlClassExpr instanceof OWLObjectIntersectionOf) {

            Vertex v = inputGraph.addVertex(null);
            v.setProperty("name", r.render(owlClassExpr));

            if (propFromParentNodeForInput.equals("")) {
                // Create vertices equal to the number of operands as below and move on
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Edge e = inputGraph.addEdge(null, parentVerticesForInput.pop(), v, propFromParentNodeForInput);
                    propFromParentNodeForInput = "";
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            for (int i = 0; i < ((OWLObjectIntersectionOf) owlClassExpr).getOperands().size(); i++) {
                parentVerticesForInput.push(v);
            }
            for (OWLClassExpression y : ((OWLObjectIntersectionOf) owlClassExpr).getOperands()) {
                generateTreeFromInputExpr(y);
            }
        }

        if (owlClassExpr instanceof OWLObjectSomeValuesFrom) {

            String property = ((OWLObjectSomeValuesFrom) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();

            if (propFromParentNodeForInput.equals("")) {
                // (type value Student) and (has_advisor some (type value Advisor))
                // Use root from the stored vertex from intersection
                // do nothing
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));
                    Edge e = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = ""; // this line may be ommitted
                    parentVerticesForInput.push(v1);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            propFromParentNodeForInput = property;
            generateTreeFromInputExpr(((OWLObjectSomeValuesFrom) owlClassExpr).getFiller());
        }

        if (owlClassExpr instanceof OWLObjectExactCardinality) {

            String property = ((OWLObjectExactCardinality) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();

            if (propFromParentNodeForInput.equals("")) {
                // (type value Student) and (has_advisor some (type value Advisor))
                // Use root from the stored vertex from intersection
                // do nothing
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));
                    Edge e = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = ""; // this line may be ommitted
                    parentVerticesForInput.push(v1);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            propFromParentNodeForInput = property;
            generateTreeFromInputExpr(((OWLObjectExactCardinality) owlClassExpr).getFiller());
        }

        // shape: [V1:(type value ind)--Edge:type-> V2:ind]
        if (owlClassExpr instanceof OWLObjectHasValue) {

            String property = ((OWLObjectHasValue) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();
            OWLIndividual ind = ((OWLObjectHasValue) owlClassExpr).getValue().asOWLNamedIndividual();
            String indFragment = ((OWLObjectHasValue) owlClassExpr).getValue().asOWLNamedIndividual().getIRI().getFragment();

            if (propFromParentNodeForInput.equals("")) {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e2 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v2, property);
                } else {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e = inputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = "";

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e2 = inputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }

        if (owlClassExpr instanceof OWLDataSomeValuesFrom) {

            String property = ((OWLDataSomeValuesFrom) owlClassExpr).getProperty().asOWLDataProperty().getIRI().getFragment();
            OWLDatatype datatype = ((OWLDataSomeValuesFrom) owlClassExpr).getFiller().asOWLDatatype();

            if (propFromParentNodeForInput.equals("")) {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v2, property);
                } else {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e = inputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = "";

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = inputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }

        if (owlClassExpr instanceof OWLDataExactCardinality) {

            String property = ((OWLDataExactCardinality) owlClassExpr).getProperty().asOWLDataProperty().getIRI().getFragment();
            OWLDatatype datatype = ((OWLDataExactCardinality) owlClassExpr).getFiller().asOWLDatatype();

            //Vertex v1 = inputGraph.addVertex(null);
            //v1.setProperty("name", r.render(owlClassExpr));
            if (propFromParentNodeForInput.equals("")) {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v2, property);
                } else {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e = inputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = "";

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = inputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }
        if (owlClassExpr instanceof OWLClass) {
            throw new ServiceOntologyException("Error: Unsupported OWLClass in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectUnionOf) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectUnionOf in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectComplementOf) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectComplementOf in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectAllValuesFrom) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectAllValuesFrom in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectMinCardinality) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectMinCardinality in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectMaxCardinality) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectMaxCardinality in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectHasSelf) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectHasSelf in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectOneOf) {
            logger.info("OWLObjectOneOf is supported in input class definition but IGNORED in creating atoms/predicates for creating TPTP query.");
        }
        if (owlClassExpr instanceof OWLDataAllValuesFrom) {
            throw new ServiceOntologyException("Error: Unsupported OWLDataAllValuesFrom in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataHasValue) {
            throw new ServiceOntologyException("Error: Unsupported OWLDataHasValue in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataMinCardinality) {
            throw new ServiceOntologyException("Error: Unsupported OWLDataMinCardinality in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataMaxCardinality) {
            throw new ServiceOntologyException("Error: Unsupported OWLDataMaxCardinality in input class expression: " + r.render(owlClassExpr));
        }
    }


    public void generateTreeFromOutputExpr(OWLClassExpression owlClassExpr) throws ServiceOntologyException {

        if (owlClassExpr instanceof OWLObjectIntersectionOf) {

            Vertex v = outputGraph.addVertex(null);
            v.setProperty("name", r.render(owlClassExpr));

            if (propFromParentNodeForOutput.equals("")) {
                // Create vertices equal to the number of operands as below and move on
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Edge e = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = "";
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            for (int i = 0; i < ((OWLObjectIntersectionOf) owlClassExpr).getOperands().size(); i++) {
                parentVerticesForOutput.push(v);
            }
            for (OWLClassExpression y : ((OWLObjectIntersectionOf) owlClassExpr).getOperands()) {
                generateTreeFromOutputExpr(y);
            }
        }

        if (owlClassExpr instanceof OWLObjectSomeValuesFrom) {

            String property = ((OWLObjectSomeValuesFrom) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();

            if (propFromParentNodeForOutput.equals("")) {
                // (type value Student) and (has_advisor some (type value Advisor))
                // Use root from the stored vertex from intersection
                // do nothing
                Vertex v = outputGraph.addVertex(null);
                v.setProperty("name", r.render(owlClassExpr));
                parentVerticesForOutput.push(v);
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));
                    Edge e = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = ""; // this line may be ommitted
                    parentVerticesForOutput.push(v1);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            propFromParentNodeForOutput = property;
            generateTreeFromOutputExpr(((OWLObjectSomeValuesFrom) owlClassExpr).getFiller());
        }

        if (owlClassExpr instanceof OWLObjectExactCardinality) {

            String property = ((OWLObjectExactCardinality) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();

            if (propFromParentNodeForOutput.equals("")) {
                // (type value Student) and (has_advisor some (type value Advisor))
                // Use root from the stored vertex from intersection
                // do nothing
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));
                    Edge e = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = ""; // this line may be ommitted
                    parentVerticesForOutput.push(v1);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            propFromParentNodeForOutput = property;
            generateTreeFromOutputExpr(((OWLObjectExactCardinality) owlClassExpr).getFiller());
        }

        // shape: [V1:(type value ind)--Edge:type-> V2:ind]
        if (owlClassExpr instanceof OWLObjectHasValue) {

            String property = ((OWLObjectHasValue) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();
            OWLIndividual ind = ((OWLObjectHasValue) owlClassExpr).getValue().asOWLNamedIndividual();
            String indFragment = ((OWLObjectHasValue) owlClassExpr).getValue().asOWLNamedIndividual().getIRI().getFragment();

            if (propFromParentNodeForOutput.equals("")) {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e2 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v2, property);
                } else {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e = outputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = "";

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e2 = outputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }

        if (owlClassExpr instanceof OWLDataSomeValuesFrom) {

            String property = ((OWLDataSomeValuesFrom) owlClassExpr).getProperty().asOWLDataProperty().getIRI().getFragment();
            OWLDatatype datatype = ((OWLDataSomeValuesFrom) owlClassExpr).getFiller().asOWLDatatype();

            if (propFromParentNodeForOutput.equals("")) {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v2, property);
                } else {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e = outputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = "";

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = outputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }

        if (owlClassExpr instanceof OWLDataExactCardinality) {

            String property = ((OWLDataExactCardinality) owlClassExpr).getProperty().asOWLDataProperty().getIRI().getFragment();
            OWLDatatype datatype = ((OWLDataExactCardinality) owlClassExpr).getFiller().asOWLDatatype();

            //Vertex v1 = outputGraph.addVertex(null);
            //v1.setProperty("name", r.render(owlClassExpr));
            if (propFromParentNodeForOutput.equals("")) {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v2, property);
                } else {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e = outputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = "";

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = outputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceOntologyException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }
        if (owlClassExpr instanceof OWLClass) {
            throw new ServiceOntologyException("Error: Unsupported OWLClass in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectUnionOf) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectUnionOf in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectComplementOf) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectComplementOf in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectAllValuesFrom) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectAllValuesFrom in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectMinCardinality) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectMinCardinality in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectMaxCardinality) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectMaxCardinality in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectHasSelf) {
            throw new ServiceOntologyException("Error: Unsupported OWLObjectHasSelf in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectOneOf) {
            logger.info("OWLObjectOneOf is supported in input class definition but IGNORED in creating atoms/predicates for creating TPTP query.");
        }
        if (owlClassExpr instanceof OWLDataAllValuesFrom) {
            throw new ServiceOntologyException("Error: Unsupported OWLDataAllValuesFrom in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataHasValue) {
            throw new ServiceOntologyException("Error: Unsupported OWLDataHasValue in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataMinCardinality) {
            throw new ServiceOntologyException("Error: Unsupported OWLDataMinCardinality in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataMaxCardinality) {
            throw new ServiceOntologyException("Error: Unsupported OWLDataMaxCardinality in output class expression: " + r.render(owlClassExpr));
        }
    }


}
