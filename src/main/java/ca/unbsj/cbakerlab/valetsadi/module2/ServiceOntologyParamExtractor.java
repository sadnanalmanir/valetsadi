package ca.unbsj.cbakerlab.valetsadi.module2;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLClassExpression;

import java.util.List;

public class ServiceOntologyParamExtractor {

    private static final Logger logger = Logger.getLogger(ServiceOntologyParamExtractor.class);

    private OntologyUtility ontologyUtility = new OntologyUtility();
    private HYDRACompatibilityChecker hydraCompatibilityChecker = new HYDRACompatibilityChecker();

    private String serviceOntologyURI;


    /**
     * Constructor.
     * @param serviceOntologyURI
     */
    public ServiceOntologyParamExtractor(String serviceOntologyURI) {
        this.serviceOntologyURI = serviceOntologyURI;
    }

    /**
     * Check validity of a URI.
     * @return truth value
     */
    public boolean isValidURI(String targetURI){
        return !ontologyUtility.isValidURI(targetURI);
    }


    /**
     * Get unique name of the service from the service ontology.
     * @return unique service name
     */
    public String getServiceNameFromURI() {
        String ontologyName  = ontologyUtility.getNameOfOntology(this.serviceOntologyURI);
        return ontologyUtility.getServiceNameFromOntologyName(ontologyName);
    }

    public String getOntologyNameFromURI(String targetURI) {
        return ontologyUtility.getNameOfOntology(targetURI);
    }

    public String getInputURIFromServiceOntology() {
        return ontologyUtility.getInputURIFromServiceOntology(this.serviceOntologyURI);
    }

    public String getOutputURIFromServiceOntology() {
        return ontologyUtility.getOutputURIFromServiceOntology(this.serviceOntologyURI);
    }

    public boolean isInputHYDRACompatible(String inputClassURI, String fileForHydraCompatibility) {
        return hydraCompatibilityChecker.isInputHYDRACompatible(inputClassURI, fileForHydraCompatibility);
    }

    public boolean isOutputHYDRACompatible(String outputClassURI, String fileForHydraCompatibility) {
        return hydraCompatibilityChecker.isOutputHYDRACompatible(outputClassURI, fileForHydraCompatibility);
    }

    public OWLClassExpression getInputClassExpr(String serviceOntologyURI, String inputClassURI) {
        return ontologyUtility.getOWLClassExpr(serviceOntologyURI, inputClassURI);
    }

    public OWLClassExpression getOutputClassExpr(String serviceOntologyURI, String outputClassURI) {
        return ontologyUtility.getOWLClassExpr(serviceOntologyURI, outputClassURI);
    }

    public List<String> getImportedOntologies() {
        return ontologyUtility.getImportedOntologies(this.serviceOntologyURI);
    }


}
