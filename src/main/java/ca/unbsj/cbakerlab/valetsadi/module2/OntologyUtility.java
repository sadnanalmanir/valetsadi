package ca.unbsj.cbakerlab.valetsadi.module2;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
//import org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxClassExpressionParser;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxOWLObjectRendererImpl;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


class OntologyUtility {

    private static final Logger logger = Logger.getLogger(OntologyUtility.class);

    private String inputClassURI;
    private String outputClassURI;

    public boolean isValidURI(String targetUrl) {

        int timeOut = 30000;
        HttpURLConnection httpUrlConn;
        try {
            httpUrlConn = (HttpURLConnection) new URL(targetUrl)
                    .openConnection();
            httpUrlConn.setRequestMethod("HEAD");

            // Set timeouts in milliseconds
            logger.info("Target URI: " + targetUrl);
            httpUrlConn.setConnectTimeout(timeOut);
            logger.info("Checking validity of target URL : Connection Timeout in 30000 ms");
            httpUrlConn.setReadTimeout(timeOut);
            logger.info("Checking validity of target URL : Reading Timeout in 30000 ms");
            // Print HTTP status code/message for your information.
            //System.out.println("Response Code: " + httpUrlConn.getResponseCode());
            //System.out.println("Response Message: " + httpUrlConn.getResponseMessage());
            return (httpUrlConn.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return false;
        }
    }

    String getNameOfOntology(String OntologyURI) {
        IRI ontologyIRI = IRI.create(OntologyURI);
        return ontologyIRI.getFragment();
    }

    String getServiceNameFromOntologyName(String ontologyName) {
        return StringUtils.substringBefore(ontologyName, ".owl");
    }


    String getInputURIFromServiceOntology(String serviceOntologyURI) {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        //OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);

        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            // get all classes from the ontology
            Set<OWLClass> classes = ontology.getClassesInSignature();
            for (OWLClass cls : classes) {
                if (cls.getIRI().getFragment().contains("Input") || cls.getIRI().getFragment().contains("input")) {
                    this.inputClassURI = cls.getIRI().toString();
                }
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return inputClassURI;
    }

    String getOutputURIFromServiceOntology(String serviceOntologyURI) {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        //OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);

        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            // get all classes from the ontology
            Set<OWLClass> classes = ontology.getClassesInSignature();
            for (OWLClass cls : classes) {
                if (cls.getIRI().getFragment().contains("Output") || cls.getIRI().getFragment().contains("output")) {
                    this.outputClassURI = cls.getIRI().toString();
                }
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return outputClassURI;
    }


    OWLClassExpression getOWLClassExpr(String serviceOntologyURI, String classExprURI) {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);
        ManchesterOWLSyntaxOWLObjectRendererImpl r = new ManchesterOWLSyntaxOWLObjectRendererImpl();
        //ManchesterOWLSyntaxClassExpressionParser parser = new ManchesterOWLSyntaxClassExpressionParser(df, null);
        OWLClassExpression classExpr = null;
        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            OWLClass outputClass = df.getOWLClass(IRI.create(classExprURI));
            classExpr = getClassExprAsEquivClassOrSubclass(ontology, outputClass);
            if (classExpr != null) {
                String classExprInManchesterSyntax = getClassExprInManchesterSyntax(ontology, outputClass, r);
                logger.info("Manchester Syntax: " + classExprInManchesterSyntax);
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return classExpr;
    }

    private OWLClassExpression getClassExprAsEquivClassOrSubclass(OWLOntology ontology, OWLClass owlClass) {
        OWLClassExpression owlClsExpr = null;

        if (isDefinedAsEquivalent(ontology, owlClass)) {
            for (OWLClassExpression eca : owlClass.getEquivalentClasses(ontology)) {
                owlClsExpr = eca;
            }
        } else if (isDefinedAsSubclass(ontology, owlClass)) {
            for (OWLClassExpression eca : owlClass.getSuperClasses(ontology)) {
                owlClsExpr = eca;
            }
        }
        return owlClsExpr;
    }

    private boolean isDefinedAsEquivalent(OWLOntology ontology, OWLClass owlClass) {
        return owlClass.getEquivalentClasses(ontology).size() > 0;
    }

    private boolean isDefinedAsSubclass(OWLOntology ontology, OWLClass owlClass) {
        return owlClass.getSuperClasses(ontology).size() > 0;
    }

    private String getClassExprInManchesterSyntax(OWLOntology ontology, OWLClass cls, ManchesterOWLSyntaxOWLObjectRendererImpl r) {

        String manchesterOWLClsExpr = null;
        if (isDefinedAsEquivalent(ontology, cls)) {
            for (OWLClassExpression eca : cls.getEquivalentClasses(ontology)) {
                manchesterOWLClsExpr = r.render(eca);
            }
        } else if (isDefinedAsSubclass(ontology, cls)) {
            for (OWLClassExpression eca : cls.getSuperClasses(ontology)) {
                manchesterOWLClsExpr = r.render(eca);
            }
        }
        return manchesterOWLClsExpr;
    }


    List<String> getImportedOntologies(String serviceOntologyURI) {

        List<String> domainOntologies = new ArrayList<String>();

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        //OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);
        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            Set<OWLImportsDeclaration> importedOntologies = ontology.getImportsDeclarations();
            for (OWLImportsDeclaration importedOnt : importedOntologies) {
                domainOntologies.add(importedOnt.getIRI().toString());
                logger.info(importedOnt.getIRI().toString());
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return domainOntologies;
    }
}
