package ca.unbsj.cbakerlab.valetsadi.module4.codegenerator;


import ca.unbsj.cbakerlab.valetsadi.Main;
import ca.unbsj.cbakerlab.valetsadi.module4.CodeGenerationException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.semanticweb.owlapi.model.*;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.xml.sax.InputSource;


import java.io.*;
import java.net.URLEncoder;
import java.util.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


class WebXmlParser extends DefaultHandler {

    Map<String, String> name2class;
    Map<String, String> name2url;
    private StringBuffer accumulator = new StringBuffer();
    private String servletName;
    private String servletClass;
    private String servletUrl;

    public WebXmlParser() {
        name2class = new HashMap<String, String>();
        name2url = new HashMap<String, String>();
    }

    public void parse(File webxmlPath) throws Exception {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setValidating(false);
        SAXParser sp = spf.newSAXParser();
        InputSource input = new InputSource(new FileReader(webxmlPath));
        input.setSystemId("file://" + webxmlPath.getAbsolutePath());
        sp.parse(input, this);
    }

    public void characters(char[] buffer, int start, int length) {
        accumulator.append(buffer, start, length);
    }


    public void endElement(String uri, String localName, String qName) {
        if (localName.equals("servlet-name") || qName.equals("servlet-name")) {
            servletName = accumulator.toString().trim();
        } else if (localName.equals("servlet-class") || qName.equals("servlet-class")) {
            servletClass = accumulator.toString().trim();
        } else if (localName.equals("url-pattern") || qName.equals("url-pattern")) {
            servletUrl = accumulator.toString().trim();
        } else if (localName.equals("servlet") || qName.equals("servlet")) {
            name2class.put(servletName, servletClass);
        } else if (localName.equals("servlet-mapping") || qName.equals("servlet-mapping")) {
            name2url.put(servletName, servletUrl);
        }
        accumulator.setLength(0);
    }

}

public class AuxiliarySourceFilesGenerator {

    private static final Logger logger = Logger.getLogger(AuxiliarySourceFilesGenerator.class);

    private final String POM_SKELETON_FILE_PATH = "/servicestub/templates/pomSkeleton";
    private final String DATABASE_CONNECTION_SKELETON_FILE_PATH = "/servicestub/templates/DatabaseConnectionSkeleton";
    private final String DATABASE_PROPERTIES_SKELETON_FILE_PATH = "/servicestub/templates/databasepropertiesSkeleton";
    private final String README_SKELETON_FILE_PATH = "/servicestub/templates/readmeSkeleton";
    private final String WEB_XML_SKELETON_FILE_PATH = "/servicestub/templates/webXmlSkeleton";
    private final String INDEX_JSP_SKELETON_FILE_PATH = "/servicestub/templates/indexSkeleton";
    private final String VOCAB_SKELETON_FILE_PATH = "/servicestub/templates/VocabularySkeleton";
    private final String SERVICE_CLASS_SKELETON_FILE_PATH = "/servicestub/templates/ServiceServletSkeleton";

    private String serviceOntologyURI;
    private String serviceName;
    private String serviceClass;
    private String inputClassURI;
    private OWLClassExpression inputClassExpr;
    private String outputClassURI;
    private OWLClassExpression outputClassExpr;
    private String description;
    private String contactEmail;

    // Contents of the files after they are created from the skeletons
    private String webXMLContent;
    private String pomXMLContent;
    private String serviceClassContent;
    private String vocabClassContent;
    private String mysqlDBClassContent;
    private String dbPropertiesContent;
    private String indexJSPContent;

    // Path for creating service codebase
    File basePath = new File(Main.SERVICE_CODE_DIR_NAME).getAbsoluteFile();
    String packagePath;


    public AuxiliarySourceFilesGenerator(String serviceOntologyURI, String serviceName, String inputClassURI, OWLClassExpression inputClassExpr, String outputClassURI, OWLClassExpression outputClassExpr, String email, String description) {
        this.serviceOntologyURI = serviceOntologyURI;
        this.serviceName = serviceName;
        this.serviceClass = Main.SERVICE_PACKAGE_STRUCTURE + "." + serviceName;
        this.inputClassURI = inputClassURI;
        this.inputClassExpr = inputClassExpr;
        this.outputClassURI = outputClassURI;
        this.outputClassExpr = outputClassExpr;
        this.description = description;
        this.contactEmail = email;
        this.packagePath = StringUtils.substringBeforeLast(serviceClass, ".");
    }


    public void generatePOM() throws CodeGenerationException {
        // Writing pom.xml file
        String pomProjectName = StringUtils.substringBeforeLast(serviceClass, ".");

        pomProjectName = StringUtils.substringAfterLast(pomProjectName, ".");
        File pomFile = new File(basePath, "pom.xml");

        try {
            logger.info("Generating " + pomFile);
            writePomFile(pomFile, pomProjectName);
            //logger.info("Finished.");
        } catch (Exception e) {
            String message = String.format("failed to write new pom.xml file for %s", serviceClass);
            throw new CodeGenerationException(message);
        }
    }

    public void generateDBConn() throws CodeGenerationException {
        // Writing connection class file for MySQL database

        String completeDBClassPath = packagePath.concat(".").concat(Main.DB_CLASS_NAME);

        File dbConnClassFile = new File(basePath, String.format("%s/%s.java", Main.SOURCE_DIRECTORY, completeDBClassPath.replace(".", "/")));

        try {
            logger.info("Generating " + dbConnClassFile);
            writeDBConnClassFile(dbConnClassFile, completeDBClassPath);
            //logger.info("Finished.");
        } catch (Exception e) {
            String message = String.format("failed to write new Database connection java file for %s", serviceClass);
            throw new CodeGenerationException(message);
        }
    }

    public void generateDBProp() throws CodeGenerationException {
        // Writing database.properties file in src/main/resources directory containing the access credentials
        File dbPropertiesFile = new File(basePath, String.format("%s/%s.properties", Main.RESOURCES_DIRECTORY, Main.DB_PROPERTY_FILENAME));

        try {
            logger.info("Generating " + dbPropertiesFile);
            writeDBPropertiesFile(dbPropertiesFile);
            //logger.info("Finished.");
        } catch (Exception e) {
            String message = String.format("failed to write new Database Properties file for %s", serviceClass);
            throw new CodeGenerationException(message);
        }
    }

    public void generateREADME() throws CodeGenerationException {
        // Writinig README.txt file about the use of this tool
        File readmeFile = new File(basePath, String.format("%s.txt", Main.README_FILENAME));

        try {
            logger.info("Generating " + readmeFile);
            writeReadmeFile(readmeFile);
            //logger.info("Finished.");
        } catch (Exception e) {
            String message = String.format("failed to write new README file for %s", serviceClass);
            throw new CodeGenerationException(message);
        }
    }

    public void generateWebXml_and_IndexJsp() throws CodeGenerationException {
        // Writing web.xml file
        File webxmlPath = new File(basePath, Main.WEB_XML_PATH);
        WebXmlParser webxml = new WebXmlParser();

        if (webxmlPath.exists()) {
            try {
                webxml.parse(webxmlPath);
            } catch (Exception e) {
                throw new CodeGenerationException("failed to parse existing web.xml: " + e.getMessage());
            }
        }
        if (webxml.name2class.containsKey(serviceName)) {
            logger.info(String.format("web.xml contains previous definition for servlet %s; it will be overwritten", serviceName));
        } else {
            logger.info(String.format("adding servlet %s to web.xml", serviceName));
            webxml.name2class.put(serviceName, serviceClass);
            try {
                webxml.name2url.put(serviceName, String.format("/%s", URLEncoder.encode(serviceName, "UTF-8")));
            } catch (UnsupportedEncodingException e) {
                // this should never happen
                throw new CodeGenerationException("failed to URL-encode service name: " + e.getMessage());
            }
        }
        try {
            logger.info("Generating " + webxmlPath);
            writeWebXml(webxmlPath, webxml);
            //logger.info("Finished.");
        } catch (Exception e) {
            throw new CodeGenerationException("failed to write web.xml", e);
        }

        // write index.jsp file
        try {
            logger.info("Generating " + basePath + "/" + Main.INDEX_JSP_PATH);
            writeIndex(new File(basePath, Main.INDEX_JSP_PATH), webxml.name2url);
            //logger.info("Finished.");
        } catch (Exception e) {
            throw new CodeGenerationException("failed to write index.jsp", e);
        }
    }

    public void generateVocab() throws CodeGenerationException {
        // Writing Vocabulary class file
        String vocabClassPath = packagePath.concat(".").concat(Main.VOCAB_FILE_NAME);
        File vocabFile = new File(basePath, String.format("%s/%s.java", Main.SOURCE_DIRECTORY,  vocabClassPath.replace(".", "/")));

        try {
            logger.info("Generating " + vocabFile);
            writeVocabFile(vocabFile, serviceClass);
        } catch (Exception e) {
            String message = String.format("failed to write new Vocab.java file for %s", serviceClass);
            logger.error(message, e);
            throw new CodeGenerationException(message);
        }
    }

    public void generateSADIClass() throws CodeGenerationException {

        // setting service params
        ServiceBean bean = new ServiceBean();
        bean.setName(this.serviceName);
        bean.setInputClassURI(this.inputClassURI);
        bean.setOutputClassURI(this.outputClassURI);
        bean.setContactEmail(this.contactEmail);
        bean.setDescription(this.description);

        logger.info(bean.getInputClassURI() + bean.getName() + bean.getDescription() + bean.getContactEmail()+ bean.getOutputClassURI());

        // Writing service class file
        File classFile = new File(basePath, String.format("%s/%s.java", Main.SOURCE_DIRECTORY, serviceClass.replace(".", "/")));

        try {
            logger.info("Generating " + classFile);
            writeClassFile(classFile, serviceClass, "inputClass", "outputClass", bean, Main.IS_SYNCHRONOUS_SERVICE, "select from where", "input.getResource()", "output.addLiteral()");
        } catch (Exception e) {
            String message = String.format("failed to write new java file for %s", serviceClass);
            throw new CodeGenerationException(message);
        }
    }

    public void createAuxiliarySourceFiles() throws CodeGenerationException {

        logger.info("Base directory for generated SADI source code: " + basePath);

        // Clean all existing files in the base directory
        if (basePath.exists()) {
            logger.info("Cleaning existing files in " + basePath);
            if (Objects.requireNonNull(basePath.listFiles()).length > 0) {
                try {
                    // if there are files
                    FileUtils.cleanDirectory(basePath);
                } catch (IOException ex) {
                    ex.getMessage();
                }
            }
        }
















    }

    private void writePomFile(File pomFile, String pomProjectName) throws Exception {
        createPath(pomFile);
        FileWriter writer = new FileWriter(pomFile);
        String template = AuxiliaryFileUtilities.readFully(this.getClass().getResourceAsStream(POM_SKELETON_FILE_PATH));
        VelocityContext context = new VelocityContext();
        context.put("pomProjectName", pomProjectName);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        pomXMLContent = FileUtils.readFileToString(pomFile, "UTF-8");
        if (StringUtils.isBlank(pomXMLContent)) {
            throw new CodeGenerationException("No content found for " + pomFile);
        }
    }

    private void writeDBConnClassFile(File dbConnClassFile, String completeDBClassPath) throws Exception {

        createPath(dbConnClassFile);
        FileWriter writer = new FileWriter(dbConnClassFile);
        String template = AuxiliaryFileUtilities.readFully(this.getClass().getResourceAsStream(DATABASE_CONNECTION_SKELETON_FILE_PATH));
        VelocityContext context = new VelocityContext();
        context.put("package", StringUtils.substringBeforeLast(completeDBClassPath, "."));
        context.put("classname", StringUtils.substringAfterLast(completeDBClassPath, "."));
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        mysqlDBClassContent = FileUtils.readFileToString(dbConnClassFile, "UTF-8");
        if (StringUtils.isBlank(mysqlDBClassContent)) {
            throw new CodeGenerationException("No content found for " + dbConnClassFile);
        }
    }

    private void writeDBPropertiesFile(File dbPropertiesFile) throws Exception {

        createPath(dbPropertiesFile);
        FileWriter writer = new FileWriter(dbPropertiesFile);
        String template = AuxiliaryFileUtilities.readFully(this.getClass().getResourceAsStream(DATABASE_PROPERTIES_SKELETON_FILE_PATH));
        VelocityContext context = new VelocityContext();
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        dbPropertiesContent = FileUtils.readFileToString(dbPropertiesFile, "UTF-8");
        if (StringUtils.isBlank(dbPropertiesContent)) {
            throw new CodeGenerationException("No content found for " + dbPropertiesFile);
        }
    }

    private void writeReadmeFile(File readmeFile) throws Exception {

        createPath(readmeFile);
        FileWriter writer = new FileWriter(readmeFile);
        String template = AuxiliaryFileUtilities.readFully(this.getClass().getResourceAsStream(README_SKELETON_FILE_PATH));
        VelocityContext context = new VelocityContext();
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        String readmeContent = FileUtils.readFileToString(readmeFile, "UTF-8");
        if (StringUtils.isBlank(readmeContent)) {
            throw new CodeGenerationException("No content found for " + readmeFile);
        }
    }

    private void writeWebXml(File webXml, WebXmlParser webxml) throws Exception {
        createPath(webXml);
        FileWriter writer = new FileWriter(webXml);
        String template = AuxiliaryFileUtilities.readWholeFileAsUTF8(this.getClass().getResourceAsStream(WEB_XML_SKELETON_FILE_PATH));
        VelocityContext context = new VelocityContext();
        context.put("name2class", webxml.name2class);
        context.put("name2url", webxml.name2url);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        webXMLContent = FileUtils.readFileToString(webXml, "UTF-8");
        if (StringUtils.isBlank(webXMLContent)) {
            throw new CodeGenerationException("No content found for " + webXml);
        }
    }

    private void writeIndex(File index, Map<String, String> name2url) throws Exception {
        createPath(index);
        FileWriter writer = new FileWriter(index);
        String template = AuxiliaryFileUtilities.readWholeFileAsUTF8(this.getClass().getResourceAsStream(INDEX_JSP_SKELETON_FILE_PATH));
        VelocityContext context = new VelocityContext();
        context.put("servlets", name2url);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        //String webXMLContents = FileUtils.readWholeFileAsUTF8(index.getCanonicalPath());
        writer.close();
        // read contents of the web.xml file
        indexJSPContent = FileUtils.readFileToString(index, "UTF-8");
        if (StringUtils.isBlank(indexJSPContent)) {
            throw new CodeGenerationException("No content found for " + index);
        }
    }


    private void writeVocabFile(File vocabFile, String serviceClass) throws Exception {
        /* create a copy of the service description where we can escape stuff...
         */

        Set<OWLIndividual> individuals = new HashSet<OWLIndividual>();
        Set<OWLClass> classes = new HashSet<OWLClass>();
        Set<OWLObjectProperty> objProps = new HashSet<OWLObjectProperty>();
        Set<OWLDataProperty> dataProps = new HashSet<OWLDataProperty>();

        Set<OWLEntity> inEntities = inputClassExpr.getSignature();
        enlistInVocabularies(inEntities,individuals,classes,objProps,dataProps);
        Set<OWLEntity> outEntities = outputClassExpr.getSignature();
        enlistInVocabularies(outEntities,individuals,classes,objProps,dataProps);

        /* write the file...
         */
        createPath(vocabFile);
        FileWriter writer = new FileWriter(vocabFile);
        String template = AuxiliaryFileUtilities.readFully(this.getClass().getResourceAsStream(VOCAB_SKELETON_FILE_PATH));
        VelocityContext context = new VelocityContext();
        context.put("package", StringUtils.substringBeforeLast(serviceClass, "."));
        context.put("dataproperties", dataProps);
        context.put("objproperties", objProps);
        context.put("classes", classes);
        context.put("individuals", individuals);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        vocabClassContent = FileUtils.readFileToString(vocabFile, "UTF-8");
        if (StringUtils.isBlank(vocabClassContent)) {
            throw new CodeGenerationException("No content found for " + vocabFile);
        }
    }

    private void enlistInVocabularies(Set<OWLEntity> allEntities, Set<OWLIndividual> individuals, Set<OWLClass> classes, Set<OWLObjectProperty> objProps, Set<OWLDataProperty> dataProps) {
        for (OWLEntity entity : allEntities) {
            if (entity.isOWLNamedIndividual()) {
                individuals.add(entity.asOWLNamedIndividual());
            }
            if (entity.isOWLClass()) {
                classes.add(entity.asOWLClass());
            }
            if (entity.isOWLObjectProperty()) {
                objProps.add(entity.asOWLObjectProperty());
            }
            if (entity.isOWLDataProperty()) {
                dataProps.add(entity.asOWLDataProperty());
            }
        }
    }

    void writeClassFile(File classFile, String serviceClass, String inputClass, String outputClass, ServiceDescription serviceDescription, boolean async, String vpAutoGeneratedSQL, String inputCodeblock, String outputCodeBlock) throws Exception {
        /* create a copy of the service description where we can escape stuff...
         */
        ServiceBean escaped = new ServiceBean();
        escaped.setName(StringEscapeUtils.escapeJava(serviceDescription.getName()));
        escaped.setDescription(StringEscapeUtils.escapeJava(serviceDescription.getDescription()));
        escaped.setContactEmail(StringEscapeUtils.escapeJava(serviceDescription.getContactEmail()));
        escaped.setInputClassURI(serviceDescription.getInputClassURI());
        escaped.setOutputClassURI(serviceDescription.getOutputClassURI());

        createPath(classFile);
        FileWriter writer = new FileWriter(classFile);
        String template = AuxiliaryFileUtilities.readFully(this.getClass().getResourceAsStream(SERVICE_CLASS_SKELETON_FILE_PATH));
        VelocityContext context = new VelocityContext();
        context.put("description", escaped);
        context.put("package", StringUtils.substringBeforeLast(serviceClass, "."));
        context.put("class", StringUtils.substringAfterLast(serviceClass, "."));
        context.put("async", async);
        context.put("sqlquery", vpAutoGeneratedSQL);
        context.put("inputcodeblock", inputCodeblock);
        context.put("outputcodeblock", outputCodeBlock);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        serviceClassContent = FileUtils.readFileToString(classFile, "UTF-8");
        if (StringUtils.isBlank(serviceClassContent)) {
            throw new CodeGenerationException("No content found for " + classFile);
        }
    }

    private void createPath(File outFile) throws IOException {
        File parent = outFile.getParentFile();
        if (parent != null && !parent.isDirectory())
            if (!parent.mkdirs()) throw new IOException(String.format("unable to create directory path ", parent));
    }
}