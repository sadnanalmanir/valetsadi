package ca.unbsj.cbakerlab.valetsadi.module4;

import ca.unbsj.cbakerlab.valetsadi.module4.codegenerator.AuxiliarySourceFilesGenerator;
import ca.unbsj.cbakerlab.valetsadi.module4.codegenerator.InputCodeGenerator;
import ca.unbsj.cbakerlab.valetsadi.module4.codegenerator.OutputCodeGenerator;
import com.tinkerpop.blueprints.Graph;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLClassExpression;

public class ServiceCodeGenerator {

    private static final Logger logger = Logger.getLogger(ServiceCodeGenerator.class);

    InputCodeGenerator inputCodeGenerator = new InputCodeGenerator();
    OutputCodeGenerator outputCodeGenerator = new OutputCodeGenerator();
    AuxiliarySourceFilesGenerator auxiliarySourceFilesGenerator;

    public void startInputCodeGenerationInJava(OWLClassExpression inputClassExpr, Graph inputGraph) {
        inputCodeGenerator.generateInputCodeInJava(inputClassExpr, inputGraph);
    }

    public String getGeneratedInputCode() {
        return inputCodeGenerator.getAUTO_GENERATED_INPUT_CODE();
    }


    public void startBuildingAuxiliaryFiles(String serviceOntologyURI, String serviceName, String inputClassURI, OWLClassExpression inputClassExpr, String outputClassURI, OWLClassExpression outputClassExpr, String email, String description) throws CodeGenerationException {

        auxiliarySourceFilesGenerator = new AuxiliarySourceFilesGenerator(serviceOntologyURI, serviceName, inputClassURI, inputClassExpr, outputClassURI, outputClassExpr, email, description);

        auxiliarySourceFilesGenerator.createAuxiliarySourceFiles();
        auxiliarySourceFilesGenerator.generatePOM();
        auxiliarySourceFilesGenerator.generateDBConn();
        auxiliarySourceFilesGenerator.generateDBProp();
        auxiliarySourceFilesGenerator.generateREADME();
        auxiliarySourceFilesGenerator.generateWebXml_and_IndexJsp();
        auxiliarySourceFilesGenerator.generateVocab();
        auxiliarySourceFilesGenerator.generateSADIClass();
    }
}
