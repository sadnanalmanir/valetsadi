package ca.unbsj.cbakerlab.valetsadi.module4.codegenerator;

import ca.unbsj.cbakerlab.valetsadi.Main;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLObjectProperty;

import java.util.LinkedList;
import java.util.List;

public class InputCodeGenerator {

    private static final Logger logger = Logger.getLogger(InputCodeGenerator.class);

    private final String ROOT_RESOURCE_NODE_NAME = "input";
    private final String PROPERTY_METHOD_SYMBOL = "getRequiredProperty";
    private final String RESOURCE_METHOD_SYMBOL = "getResource()";
    private final String GET_FLOAT_METHOD_SYMBOL = "getFloat()";
    private final String GET_INT_METHOD_SYMBOL = "getInt()";
    private final String GET_DOUBLE_METHOD_SYMBOL = "getDouble()";
    private final String GET_STRING_METHOD_SYMBOL = "getString()";
    private final String URI_METHOD_SYMBOL = "getURI";
    private final String SUBSTRING_METHOD_SYMBOL = "substring";
    private final String LENGTH_METHOD_SYMBOL = "length";
    private final String OPEN_PAR = "(";
    private final String CLOSED_PAR = ")";
    private final String DOT = ".";
    private final String STMT_END = ";";
    private final String SPACE = " ";
    private final String ESCAPE_DOUBLE_QUOTE_SYMBOL = "\"";
    private final String VOCAB_CLASS_NAME = "Vocab";
    private final String EQUAL_SIGN = "=";
    private final String LOGICAL_EQUAL_SIGN = "==";
    private final String NULL_SYMBOL = "null";

    private final String TYPE_STRING = "String";
    private final String TYPE_INT = "int";
    private final String TYPE_FLOAT = "float";
    private final String TYPE_DOUBLE = "double";

    private List<String> statements = new LinkedList<String>();

    private final String INPUT_VAR_SYMBOL = "X";

    private String RIGHT_HAND_SIDE = "";
    private String LEFT_HAND_SIDE = "";

    private String AUTO_GENERATED_INPUT_CODE = "";



    public void generateInputCodeInJava(OWLClassExpression inputClassExpr, Graph inputGraph) {

        // If the WHERE clause variables are related to data properties only
        if (!inputClassExpr.getDataPropertiesInSignature().isEmpty()) {
            for (Edge edge : inputGraph.getEdges()) {
                if (isObjectProperty(edge.getLabel(), inputClassExpr)) {
                    if (!edge.getLabel().equals("type")) {
                        RIGHT_HAND_SIDE += createCodeForResource(edge.getLabel());
                    }
                }
                if (isDataProperty(edge.getLabel(), inputClassExpr)) {
                    RIGHT_HAND_SIDE += createCodeForData(edge.getLabel());
                    //log.info("rhs:" + RIGHT_HAND_SIDE);
                    if (edge.getVertex(Direction.IN).getProperty("name").toString().equals("string")) {
                        RIGHT_HAND_SIDE += DOT + GET_STRING_METHOD_SYMBOL;
                        LEFT_HAND_SIDE = TYPE_STRING + SPACE + INPUT_VAR_SYMBOL + edge.getVertex(Direction.IN).getId().toString();
                    } else if (edge.getVertex(Direction.IN).getProperty("name").toString().equals("int")) {
                        RIGHT_HAND_SIDE += DOT + GET_INT_METHOD_SYMBOL;
                        LEFT_HAND_SIDE = TYPE_INT + SPACE + INPUT_VAR_SYMBOL + edge.getVertex(Direction.IN).getId().toString();
                    } else if (edge.getVertex(Direction.IN).getProperty("name").toString().equals("float")) {
                        RIGHT_HAND_SIDE += DOT + GET_FLOAT_METHOD_SYMBOL;
                        LEFT_HAND_SIDE = TYPE_FLOAT + SPACE + INPUT_VAR_SYMBOL + edge.getVertex(Direction.IN).getId().toString();
                    } else if (edge.getVertex(Direction.IN).getProperty("name").toString().equals("double")) {
                        RIGHT_HAND_SIDE += DOT + GET_DOUBLE_METHOD_SYMBOL;
                        LEFT_HAND_SIDE = TYPE_DOUBLE + SPACE + INPUT_VAR_SYMBOL + edge.getVertex(Direction.IN).getId().toString();
                    }
                    String inputCheckMsg = createInputCheckMsg(INPUT_VAR_SYMBOL + edge.getVertex(Direction.IN).getId().toString());
                    String indent = "\n\n" + "\t\t";
                    statements.add(indent + LEFT_HAND_SIDE + SPACE + EQUAL_SIGN + SPACE + ROOT_RESOURCE_NODE_NAME + RIGHT_HAND_SIDE + STMT_END + inputCheckMsg);
                    LEFT_HAND_SIDE = "";
                    RIGHT_HAND_SIDE = "";
                }
            }
        } // no data properties, the WHERE clause variables are related to OWLObjectHasValue (i.e. type value Person)
        // in the ROOT node only
        else {
            LEFT_HAND_SIDE = "";
            RIGHT_HAND_SIDE = "";
            for (Edge edge : inputGraph.getEdges()) {
                if (isObjectProperty(edge.getLabel(), inputClassExpr)) {
                    if (edge.getLabel().equals("type") && edge.getVertex(Direction.OUT).getId().toString().equals("0")) {
                        LEFT_HAND_SIDE = TYPE_STRING + SPACE + INPUT_VAR_SYMBOL + edge.getVertex(Direction.OUT).getId().toString();
                        RIGHT_HAND_SIDE = createCodeForUriInstance(edge.getVertex(Direction.IN).getProperty("name").toString());
                    }
                    String inputCheckMsg = createInputCheckMsg(INPUT_VAR_SYMBOL + edge.getVertex(Direction.OUT).getId().toString());
                    String indent = "\n\n" + "\t\t";
                    statements.add(indent + LEFT_HAND_SIDE + SPACE + EQUAL_SIGN + SPACE + ROOT_RESOURCE_NODE_NAME + RIGHT_HAND_SIDE + STMT_END + inputCheckMsg);
                    LEFT_HAND_SIDE = "";
                    RIGHT_HAND_SIDE = "";
                }

            }
        }

    }

    private boolean isObjectProperty(String property, OWLClassExpression classExpr) {

        for (OWLObjectProperty objProp : classExpr.getObjectPropertiesInSignature()) {
            if (property.equals(objProp.getIRI().getFragment())) {
                return true;
            }
        }
        return false;
    }

    private boolean isDataProperty(String property, OWLClassExpression classExpr) {
        for (OWLDataProperty dataProp : classExpr.getDataPropertiesInSignature()) {
            if (property.equals(dataProp.getIRI().getFragment())) {
                return true;
            }
        }
        return false;
    }

    private String createCodeForResource(String objProperty) {
        return DOT + PROPERTY_METHOD_SYMBOL + OPEN_PAR + VOCAB_CLASS_NAME + DOT + objProperty + CLOSED_PAR + DOT + RESOURCE_METHOD_SYMBOL;
    }

    private String createCodeForData(String dataProperty) {
        return DOT + PROPERTY_METHOD_SYMBOL + OPEN_PAR + VOCAB_CLASS_NAME + DOT + dataProperty + CLOSED_PAR;
    }

    private String createCodeForUriInstance(String individualName) {
        String uriMap = createUriMap(individualName);
        return DOT + URI_METHOD_SYMBOL + OPEN_PAR + CLOSED_PAR + DOT + SUBSTRING_METHOD_SYMBOL + OPEN_PAR + OPEN_PAR
                + ESCAPE_DOUBLE_QUOTE_SYMBOL + uriMap + EQUAL_SIGN + ESCAPE_DOUBLE_QUOTE_SYMBOL
                + CLOSED_PAR + DOT + LENGTH_METHOD_SYMBOL + OPEN_PAR + CLOSED_PAR + CLOSED_PAR;
    }

    public void printStatements() {
        for (String stmt : statements) {
            logger.info(stmt);
        }
    }

    private String createInputCheckMsg(String argument) {
        String msg = "\n" + "\t\t" + "if" + OPEN_PAR + argument + SPACE + LOGICAL_EQUAL_SIGN + SPACE + NULL_SYMBOL + CLOSED_PAR;
        msg += "\n" + "\t\t" + "{";
        msg += "\n" + "\t\t" + "\tlog.fatal" + OPEN_PAR + "\"" + "Cannot extract " + argument + " from the URI." + "\"" + ")" + ";";
        msg += "\n" + "\t\t" + "\tthrow new IllegalArgumentException"
                + "(" + "\"" + "Cannot extract " + argument
                + " from the URI" + "\"" + ")" + ";" + "\n" + "\t\t" + "}";
        return msg;
    }

    public String getAUTO_GENERATED_INPUT_CODE() {
        for (String stmt : statements) {
            AUTO_GENERATED_INPUT_CODE += stmt;
        }
        return AUTO_GENERATED_INPUT_CODE;
    }

    private String createUriMap(String individualName) {
        String uriPrefix = Main.URI_PREFIX;
        String uriSuffix = Main.URI_SUFFIX;
        return uriPrefix + individualName + uriSuffix;
    }


}
