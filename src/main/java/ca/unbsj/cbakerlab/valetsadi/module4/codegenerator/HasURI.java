package ca.unbsj.cbakerlab.valetsadi.module4.codegenerator;

public interface  HasURI {
    /**
     * Returns the URI of the object.
     * @return the URI of the object
     */
    String getURI();
}
