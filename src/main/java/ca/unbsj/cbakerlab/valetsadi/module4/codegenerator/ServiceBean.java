package ca.unbsj.cbakerlab.valetsadi.module4.codegenerator;

import java.io.Serializable;

public class ServiceBean implements Serializable, ServiceDescription {

    private String URI;
    private String name;
    private String description;
    private String email;
    private String inputClassURI;
    private String inputClassLabel;
    private String outputClassURI;
    private String outputClassLabel;

    public ServiceBean()
    {
        URI = null;
        name = null;
        description = null;
        email = null;
        inputClassURI = null;
        inputClassLabel = "";
        outputClassURI = null;
        outputClassLabel = "";
    }

    @Override
    public String getURI() {
        return URI;
    }

    public void setURI(String URI)
    {
        this.URI = URI;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }


    @Override
    public String getContactEmail() {
        return email;
    }

    public void setContactEmail(String email)
    {
        this.email = email;
    }

    @Override
    public String getInputClassURI() {
        return inputClassURI;
    }

    public void setInputClassURI(String inputClassURI)
    {
        this.inputClassURI = inputClassURI;
    }

    @Override
    public String getInputClassLabel() {
        return inputClassLabel;
    }

    public void setInputClassLabel(String inputClassLabel)
    {
        this.inputClassLabel = inputClassLabel;
    }

    @Override
    public String getOutputClassURI() {
        return outputClassURI;
    }

    public void setOutputClassURI(String outputClassURI)
    {
        this.outputClassURI = outputClassURI;
    }

    @Override
    public String getOutputClassLabel() {
        return outputClassLabel;
    }

    public void setOutputClassLabel(String outputClassLabel)
    {
        this.outputClassLabel = outputClassLabel;
    }
}
