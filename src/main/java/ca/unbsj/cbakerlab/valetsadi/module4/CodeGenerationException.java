package ca.unbsj.cbakerlab.valetsadi.module4;

public class CodeGenerationException  extends Exception {

    public CodeGenerationException() {
        super();
    }

    public CodeGenerationException(String message) {
        super(message);
    }

    public CodeGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}